// /* eslint-disable no-var,no-var,block-scoped-var,vars-on-top,quotes,no-bitwise */
// var fs = require("fs");
// var glob = require("glob");
// var path = require("path");
// var readline = require("readline-sync");

// module.exports = async () => {
//     let reactNativeHostInstantiation;
//     console.log("Running android postlink script");

//     const ignoreFolders = {ignore: ["node_modules/**", "**/build/**"]};

//     const buildGradlePath = path.join("android", "app", "build.gradle");
//     const mainBuildGradlePath = path.join("android", "build.gradle");
//     const settingsGradlePath = path.join("android", "settings.gradle");
//     const manifestPath = glob.sync("**/AndroidManifest.xml", ignoreFolders)[0];

//     function findMainApplication() {
//         if (!manifestPath) {
//             return null;
//         }

//         var manifest = fs.readFileSync(manifestPath, "utf8");

//         // Android manifest must include single 'application' element
//         var matchResult = manifest.match(/application\s+android:name\s*=\s*"(.*?)"/);
//         if (matchResult) {
//             var appName = matchResult[1];
//         }
//         else {
//             return null;
//         }

//         var nameParts = appName.split(".");
//         var searchPath = glob.sync(`**/${nameParts[nameParts.length - 1]}.java`, ignoreFolders)[0];
//         return searchPath;
//     }

//     var mainApplicationPath = findMainApplication() || glob.sync("**/MainApplication.java", ignoreFolders)[0];
//     var mainActivityPath = glob.sync("**/MainActivity.java", ignoreFolders)[0];

//     var getWebViewDebuggingEnabled = `
//         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//           if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
//             WebView.setWebContentsDebuggingEnabled(true);
//           }
//         }
//     `;

//     var getMultiDexAndFacebookCallback = `
//     @Override
//     protected void attachBaseContext(Context base) {
//         super.attachBaseContext(base);
//         MultiDex.install(this);
//     }

//     private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

//     protected static CallbackManager getCallbackManager() {
//         return mCallbackManager;
//     }
//     `;

//     const getFacebookPackage = "new FBSDKPackage(mCallbackManager),";

//     function isAlreadyAddedWebviewDebugMode(codeContents) {
//         return /if \(Build\.VERSION\.SDK_INT >= Build\.VERSION_CODES\.KITKAT\) {/.test(codeContents);
//     }

//     function isAlreadyOverridden(codeContents) {
//         return /@Override\s*\n\s*protected void attachBaseContext\(Context base\)\s*\{[\s\S]*?\}/.test(codeContents);
//     }

//     function isAlreadyPackageAdded(codeContents) {
//         return /new FBSDKPackage\(mCallbackManager\)/.test(codeContents);
//     }

//     function isAlreadyPackageAddedWithOutCallbackManager(codeContents) {
//         return /new FBSDKPackage\(\)/.test(codeContents);
//     }

//     if (mainApplicationPath) {
//         let mainApplicationContents = fs.readFileSync(mainApplicationPath, "utf8");

//         if (isAlreadyAddedWebviewDebugMode(mainApplicationContents)) {
//             console.log(`"AddedWebviewDebugMode" is already overridden`);
//         }
//         else {
//             reactNativeHostInstantiation = "SoLoader.init(this, /* native exopackage */ false);";
//             mainApplicationContents = mainApplicationContents.replace(
//                 reactNativeHostInstantiation,
//                 `${getWebViewDebuggingEnabled}\n${reactNativeHostInstantiation}`
//             );
//             fs.writeFileSync(mainApplicationPath, mainApplicationContents);
//         }

//         if (isAlreadyOverridden(mainApplicationContents)) {
//             console.log(`"attachBaseContext" is already overridden`);
//         }
//         else {
//             reactNativeHostInstantiation = "private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {";
//             mainApplicationContents = mainApplicationContents.replace(
//                 reactNativeHostInstantiation,
//                 `${getMultiDexAndFacebookCallback}\n${reactNativeHostInstantiation}`
//             );
//             fs.writeFileSync(mainApplicationPath, mainApplicationContents);
//         }

//         if (isAlreadyPackageAdded(mainApplicationContents)) {
//             console.log(`"FBSDKPackage" is already overridden`);
//         }
//         else if (isAlreadyPackageAddedWithOutCallbackManager(mainApplicationContents)) {
//             reactNativeHostInstantiation = "new FBSDKPackage()";
//             mainApplicationContents = mainApplicationContents.replace(
//                 reactNativeHostInstantiation,
//                 `${getFacebookPackage}`
//             );
//             fs.writeFileSync(mainApplicationPath, mainApplicationContents);
//         }
//         else {
//             reactNativeHostInstantiation = "new MainReactPackage()";
//             mainApplicationContents = mainApplicationContents.replace(
//                 reactNativeHostInstantiation,
//                 `${getFacebookPackage}\n${reactNativeHostInstantiation}`
//             );
//             fs.writeFileSync(mainApplicationPath, mainApplicationContents);
//         }

//     }
//     else {
//         return Promise.reject(`Couldn't find Android application entry point. You might need to update it manually. \
//     Please ask to Caner`);
//     }

//     if (!fs.existsSync(buildGradlePath)) {
//         return Promise.reject(`Couldn't find build.gradle file. You might need to update it manually. \
//     Please refer to plugin installation section for Android at \
//     https://github.com/microsoft/react-native-code-push#plugin-installation-android---manual`);
//     }

//     // 2. Add the dependencies.gradle build task definitions
//     let buildGradleContents = fs.readFileSync(buildGradlePath, "utf8");

//     const dependenciesGradleLink = `
//     dependencies {
//         compile project(':react-native-vector-icons')
//         compile project(':react-native-text-input-mask')
//         compile project(':react-native-maps')
//         compile project(':react-native-mall-frame-client')
//         compile project(':react-native-image-picker')
//         compile project(':react-native-i18n')
//         compile project(':react-native-google-signin')
//         compile project(':react-native-fetch-blob')
//         compile project(':react-native-fbsdk')
//         compile project(':react-native-bluetooth-listener')
//         compile project(':react-native-audio-toolkit')
//         compile fileTree(dir: "libs", include: ["*.jar"])
//         compile "com.android.support:appcompat-v7:26.0.1"
//         compile "com.facebook.react:react-native:+"  // From node_modules

//         compile(project(':react-native-maps')) {
//             exclude group: 'com.google.android.gms', module: 'play-services-base'
//             exclude group: 'com.google.android.gms', module: 'play-services-maps'
//         }

//         compile(project(":react-native-google-signin")) {
//             exclude group: "com.google.android.gms" // very important
//         }

//         //  http://stackoverflow.com/questions/15209831/unable-to-execute-dex-method-id-not-in-0-0xffff-65536
//         compile 'com.android.support:multidex:1.0.3'

//         //noinspection GradleCompatible

//         compile('com.google.android.gms:play-services-base:11.8.0') {
//             force = true;
//         }
//         compile('com.google.android.gms:play-services-maps:11.8.0') {
//             force = true;
//         }
//         compile('com.google.android.gms:play-services-auth:11.8.0') {
//             force = true;
//         }
//     }

//     configurations.all {
//         resolutionStrategy.eachDependency { DependencyResolveDetails details ->
//             if (details.getRequested().getGroup() == 'com.google.android.gms') {
//                 details.useVersion('11.8.0')
//             }
//         }
//     }

//     /// / Run this once to be able to run the application with BUCK
//     // puts all compile dependencies into folder libs for BUCK to use
//     task copyDownloadableDepsToLibs(type: Copy) {
//         from configurations.compile
//         into 'libs'
//     }

//     apply plugin: 'com.google.gms.google-services'
//     `;

//     if (~buildGradleContents.indexOf("apply plugin: 'com.google.gms.google-services'")) {
//         console.log(`"google-services" is already linked in the build definition`);
//     }
//     else {
//         buildGradleContents = `${buildGradleContents.split("dependencies {")[0]}\n${dependenciesGradleLink}`;
//         fs.writeFileSync(buildGradlePath, buildGradleContents);
//     }

//     var manifestGradleContents = fs.readFileSync(manifestPath, "utf8");
//     var manifestUserPermissions = `
//     <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
//     <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
//     <uses-permission android:name="android.permission.CAMERA" />
//     <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" /> 
//     <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
//     <uses-permission android:name="android.permission.VIBRATE" />
//     `;

//     if (~manifestGradleContents.indexOf("<uses-permission android:name=\"android.permission.WRITE_EXTERNAL_STORAGE\" />")) {
//         console.log(`"WRITE_EXTERNAL_STORAGE" is already linked in the manifest definition`);
//     }
//     else {
//         reactNativeHostInstantiation = "<uses-permission android:name=\"android.permission.INTERNET\" />";
//         manifestGradleContents = manifestGradleContents.replace(
//             reactNativeHostInstantiation,
//             `${manifestUserPermissions}\n${reactNativeHostInstantiation}`
//         );
//         fs.writeFileSync(manifestPath, manifestGradleContents);
//     }

//     var packageName = readline.question("Android package name \"com.kns.asdasd\"? : ");
//     console.log("packageName: ", packageName);

//     var facebook_app_id = readline.question("Android facebook app id? : ");
//     console.log("facebook app id: ", facebook_app_id);

//     var manifestGradleContents = fs.readFileSync(manifestPath, "utf8");
//     var manifestProviderAndKeys = `
//     <provider
//     android:name="android.support.v4.content.FileProvider"
//     android:authorities="${packageName}.provider"
//     android:exported="false"
//     android:grantUriPermissions="true" />

//     <meta-data
//     android:name="com.google.android.geo.API_KEY"
//     android:value="AIzaSyBhKn-IWmNQJNacvQdJnPq40JPQF8jMdoM" />

//     <meta-data
//     android:name="com.facebook.sdk.ApplicationId"
//     android:value="${facebook_app_id}" />
//     `;

//     if (~manifestGradleContents.indexOf("android.support.v4.content.FileProvider")) {
//         console.log(`"android.support.v4.content.FileProvider" is already linked in the manifest definition`);
//     }
//     else {
//         reactNativeHostInstantiation = "</application>";
//         manifestGradleContents = manifestGradleContents.replace(
//             reactNativeHostInstantiation,
//             `${manifestProviderAndKeys}\n${reactNativeHostInstantiation}`
//         );
//         fs.writeFileSync(manifestPath, manifestGradleContents);
//     }

//     var settingsGradleContents = fs.readFileSync(settingsGradlePath, "utf8");

//     var settingsGradleInclude = `
//     include ':react-native-vector-icons'
//     project(':react-native-vector-icons').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-vector-icons/android')
//     include ':react-native-text-input-mask'
//     project(':react-native-text-input-mask').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-text-input-mask/android')
//     include ':react-native-maps'
//     project(':react-native-maps').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-maps/lib/android')
//     include ':react-native-mall-frame-client'
//     project(':react-native-mall-frame-client').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mall-frame-client/android')
//     include ':react-native-image-picker'
//     project(':react-native-image-picker').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-image-picker/android')
//     include ':react-native-i18n'
//     project(':react-native-i18n').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-i18n/android')
//     include ':react-native-google-signin'
//     project(':react-native-google-signin').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-google-signin/android')
//     include ':react-native-fetch-blob'
//     project(':react-native-fetch-blob').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-fetch-blob/android')
//     include ':react-native-fbsdk'
//     project(':react-native-fbsdk').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-fbsdk/android')
//     include ':react-native-bluetooth-listener'
//     project(':react-native-bluetooth-listener').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-bluetooth-listener/android')
//     include ':react-native-audio-toolkit'
//     project(':react-native-audio-toolkit').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-audio-toolkit/android/lib')
//     `;

//     if (~settingsGradleContents.indexOf("include ':react-native-mall-frame-client'")) {
//         console.log(`"include ':react-native-mall-frame-client'" is already linked in the manifest definition`);
//     }
//     else {
//         reactNativeHostInstantiation = "include ':app'";
//         settingsGradleContents = settingsGradleContents.replace(
//             reactNativeHostInstantiation,
//             `${settingsGradleInclude}\n${reactNativeHostInstantiation}`
//         );
//         fs.writeFileSync(settingsGradlePath, settingsGradleContents);
//     }

//     var mainBuildGradleContents = fs.readFileSync(mainBuildGradlePath, "utf8");

//     var mainGradleInclude = `
//     buildscript {
//         repositories {
//             jcenter()
//             google()
//         }
//         dependencies {
//             classpath 'com.android.tools.build:gradle:3.0.1'
//             classpath 'com.google.gms:google-services:3.0.0'
//         }
//     }

//     allprojects {
//         repositories {
//             mavenCentral()
//             mavenLocal()
//             jcenter()
//             google()
//             maven {
//                 url "$rootDir/../node_modules/react-native/android"
//             }
//         }
//     }
//     `;

//     if (~mainBuildGradleContents.indexOf("com.google.gms:google-services:3.0.0")) {
//         console.log(`"com.google.gms:google-services:3.0.0" is already linked in the manifest definition`);
//     }
//     else {
//         fs.writeFileSync(mainBuildGradlePath, mainGradleInclude);
//     }

//     let mainActivityContents = fs.readFileSync(mainActivityPath, "utf8");
//     const mainActivityInclude = `
//     @Override
//     public void onActivityResult(int requestCode, int resultCode, Intent data) {
//         super.onActivityResult(requestCode, resultCode, data);
//         MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
//     }
//     `;

//     if (~mainActivityContents.indexOf("onActivityResult")) {
//         console.log(`"onActivityResult" is already linked in the manifest definition`);
//     }
//     else {
//         reactNativeHostInstantiation = "public class MainActivity extends ReactActivity {";

//         mainActivityContents = mainActivityContents.replace(
//             reactNativeHostInstantiation,
//             `${reactNativeHostInstantiation}\n${mainActivityInclude}`
//         );
//         fs.writeFileSync(mainActivityPath, mainActivityContents);
//     }

//     return Promise.resolve();
// };

