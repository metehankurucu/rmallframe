import {NativeModules} from "react-native";

const {RNMallFrameClient} = NativeModules;

const Geofence = {
    start(params) {
        RNMallFrameClient.startGeofence(params);
    },
};

export {Geofence};
