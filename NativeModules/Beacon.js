import {NativeModules} from "react-native";

const {RNMallFrameClient} = NativeModules;

const Beacon = {
    start() {
        RNMallFrameClient.startBeacon();
    }
};

export {Beacon};
