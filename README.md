
# react-native-mall-frame-client

## Getting started

`$ npm install react-native-mall-frame-client --save`

### Mostly automatic installation

`$ react-native link react-native-mall-frame-client`

- Android için custom postlink scripti eklendi.
- I0S için bir sonraki versiyonda eklenecek.

- For iOS you have to drag and drop InputMask framework to Embedded Binaries in General tab of Target and check ‘Yes’ in ‘Always Embed Swift Standard Libraries’ of Build Settings.

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-mall-frame-client` and add `RNMallFrameClient.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNMallFrameClient.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.kns.RNMallFrameClientPackage;` to the imports at the top of the file
  - Add `new RNMallFrameClientPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-mall-frame-client'
  	project(':react-native-mall-frame-client').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-mall-frame-client/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-mall-frame-client')
  	```


## Usage

Simply config file.
```javascript
import {Platform} from 'react-native';
import {UpdateNewConfig} from "react-native-mall-frame-client/config/config";
import {
    AboutUs,
    AudioRecord,
    Browser,
    CafeRestaurants,
    Cinema,
    CinemaDetail,
    Contact,
    Dining,
    Entertainment,
    EventDetail,
    Events,
    Home,
    HowToGetHere,
    ImagePick,
    MallMap,
    OfferDetail,
    Offers,
    Parking,
    ServiceDetail,
    Services,
    Splash,
    StoreDetail,
    StoresList,
    storesPager,
    Wayfinder,
    WayfinderStoreList
} from "../app/screens";
import BrandListItem from "../app/components/brand/BrandListItem";
import OfferListItem from "../app/components/offer/OfferListItem";


let configuration = {
    /**
     *
     * Api URL And Default Info Data
     */

    BASE_URL: "{{API_URL}}",
    API_URL: "{{API_URL_WITH_MALL_ID}}",

    INFO_DATA: {
        language: 'en',
        device: Platform.OS,
        env: __DEV__ ? 'development' : 'production',
        push_token: this.push_token || '',
        userid: this.userid || ''
    },

    region: {
        identifier: 'KNS',
        uuid: '',
        major: ''
    },

    BASE_COLOR: '#63abfe',
    BASE_SECOND_COLOR: '#2a2a2a',

    /**
     *
     * Default Navbar Style
     */

    DEFAULT_NAVBAR_COLOR: '#63abfe',
    DEFAULT_NAVBAR_TITLE_COLOR: '#fff',

    /**
     *
     * Default Status Bar Color for Android and Status Bar Style Both
     */

    DEFAULT_STATUS_BAR_COLOR: '#63abfe',
    DEFAULT_STATUS_BAR_STYLE: 'light-content',

    /**
     *
     * Default BackgroundColor For Main Screens
     */

    DEFAULT_BACKGROUND_COLOR: '#f3f3f3',

    /**
     *
     * Default Ripple Effect Color for View and Buttons in Android
     */

    DefaultRippleColor: '#d5d5d5',

    /**
     *
     * Default Button Color
     */

    DEFAULT_BUTTON_COLOR: '#2a2a2a',
    DEFAULT_BUTTON_HIGHLIGHT_COLOR: '#555555',


    /**
     *
     * Default http parameter suffix
     */

    DEFAULT_HTTP_SUFFIX: () => {
        return `?language=en&device=${Platform.OS}`;
    },

    /**
     *
     *
     * beacon control active or passive
     */
    beaconMode: false,


    SplashScreen: {
        Enabled: true,
        Component: Splash,
        BackgroundImage: require('../assets/images/splash.png'),
    },

    HomeScreen: {
        Enabled: false,
        Component: Home,
        BoxHeight: 130,
        Refreshable: false
    },

    BrandScreen: {
        Enabled: false,
        Component: storesPager,
    },
    BrandListScreen: {
        Enabled: false,
        Component: StoresList,
    },
    BrandDetailScreen: {
        Enabled: false,
        Component: StoreDetail,
    },
    BrandListSubCategoryScreen: {
        Enabled: false,
        Component: false,
    },
    BrandImageGalleryScreen: {
        Enabled: false,
        Component: false,
    },
    BrandListItemComponent: {
        Enabled: true,
        Component: BrandListItem
    },


    WayfinderScreen: {
        Enabled: false,
        Component: Wayfinder,
    },
    WayfinderStoreListScreen: {
        Enabled: false,
        Component: WayfinderStoreList,
    },
    MallMapScreen: {
        Enabled: false,
        Component: MallMap,
    },

    CafeRestaurantScreen: {
        Enabled: false,
        Component: CafeRestaurants,
        CafeRestaurantCategoryId: "V1-x8j93l",
    },

    EventScreen: {
        Enabled: false,
        Component: Events,
    },
    EventDetailScreen: {
        Enabled: false,
        Component: EventDetail,
    },

    OfferScreen: {
        Enabled: false,
        Component: Offers,
    },
    OfferDetailScreen: {
        Enabled: false,
        Component: OfferDetail,
    },
    OfferListItemComponent: {
        Enabled: true,
        Component: OfferListItem
    },

    EntertainmentScreen: {
        Enabled: false,
        Component: Entertainment,
        FunCategoryId: "E15fLoc3e",
    },

    ServiceScreen: {
        Enabled: false,
        Component: Services,
    },
    ServiceDetailScreen: {
        Enabled: false,
        Component: ServiceDetail,
    },

    DiningScreen: {
        Enabled: false,
        Component: Dining,
    },
    CinemaScreen: {
        Enabled: false,
        Component: Cinema,
    },
    CinemaDetailScreen: {
        Enabled: false,
        Component: CinemaDetail,
    },
    ParkingScreen: {
        Enabled: false,
        Component: Parking,
    },
    CarParkingPhotoScreen: {
        Enabled: false,
        Component: ImagePick,
    },
    AudioRecordParkingScreen: {
        Enabled: false,
        Component: AudioRecord,
    },

    ContactScreen: {
        Enabled: false,
        Component: Contact,
    },

    AboutScreen: {
        Enabled: false,
        Component: AboutUs,
    },

    HowToGetHereScreen: {
        Enabled: false,
        Component: HowToGetHere,
    },

    BrowserScreen: {
        Enabled: false,
        Component: Browser,
    },

    WhatsNewScreen: {
        Enabled: false,
        Component: false,
    },
    WhatsNewDetailScreen: {
        Enabled: false,
        Component: false,
    },

    UpdateProfileScreen: {
        Enabled: false,
        Component: false,
    },
    RegisterScreen: {
        Enabled: false,
        Component: false,
    },
    ForgetPasswordScreen: {
        Enabled: false,
        Component: false,
    },
    ProfileScreen: {
        Enabled: false,
        Component: false,
    },
    LoginScreen: {
        Enabled: false,
        Component: false,
    },

    floorDimensionWidth: 1440,
    floorDimensionHeight: 1200,

    LoginWithFacebook: true,
    LoginWithGoogle: true,

    newsKeymap: {
        enabled: false,
    },

    DEFAULT_LANGUAGES: [
        {lang: 'English', sort: "EN"},
        {lang: 'Turkish', sort: "TR"},
        {lang: 'Arabian', sort: "AR"},
        {lang: 'Russian', sort: "RU"}
    ],


};

UpdateNewConfig(configuration);
```

You must update new config in index.{}.js file.
```javascript

import { AppRegistry } from 'react-native';
import config from "./config/config"; // must be import before app.
import App from './App';
AppRegistry.registerComponent('example', () => App);

```
