/* eslint-disable import/extensions,import/no-unresolved,consistent-return */
import { NativeEventEmitter, NativeModules, Platform } from "react-native";
import { Geofence, Beacon } from "./NativeModules/";
import { Store } from "./app/utils/store";

const { RNMallFrameClient } = NativeModules;

export default {
    events: [
        "geofence",
        "beaconsDidRange"
    ],

    configure(params) {
        if (RNMallFrameClient) {
            RNMallFrameClient.configure(params);
        } else {
            console.log('RNMallFrameClient NativeModule is null')
        }

    },

    on(event, callbackFn) {
        if (typeof callbackFn !== "function") {
            throw Error("RNMallFrameClient: callback function must be provided");
        }
        if (this.events.indexOf(event) < 0) {
            throw Error(`RNMallFrameClient: Unknown event "${event}"`);
        }

        return new NativeEventEmitter(RNMallFrameClient).addListener(event, callbackFn);
    },
    gpsEnable() {
        RNMallFrameClient.enableGPS();
    },
    getCurrentState() {
        return new Promise(async (resolve) => {
            if (Platform.OS === "ios") {
                const status = await RNMallFrameClient.getCurrentState();
                return resolve(status[0] === "authorizedWhenInUse" || status[0] === "authorizedAlways");
            }

            resolve(false);
        });
    },
};


export {
    Geofence,
    Beacon
};
export { Store };
