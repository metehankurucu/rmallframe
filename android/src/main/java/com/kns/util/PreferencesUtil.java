package com.kns.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesUtil {

    private static final String USER_PREFERENCES = "MALLFRAME_PREFERENCES";

    private static final String USER = "USER";
    private static final String TOKEN = "TOKEN";
    private static final String URL = "URL";
    private static final String CLIENTID = "CLIENTID";
    private static final String LANGUAGE = "LANGUAGE";


    public static String getToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(TOKEN, "");
    }

    public static void saveToken(Context context, String token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(TOKEN, token).apply();
    }

    public static String getUserid(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER, "");
    }

    public static void saveUserid(Context context, String token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(USER, token).apply();
    }

    public static String getUrl(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(URL, "");
    }

    public static void saveUrl(Context context, String url) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(URL, url).apply();
    }

    public static String getClientId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(CLIENTID, "");
    }

    public static void saveClientId(Context context, String client_id) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(CLIENTID, client_id).apply();
    }

    public static String getLanguage(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(LANGUAGE, "");
    }

    public static void saveLanguage(Context context, String language) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(LANGUAGE, language).apply();
    }
}
