
package com.kns;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.kns.util.PreferencesUtil;
import android.util.Log;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;public class RNMallFrameClientModule extends ReactContextBaseJavaModule {

    public static ReactApplicationContext reactContext;

    protected static final String TAG = "mallframe";
    public static boolean isAppOnForeground = false;


    public RNMallFrameClientModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "RNMallFrameClient";
    }

    /**
     * default configure
     *
     * @param configure
     * @return
     */
    @ReactMethod
    public String configure(ReadableMap configure) {

        //configure Data Saved Preferences

        String url = configure.getString("url");
        String userid = configure.getString("userid");
        String push_token = configure.getString("push_token");
        String client_id = configure.getString("client_id");
        String language = configure.getString("language");


        PreferencesUtil.saveUrl(reactContext, url);
        PreferencesUtil.saveUserid(reactContext, userid);
        PreferencesUtil.saveToken(reactContext, push_token);
        PreferencesUtil.saveClientId(reactContext, client_id);
        PreferencesUtil.saveLanguage(reactContext, language);

        Log.d(TAG, url);
        Log.d(TAG, userid);
        Log.d(TAG, push_token);
        Log.d(TAG, client_id);
        Log.d(TAG, language);
        Log.d(TAG, "configure saved");


        return "OK";
    }
}
