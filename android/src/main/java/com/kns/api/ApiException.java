package com.kns.api;

/**
 * Created by erenbekar on 6/2/15.
 */
public class ApiException extends Exception {

    private int code;

    public ApiException() {
    }

    public ApiException(String errorMessage) {
        super(errorMessage);
    }

    public ApiException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }

    public ApiException(Throwable throwable) {
        super(throwable);
    }

    public ApiException(int code, String errorMessage) {
        super(errorMessage);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

}