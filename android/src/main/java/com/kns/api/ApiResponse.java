package com.kns.api;

/**
 * Created by erenbekar on 1/13/16.
 */
public class ApiResponse<T> {

    private T data;
    private boolean succeeded;
    private String errorMessage;

    public ApiResponse(T data) {
        this.data = data;
        this.succeeded = true;
    }

    public ApiResponse(String errorMessage) {
        this.errorMessage = errorMessage;
        this.succeeded = false;
    }

    public T getData() {
        return data;
    }

    public boolean isSucceeded() {
        return succeeded;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
