package com.kns.models;

import com.google.gson.annotations.SerializedName;
import com.kns.models.batch.Info;

import java.io.Serializable;

public class BaseRequest implements Serializable {

    @SerializedName("info")
    private Info info;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }
}
