package com.kns.models.batch;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by erenbekar on 7/15/16.
 */
public class Info implements Serializable {

    @SerializedName("language")
    private String language;

    @SerializedName("device")
    private String device;

    @SerializedName("env")
    private String env;

    @SerializedName("push_token")
    private String pushToken;

    @SerializedName("userid")
    private String userId;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
