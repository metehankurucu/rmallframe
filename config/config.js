/* eslint-disable global-require,no-use-before-define */
import { Platform } from "react-native";
import _ from "lodash";
import I18n from "react-native-i18n";

import RNMallFrameClient from "../";
import { screens } from "./utils";
import Languages from "../app/translation/languages/";
import BrandTabViewComponent from "../app/screens/stores/BrandScreenComponents/BrandTabViewComponent";
import ContactDesign1 from "../app/screens/contact/ContactComponents/ContactDesign1";
import ServiceSimpleListComponent from "../app/screens/services/ServiceScreenComponents/ServiceSimpleListComponent";
import HomeGridComponent from "../app/screens/home/HomeComponents/HomeGridComponent";
import WayfinderComponent1 from "../app/screens/navigation/WayfinderComponents/WayfinderComponent1";
import { OfferListItem1 } from "../app/components/offers";
import BrandDetailDesign1 from "../app/screens/stores/BrandDetailComponents/BrandDetailDesign1";
import { EventListItem1 } from "../app/components/events/EventListItem1";
import CinemaDetailComponent1 from "../app/screens/cinema/CinamaDetailComponents/CinemaDetailComponent1";
import AtoZListScreen from "../app/screens/stores/AtoZListComponents/AtoZLargeListScreen";
import UpdateProfileComponent1 from "../app/screens/profile/UpdateUserComponents/UpdateProfileComponent1";

export const DEBUG_MODE = __DEV__;

const configuration = {
    /**
     *
     * Api URL And Default Info Data
     */
    MALL_NAME: "",
    BASE_URL: "",
    API_URL: "",
    INFO_DATA: () => {
        return {
            language: I18n.locale,
            device: Platform.OS,
            env: __DEV__ ? "development" : "production",
            push_token: this.pushToken || "",
            userid: this.userid || ""
        };
    },

    region: {
        identifier: "KNS",
        uuid: "",
        major: ""
    },

    BASE_COLOR: "#fff",
    BASE_SECOND_COLOR: "#ccc",
    BASE_THIRD_COLOR: "#8d8ccc",

    /**
     *
     * Default Navbar Style
     */

    DEFAULT_NAVBAR_COLOR: "#ccc",
    headerTitleStyle: {
        color: "#fff",
    },
    /**
     *
     * Default Status Bar Color for Android and Status Bar Style Both
     */

    DEFAULT_STATUS_BAR_COLOR: "#ccc",
    DEFAULT_STATUS_BAR_STYLE: "light-content",

    /**
     *
     * Default BackgroundColor For Main Screens
     */

    DEFAULT_BACKGROUND_COLOR: "#f3f3f3",

    /**
     *
     * Default Ripple Effect Color for View and Buttons in Android
     */

    DefaultRippleColor: "#d5d5d5",

    /**
     *
     * Default Button Color
     */

    DEFAULT_BUTTON_COLOR: "#ccc",
    DEFAULT_BUTTON_HIGHLIGHT_COLOR: "#d2ad57",

    /**
     *
     * Default http parameter suffix
     */

    DEFAULT_HTTP_SUFFIX: () => {
        return `?language=${I18n.locale}&device=${Platform.OS}`;
    },

    /**
     *
     * beacon control active or passive
     */
    beaconMode: false,

    /**
     *
     * you wanna listen to bluetooth then go to return true
     */
    listenToBluetooth: false,

    setPushToken: (pushToken) => {
        this.pushToken = pushToken;
        updateConfigureMallFrameClient();
    },
    getPushToken: () => {
        return this.pushToken;
    },

    setUserID: (userid) => {
        this.userid = userid;
        updateConfigureMallFrameClient();
    },
    getUserID: () => {
        return this.userid;
    },
    setClientId: (client_id) => {
        this.client_id = client_id;
        updateConfigureMallFrameClient();
    },
    getClientId: () => {
        return this.client_id || "";
    },

    PlaceHolderImage: require("../assets/images/placeholder_store.png"),
    AnonymousUserImage: require("../assets/images/anonymous_user.png"),

    SideBarScreen: {
        Enabled: false,
        Component: false,
        BackgroundImage: require("../assets/images/menu_background.png"),
        ChangeLanguage: false,
    },

    SplashScreen: {
        Enabled: false, // if SplashScreen not enable you must add backgroundImage
        Component: false,
        BackgroundImage: false,
    },

    HomeScreen: {
        Component: HomeGridComponent,
        Refreshable: true,
        BoxHeight: 150,
        SwiperHeight: false,
        SearchBarBackgroundImage: require("../assets/images/search_bg.png"),
        DefaultGridType: [
            [{
                routeName: "Brands",
                backgroundImage: require("../assets/images/directory_bg.png"),
                icon: require("../assets/icons/directory.png"),
                name: "Brands"
            }],
            [
                {
                    routeName: "Events",
                    swiper: true
                },
            ],
            [{
                routeName: "Fun",
                backgroundImage: require("../assets/images/entertainment_bg.png"),
                icon: require("../assets/icons/entertainment.png"),
                name: "Entertainment"
            }],
            [
                {
                    routeName: "Offers",
                    swiper: true
                },
            ],
            [{
                routeName: "Parking",
                backgroundImage: require("../assets/images/parking_bg.png"),
                icon: require("../assets/icons/parking.png"),
                name: "Parking"
            }],
            [{
                routeName: "AboutUs",
                backgroundImage: require("../assets/images/aboutus_bg.png"),
                icon: require("../assets/icons/aboutus.png"),
                name: "AboutUs"
            }],
        ],
    },

    BrandScreen: {
        Component: BrandTabViewComponent,
        ListTab: true,
        CategoryTab: true,
        LevelTab: true
    },

    AtoZListScreen: {
        Component: AtoZListScreen
    },

    BrandListScreen: {
        Enabled: false,
        Component: false,
    },

    BrandDetailScreen: {
        Component: BrandDetailDesign1,
    },

    BrandListSubCategoryScreen: {
        Enabled: false,
        Component: false,
    },

    BrandImageGalleryScreen: {
        Enabled: false,
        Component: false,
    },

    BrandListItemComponent: {
        enabled: false,
        Component: false
    },

    WayfinderScreen: {
        Component: WayfinderComponent1,
        HeaderImage: require("../assets/images/wayfinder_header.png")
    },
    WayfinderStoreListScreen: {
        Enabled: false,
        Component: false,
    },
    MallMapScreen: {
        Enabled: false,
        Component: false,
        SearchBar: false
    },

    CafeRestaurantScreen: {
        Enabled: false,
        Component: false,
        CafeRestaurantCategoryId: false,
    },

    EventScreen: {
        Enabled: false,
        Component: false,
    },
    EventDetailScreen: {
        Enabled: false,
        Component: false,
        BackgroundColor: "#fff"
    },
    EventListItemComponent: {
        Component: EventListItem1
    },

    OfferScreen: {
        Enabled: false,
        Component: false,
    },
    OfferDetailScreen: {
        Enabled: false,
        Component: false,
        BackgroundColor: "#fff"
    },
    OfferListItemComponent: {
        Component: OfferListItem1
    },

    EntertainmentScreen: {
        Enabled: false,
        Component: false,
        FunCategoryId: false,
    },

    ServiceScreen: {
        Component: ServiceSimpleListComponent,
        DetailEnabled: true
    },
    ServiceDetailScreen: {
        Enabled: false,
        Component: false,
    },

    DiningScreen: {
        Enabled: false,
        Component: false,
        DiningCategoryId: false,

    },

    CinemaScreen: {
        Enabled: false,
        Component: false,
    },
    CinemaStoreScreen: {
        Enabled: false,
        Component: false,
    },
    CinemaDetailScreen: {
        Component: CinemaDetailComponent1,
    },

    MagazineScreen: {
        Enabled: false,
        Component: false,
    },
    MagazineDetailScreen: {
        Enabled: false,
        Component: false,
    },

    ParkingScreen: {
        Enabled: false,
        Component: false,
    },
    CarParkingPhotoScreen: {
        Enabled: false,
        Component: false,
    },
    AudioRecordParkingScreen: {
        Enabled: false,
        Component: false,
    },

    ContactScreen: {
        Component: ContactDesign1,
    },

    AboutScreen: {
        Enabled: false,
        Component: false,
    },

    HowToGetHereScreen: {
        Enabled: false,
        Component: false,
    },

    BrowserScreen: {
        Enabled: false,
        Component: false,
    },

    WhatsNewScreen: {
        Enabled: false,
        Component: false,
    },
    WhatsNewDetailScreen: {
        Enabled: false,
        Component: false,
    },

    UpdateProfileScreen: {
        Component: UpdateProfileComponent1,
        PhoneVerify: {
            Enabled: false,
        }
    },
    RegisterScreen: {
        Enabled: false,
        Component: false,
    },
    ForgetPasswordScreen: {
        Enabled: false,
        Component: false,
    },
    ProfileScreen: {
        Enabled: false,
        Component: false,
    },
    LoginScreen: {
        Enabled: false,
        Component: false,
    },

    floorDimensionWidth: null,
    floorDimensionHeight: null,

    LoginWithFacebook: true,
    LoginWithGoogle: false,
    SocialMustRegister: false,
    GoogleConfiguration: {
        iosClientId: "",
        webClientId: "",
        offlineAccess: false
    },

    NavigationWay: "pedestrian",
    UseMultipleNavigationWay: false,
    MapHtmlFile: Platform.select({
        ios: require("../map/map.html"),
        android: DEBUG_MODE ? require("../map/map.html") : { uri: "file:///android_asset/map/map.html" }
    }),

    storesKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        name: "name",
        explanation: "explanation",
        logo: "logo",
        image: "facade",
        categories: "categories",
        locations: "locations",
        floor_names: "floor_names",
        web: "web",
        email: "email",
        phone: "phone",
    },
    offersKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        store_id: "store_id",
        title: "title",
        explanation: "explanation",
        image: "image",
        url: "url",
        start_date: "start_date",
        end_date: "end_date",
    },
    eventsKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        store_id: "store_id",
        title: "title",
        explanation: "explanation",
        image: "image",
        url: "url",
    },
    promotionsKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        store_id: "store_id",
        title: "title",
        subtitle: "subtitle",
        explanation: "explanation",
        image: "image",
        url: "url",
    },
    newsKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        store_id: "store_id",
        title: "title",
        explanation: "explanation",
        image: "image",
        url: "url",
    },
    servicesKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        name: "name",
        explanation: "explanation",
        image: "image",
        locations: "locations",
    },
    cinemasKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        store_id: "store_id",
        name: "title",
        explanation: "explanation",
        genre: "genre",
        cast: "cast",
        director: "director",
        duration: "duration",
        poster: "poster",
        trailer: "trailer",
        sessions: "sessions",
        buyTicket: "buy_ticket",
    },
    floorsKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        no: "no",
        name: "name",
        svg: "svg",
        cast: "cast",
        dimension: "dimension",
        duration: "duration",
        locations: "locations",
        scale: "scale",
    },
    categoriesKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        name: "name",
        color: "color",
        is_subcategory: "is_subcategory",
        parent_category: "parent_category",
    },
    userKeymap: {
        enabled: true,
        extra_data: false,
        _id: "_id",
        login_type: "login_type",
        email: "email",
        phone: "phone",
        name: "name",
        surname: "surname",
        birthdate: "birthdate",
        gender: "gender",
        profile_picture: "profile_picture",
        favorite_stores: "favorite_stores",
        favorite_offers: "favorite_offers",
        favorite_events: "favorite_events",
        favorite_promotions: "favorite_promotions",
    },

    DrawerMenuIcon: {
        name: "menu",
        size: 20,
        style: {
            marginLeft: 15
        },
        color: "#fff"
    },

    ProfileMenuIcon: {
        name: "profile",
        size: 18,
        style: {
            marginRight: 15
        },
        color: "#fff"
    },

    DEFAULT_LANGUAGES: [
        {
            lang: "English",
            sort: "EN"
        },
        {
            lang: "Turkish",
            sort: "TR"
        },
    ],

    DEFAULT_LANGUAGE: "en",
    languages: Languages
};

let mergedConfig = configuration;

const config = () => {
    return mergedConfig;
};

const updateConfigureMallFrameClient = () => {
    RNMallFrameClient.configure({
        url: config().API_URL || "",
        userid: config()
            .getUserID() || "",
        push_token: config()
            .getPushToken() || "",
        client_id: config()
            .getClientId() || "",
        language: I18n.locale,
    });
};

const controlConfiguration = (newProp) => {
    screens.forEach((screen) => {
        if (newProp[screen]) {
            const { Enabled, Component } = newProp[screen];
            if (Enabled && !Component) {
                throw new Error(`If you enabled ${screen} screen then you must declare component.`);
            }
        }
    });

};

export const UpdateNewConfig = (newProp) => {
    mergedConfig = newProp;

    controlConfiguration(newProp);
    mergedConfig = _.assignInWith(configuration, newProp, customizer);

    function customizer(objValue, srcValue) {
        if (objValue !== undefined && srcValue !== undefined && typeof objValue === "object" && typeof srcValue === "object") {
            _.assignWith(objValue, srcValue, customizer);
            return objValue;
        }
        return _.isUndefined(srcValue) ? objValue : srcValue;
    }
};

export default config;
