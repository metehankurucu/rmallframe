/* eslint-disable react/prop-types,import/prefer-default-export */
import React from "react";
import {Dimensions, Text, TouchableOpacity, View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {Card} from "../common";
import {getDefaultLanguageText} from "../../translation/translate";
import {Normalize} from "../../../config/helpers";

const {width} = Dimensions.get("window");

export const CinemaItem = ({cinema, onPress}) => {
    const styles = {
        cardContainer: {
            margin: 5,
            width: width / 2 - 10,
            borderRadius: 8,
            backgroundColor: "#fff",
            shadowColor: "#222",
            shadowOpacity: 0.3,
            shadowOffset: {
                width: 0,
                height: 2
            },
            elevation: 2
        },
        titleStyle: {
            textAlign: "center",
            color: "#000",
            fontSize: 12
        },
        textContainer: {
            paddingLeft: 15,
            paddingRight: 15,
            height: 50,
            justifyContent: "center",
            alignItems: "center",
        },
        imageStyle: {
            height: Normalize(240),
            width: "100%",
            resizeMode: "cover"
        },
    };

    const {poster, name} = cinema;

    return (
        <Card style={styles.cardContainer}>
            <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
                <CachedImage style={styles.imageStyle} source={{uri: poster}}/>
                <View style={styles.textContainer}>
                    <Text numberOfLines={2} style={styles.titleStyle}>{getDefaultLanguageText(name)}</Text>
                </View>
            </TouchableOpacity>
        </Card>
    );
};


