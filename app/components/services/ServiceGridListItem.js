import React from "react";
import {Text, View} from "react-native";
import {CachedImage} from "react-native-img-cache";

import {getDefaultLanguageText, translateText} from "../../translation/translate";
import {Normalize} from "../../../config/helpers";
import {Button} from "../common";
import config from "../../../config/config";

const ServiceGridListItem = ({onPress, data}) => {
    const styles = {
        cardContainer: {
            backgroundColor: "#ffffff",
            margin: 5,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
            shadowColor: "#222",
            shadowOpacity: 0.3,
            shadowOffset: {
                width: 0,
                height: 2
            },
            elevation: 2
        },
        titleStyle: {
            textAlign: "center",
        },
        navigationTitleStyle: {
            textAlign: "center",
            fontSize: 12,
            padding: 3,
            color: "#fff",
        },
        textContainer: {
            justifyContent: "center",
        },
        imageStyle: {
            width: Normalize(50),
            height: Normalize(50),
            resizeMode: "contain"
        },
        imageContainer: {
            backgroundColor: config().BASE_SECOND_COLOR,
            width: Normalize(50),
            alignSelf: "center",
            margin: 15,
        },
        actionTextStyle: {
            height: 35,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: config().BASE_SECOND_COLOR
        }
    };

    return (
        <View style={styles.cardContainer}>
            <Button onPress={onPress}>
                <View>
                    <View style={styles.imageContainer}>
                        <CachedImage
                            style={styles.imageStyle}
                            source={{uri: data.image}}
                        />
                    </View>
                    <View style={styles.textContainer}>
                        <Text numberOfLines={1} style={styles.titleStyle}>{getDefaultLanguageText(data.name)}</Text>
                    </View>
                </View>
                {
                    data.locations.length > 0 ? (
                        <View style={styles.actionTextStyle}>
                            <Text
                                numberOfLines={1}
                                style={styles.navigationTitleStyle}
                            >
                                {
                                    translateText("GetDirections")
                                        .toLocaleUpperCase("tr")
                                }
                            </Text>
                        </View>
                    ) : (
                        <View style={{height: 30}}/>
                    )
                }
            </Button>
        </View>
    );
};

ServiceGridListItem.propTypes = {};

export default ServiceGridListItem;
