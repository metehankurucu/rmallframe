/* eslint-disable react/prop-types */
import React from "react";
import {Text, View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {Button} from "../common";
import {getDefaultLanguageText} from "../../translation/translate";
import {Normalize} from "../../../config/helpers";
import config from "../../../config/config";

const styles = {
    cardContainer: {
        backgroundColor: "#ffffff",
        margin: 5,
    },
    titleStyle: {
        textAlign: "center",
        fontSize: 15,
        height: 40
    },
    textContainer: {
        padding: 15,
        justifyContent: "center",
    },
    imageStyle: {
        width: "100%",
        height: Normalize(130),
        resizeMode: "cover"
    },
};

const OfferListItem1 = ({offer, onPress}) => {
    const {PlaceHolderImage} = config();

    return (
        <View style={styles.cardContainer}>
            <Button onPress={onPress}>
                <View>
                    <CachedImage
                        style={styles.imageStyle}
                        source={{uri: offer.image}}
                        defaultSource={PlaceHolderImage}
                    />
                    <View style={styles.textContainer}>
                        <Text numberOfLines={2} style={styles.titleStyle}>{getDefaultLanguageText(offer.title)}</Text>
                    </View>
                </View>
            </Button>
        </View>
    );

};

export {OfferListItem1};


