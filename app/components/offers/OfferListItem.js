/* eslint-disable react/prop-types */
import React from "react";
import config from "../../../config/config";

const OfferListItem = ({offer, onPress}) => {
    const {Component} = config().OfferListItemComponent;

    return (
        <Component offer={offer} onPress={onPress}/>
    );
};

export {OfferListItem};


