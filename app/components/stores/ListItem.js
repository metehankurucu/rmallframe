import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CachedImage } from "react-native-img-cache";
import PropTypes from "prop-types";
import { getDefaultLanguageText } from "../../translation/translate";
import { Card, Icon } from "../common";
import config from "../../../config/config";

const styles = {
    rootContainer: {
        backgroundColor: "#fff",
        borderBottomWidth: 1,
        borderColor: "#ccc",
        padding: 10,
        height: 85,
        zIndex: 1
    },
    container: {
        marginLeft: 15,
        flexDirection: "row",
        justifyContent: "space-around",
        flex: 1,
        height: "100%",
        width: "100%"
    },
    storeText: {
        fontSize: 16,
        textAlign: "left"
    },
    floorText: {
        fontSize: 13,
        fontWeight: "400",
        color: "#ccc",
    },
    logoImage: {
        resizeMode: "contain",
        width: 64,
        height: 64,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#000"
    },
    textContainer: {
        flexDirection: "column",
        justifyContent: "center",
        flex: 1
    },
    floorContainer: {
        flexDirection: "row"
    },
    favImage: {
        width: 20,
        height: 20
    }
};

const ListItem = ({ key, firstItemSection, onPress, item, onFavPress }) => {
    const { PlaceHolderImage, BrandListItemComponent } = config();
    const { Enabled, Component } = BrandListItemComponent;

    if (!Enabled) {
        return (
            <Card style={[styles.rootContainer, { borderTopWidth: firstItemSection ? 1 : 0 }]} key={key}>
                <TouchableOpacity removeClippedSubviews onPress={onPress} activeOpacity={0.7}>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{
                            width: 64,
                            height: 64,
                        }}
                        >
                            <CachedImage
                                source={{ uri: item.logo }}
                                defaultSource={PlaceHolderImage}
                                style={styles.logoImage}
                            />
                        </View>
                        <View style={styles.container}>
                            <View style={styles.textContainer}>
                                <Text
                                    style={styles.storeText}
                                    numberOfLines={1}
                                >{getDefaultLanguageText(item.name)}
                                </Text>
                                <View style={styles.floorContainer}>
                                    <CachedImage
                                        style={{
                                            width: 12,
                                            height: 12,
                                            alignSelf: "center"
                                        }}
                                        source={require("../../../assets/icons/store_map_icon.png")}
                                    />
                                    <Text
                                        style={styles.floorText}
                                        numberOfLines={1}
                                    >{getDefaultLanguageText(item.floor_names)}
                                    </Text>
                                </View>
                            </View>
                            <View style={{ alignSelf: "center" }}>
                                <Icon name={item.fav ? "heart" : "heart-line"} size={21} color="#e10000" />
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </Card>
        );
    }

    return (
        <Component key={key} firstItemSection={firstItemSection} onPress={onPress} item={item} onFavPress={onFavPress} />
    );
};

ListItem.propTypes = {
    key: PropTypes.string,
    firstItemSection: PropTypes.bool,
    onPress: PropTypes.func,
    item: PropTypes.instanceOf(Object)
};

export default ListItem;
