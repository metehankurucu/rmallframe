import React from 'react';
import { Text, View, StyleSheet } from "react-native";

const ListHeader = ({ title }) => (
    <View style={styles.rootContainer}>
        <Text style={styles.storeLetter}>{title}</Text>
    </View>
);

const styles = StyleSheet.create({
    storeLetter: {
        fontWeight: '600',
        fontSize: 16,
    },
    rootContainer: {
        padding: 5,
        paddingTop: 2,
        backgroundColor: '#fff',
        borderColor: "#ccc",
        borderBottomWidth: 1,
    },
});

export default ListHeader;