import React from 'react';
import {ActivityIndicator, Modal, Text, View} from 'react-native';

const Loader = props => {
    const {
        loading,
        onRequestClose,
        title,
    } = props;
    return (
        <Modal visible={loading} animationType="fade" transparent={true} onRequestClose={onRequestClose}>
            <View style={styles.container}>
                <View style={styles.textContainer}>
                    <Text style={styles.alertText}>{title}</Text>
                </View>
                <View style={styles.indicatorContainer}>
                    <ActivityIndicator size='large' color='#00BFFF'/>
                </View>
            </View>
        </Modal>
    )
};

const styles = {
    container: {
        marginTop: '65%',
        backgroundColor: '#fff',
        width: '70%',
        height: 150,
        shadowOpacity: 0.2,
        elevation: 1,
        alignSelf: 'center'
    },
    alertText: {
        fontSize: 21,
        fontWeight: 'bold',
        color: '#00BFFF',
        width: '100%',
        margin: 10,
    },
    indicatorContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 120,
    },
    textContainer: {
        borderBottomWidth: 1,
        borderColor: '#717171',
        height: 40,
        padding: 15,
        alignItems: 'center',
        justifyContent: 'center',
    }
};

export default Loader;