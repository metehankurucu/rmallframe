import React from 'react';
import {View} from 'react-native';

const styles = {
    containerStyle: {
        flex: 1,
        flexDirection: "row",
        position: "relative"
    }
};

const CardSection = (props) => {
    return (
        <View style={[styles.containerStyle, props.style]}>
            {props.children}
        </View>
    );
};

export default CardSection;