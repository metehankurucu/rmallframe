import React from 'react';
import {View} from 'react-native';

const Card = ({style, children, tabLabel, key}) => {
    return (
        <View key style={[styles.containerStyle, style]} tabLabel={tabLabel} >
            {children}
        </View>
    );
};

const styles = {
    containerStyle: {
        flex: 1
    }
};

export {Card};