import {createIconSetFromIcoMoon} from "react-native-vector-icons";
import icoMoonConfig from "../../../config/selection.json";

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export {Icon};
