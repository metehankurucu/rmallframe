import React, { Component } from "react";
import { Text, StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import PropTypes from "prop-types";

let Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

class AlphabetPicker extends Component {
    constructor(props, context) {
        super(props, context);
        if (props.alphabet) {
            Alphabet = props.alphabet;
        }
        this.state = {
            alphabet: Alphabet,
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (this.props.alphabet !== nextProps.alphabet) {
            this.setState({ alphabet: nextProps.alphabet });
        }
    }

    _onTouchLetter(letter) {
        letter && this.props.onTouchLetter && this.props.onTouchLetter(letter);
    }

    render() {
        const { alphabet } = this.state;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={styles.wrapper}
            >
                {
                    alphabet.map((letter, index) => (
                        <TouchableOpacity key={index} onPress={() => this._onTouchLetter(letter)}>
                            <Text style={styles.letter}>{letter}</Text>
                        </TouchableOpacity>
                    ))
                }
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    letter: {
        fontSize: 11,
        color: "#ccc",
        padding: 5
    },
    wrapper: {
        paddingHorizontal: 5,
        backgroundColor: "#fff",
        borderRadius: 1,
        flex: 1,
    }
})

AlphabetPicker.propTypes = {
    onTouchLetter: PropTypes.func,
    alphabet: PropTypes.instanceOf(Array),
    onTouchEnd: PropTypes.func,
    onTouchStart: PropTypes.func,
};

export default AlphabetPicker;


