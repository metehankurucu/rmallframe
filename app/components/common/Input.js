import React from "react";
import {TextInput, View} from "react-native";
import PropTypes from "prop-types";

const styles = {
    inputStyle: {
        color: "#000",
        marginRight: 10,
        marginLeft: 10,
        fontSize: 15,
        height: 40,
        width: "100%"
    },
    containerStyle: {
        justifyContent: "center",
    },
    error: {
        width: "100%",
        height: 1,
        marginBottom: 2,
        backgroundColor: "#D9D9D9"
    }
};

const Input = (props) => {
    const {inputStyle, containerStyle, error} = styles;
    const input = {...inputStyle, ...props.inputTextStyle};

    return (
        <View style={[containerStyle, props.style]}>
            <TextInput
                {...props}
                style={input}
                placeholderTextColor="#6b6b6b"
                underlineColorAndroid="rgba(0,0,0,0)"
                autoCorrect={false}
            />
            {!!props.error && <View style={error}/>}
        </View>
    );
};

Input.propTypes = {
    error: PropTypes.string,
    style: PropTypes.instanceOf(Object)
};

export {Input};

