/**
 * Custom WebView with autoHeight feature
 *
 * @prop source: Same as WebView
 * @prop autoHeight: true|false
 * @prop defaultHeight: 100
 * @prop width: device Width
 * @prop ...props
 *
 * @author Elton Jain
 * @version v1.0.2
 */

import React, {Component} from "react";
import {WebView} from "react-native-webview";
import config from "../../../config/config";
import {getDefaultLanguageText} from "../../translation/translate";

const injectedScript = function () {
    function waitForBridge() {
        if (window.postMessage.length !== 1) {
            setTimeout(waitForBridge, 200);
        }
        else {
            let height = 0;
            if (document.documentElement.clientHeight > document.body.clientHeight) {
                height = document.documentElement.clientHeight;
            }
            else {
                height = document.body.clientHeight;
            }
            postMessage(height);
        }
    }

    waitForBridge();
};

export default class AutoHeightWebView extends Component {
    state = {
        webViewHeight: Number,
    };

    static defaultProps = {
        autoHeight: true,
    };

    constructor(props: Object) {
        super(props);

        let url = "";
        if (getDefaultLanguageText(this.props.defaultUrl).includes(config().BASE_URL) === -1) {
            url = config().BASE_URL
        }

        url += `${getDefaultLanguageText(this.props.defaultUrl) + config().DEFAULT_HTTP_SUFFIX()}`;

        this.state = {
            webViewHeight: this.props.defaultHeight,
            url
        };

        this._onMessage = this._onMessage.bind(this);
    }

    browserClosed = () => {
        let url = "";
        if (getDefaultLanguageText(this.props.defaultUrl).includes(config().BASE_URL) === -1) {
            url = config().BASE_URL
        }

        url += `${getDefaultLanguageText(this.props.defaultUrl) + config().DEFAULT_HTTP_SUFFIX()}`;

        this.setState({
            url
        });
    };

    openExternalLink = (req) => {
        // console.log(req);
        const isLocal = req.url.search(this.state.url) !== -1;
        const isBridge = req.url.search("react-js-navigation://") !== -1;

        if (isBridge) {
            return;
        }
        else if (isLocal) {
            this.props.navigation.navigate("Browser", {
                uri: req.url,
                title: req.title,
                browserClosed: this.browserClosed
            });
        }

        if (!req.url.includes(this.props.defaultUrl)) {
            this.stopLoading();
        }
    };

    _onMessage(e) {
        this.setState({
            webViewHeight: parseInt(e.nativeEvent.data) + 30
        });
    }

    stopLoading() {
        this.webview.stopLoading();
    }

    render() {
        const _w = this.props.width || "100%";
        const _h = this.props.autoHeight ? this.state.webViewHeight : this.props.defaultHeight;

        return (
            <WebView
                ref={(ref) => {
                    this.webview = ref;
                }}
                injectedJavaScript={`(${String(injectedScript)})();`}
                scrollEnabled={this.props.scrollEnabled || false}
                onMessage={this._onMessage}
                javaScriptEnabled
                onNavigationStateChange={this.openExternalLink.bind(this)}
                automaticallyAdjustContentInsets

                {...this.props}

                source={
                    this.state.url}
                style={[{
                    width: _w,
                    margin: 0,
                    padding: 0
                }, this.props.style, {height: _h}]}
            />
        );
    }
}
