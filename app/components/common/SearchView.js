import React, { Component } from "react";
import PropTypes from "prop-types";
import { TextInput, View } from "react-native";
import { CachedImage } from "react-native-img-cache";

class SearchView extends Component {
    static defaultProps = {
        onChange() {
        },
        caseSensitive: false,
        fuzzy: false,
        throttle: 100
    };

    constructor(props) {
        super(props);
        this.state = {
            searchTerm: this.props.value || ""
        };
    }


    UNSAFE_componentWillReceiveProps(nextProps) {
        if (typeof nextProps.value !== "undefined" && nextProps.value !== this.props.value) {
            const e = {
                target: {
                    value: nextProps.value
                }
            };
            this.updateSearch(e);
        }
    }

    render() {
        const {
            style, onChange, caseSensitive, sortResults, throttle, filterKeys, value, fuzzy, ...inputProps
        } = this.props;
        inputProps.type = inputProps.type || "search";
        inputProps.value = this.state.searchTerm;
        inputProps.onChange = this.updateSearch.bind(this);
        inputProps.placeholder = inputProps.placeholder || "Search";
        return (
            <View style={this.props.containerStyle}>
                <TextInput
                    underlineColorAndroid="rgba(0,0,0,0)"
                    style={style}
                    {...inputProps} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                />
                <CachedImage
                    source={this.props.searchImage || require("../../../assets/icons/search_icon.png")}
                    style={styles.searchImage}
                />
            </View>
        );
    }

    updateSearch(e) {
        const searchTerm = e.target.value;
        this.setState({
            searchTerm
        }, () => {
            if (this._throttleTimeout) {
                clearTimeout(this._throttleTimeout);
            }

            this._throttleTimeout = setTimeout(
                () => {
                    return this.props.onChange(searchTerm);
                },
                this.props.throttle
            );
        });
    }
}

const styles = {
    searchImage: {
        position: "absolute",
        marginTop: 12,
        width: 16,
        height: 16,
        right: 10
    }
};


SearchView.propTypes = {
    onChange: PropTypes.func,
    caseSensitive: PropTypes.bool,
    sortResults: PropTypes.bool,
    fuzzy: PropTypes.bool,
    throttle: PropTypes.number,
    value: PropTypes.string
};

export { SearchView };
