import React from "react";
import {ActivityIndicator, View} from "react-native";

const Indicator = () => {
    return (
        <View style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center"
        }}
        >
            <ActivityIndicator size="small" color="#222"/>
        </View>
    );
};

export {Indicator};
