/* eslint-disable react/prop-types */
import React from "react";
import {Text, Platform,View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {getDefaultLanguageText} from "../../translation/translate";
import {Button} from "../common";
import {Normalize} from "../../../config/helpers";

const styles = {
    cardContainer: {
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: "#fff",
        borderRadius: 8,
        marginTop: Platform.select({
            android: 15,
            ios: 45
        }),
        marginBottom: 5,
        shadowColor: "#222",
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 2
        },
        elevation: 2
    },
    textContainer: {
        padding: 15,
        justifyContent: "center",
    },
    imageStyle: {
        height: Normalize(100),
        resizeMode: "cover",
        borderRadius: 8,
        marginLeft: 8,
        marginRight: 8,
        marginTop: Platform.select({
            android: 8,
            ios: -30
        })
    },
    titleContainer: {
        margin: 15,
    },
    titleStyle: {
        fontSize: 13
    }
};

const EventListItem2 = ({data, onPress}) => {
    return (
        <View style={styles.cardContainer}>
            <Button
                onPress={onPress}
            >
                <CachedImage style={styles.imageStyle} source={{uri: data.image}}/>
                <View style={styles.titleContainer}>
                    <Text style={styles.titleStyle} numberOfLines={1}>{getDefaultLanguageText(data.title)}</Text>
                </View>
            </Button>
        </View>
    );
};

export {EventListItem2};


