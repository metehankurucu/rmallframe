/* eslint-disable react/prop-types,import/extensions,import/no-unresolved */
import React from "react";
import {Text, View} from "react-native";
import config from "../../../config/config";

const EventListItem = ({data, onPress}) => {
    const {Component} = config().EventListItemComponent;

    return (
        <Component data={data} onPress={onPress}/>
    );
};

export {EventListItem};


