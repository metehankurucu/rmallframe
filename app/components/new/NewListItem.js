import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {Button} from "../common";
import {getDefaultLanguageText} from "../../translation/translate";
import {Normalize} from "../../../config/helpers";

const NewListItem = ({new_item, onPress}) => {
    const styles = {
        cardContainer: {
            backgroundColor: '#ffffff',
            margin: 5,
        },
        titleStyle: {
            textAlign: 'center',
            fontSize: 15,
        },
        textContainer: {
            padding: 15,
            justifyContent: 'center',
        },
        imageStyle: {
            width: '100%',
            height: Normalize(130),
            resizeMode: 'cover'
        },
    };

    return (
        <View style={styles.cardContainer}>
            <Button onPress={onPress}>
                <View>
                    <CachedImage style={styles.imageStyle} source={{uri: new_item.image}}/>
                    <View style={styles.textContainer}>
                        <Text numberOfLines={2} œstyle={styles.titleStyle}>{getDefaultLanguageText(new_item.title)}</Text>
                    </View>
                </View>
            </Button>
        </View>
    );
};



export {NewListItem};


