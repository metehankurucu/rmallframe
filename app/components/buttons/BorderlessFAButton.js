/* eslint-disable react/prop-types */
import {Button} from "../common";
import React from "react";
import {View} from "react-native";
import config from "../../../config/config";

const BorderlessFAButton = ({onPress, style, rippleColor, children}) => {
    const styles = {
        defaultDimension: {
            height: 48,
            width: 48,
            borderRadius: 48,
        },

        container: {
            backgroundColor: config().DEFAULT_BUTTON_COLOR,
            elevation: 3,
            shadowOpacity: 0.5,
            shadowColor: "#000",
            alignItems: "center",
            justifyContent: "center"
        },
        titleContainer: {
            alignItems: "center",
            justifyContent: "center"
        }
    };

    return (
        <Button
            onPress={onPress}
            style={[styles.defaultDimension, styles.container, style]}
            color={rippleColor || config().DefaultRippleColor}
        >
            <View style={[styles.defaultDimension, styles.titleContainer]}>
                {children}
            </View>
        </Button>
    )
};

export {BorderlessFAButton};

