import React from "react";
import PropTypes from "prop-types";
import {Button, Icon} from "../common";

const FavButton = ({
                       fav, onPress, buttonStyle, iconStyle
                   }) => {
    return (
        <Button
            onPress={onPress}
            style={[buttonStyle]}
        >
            <Icon name={fav ? "heart" : "heart-line"} size={21} color="#e10000" style={iconStyle}/>
        </Button>
    );
};

FavButton.propTypes = {
    fav: PropTypes.bool,
    onPress: PropTypes.func,
    buttonStyle: PropTypes.instanceOf(Object),
    iconStyle: PropTypes.instanceOf(Object),
};

FavButton.defaultProps = {
    fav: false,
    onPress: () => {
    },
    buttonStyle: {},
    iconStyle: {},
};

export {FavButton};

