/* eslint-disable no-unused-vars,react-native/split-platform-components */
import {PermissionsAndroid} from "react-native";
import {checkPermission, requestPermission} from "./Permission";

const WRITE_EXTERNAL_STORAGE = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;

const checkExternalStorage = () => {
    return checkPermission(WRITE_EXTERNAL_STORAGE);
};

const requestExternalStoragePermission = async () => {
    if (!checkExternalStorage()) {
        try {
            const granted = await requestPermission(WRITE_EXTERNAL_STORAGE, "We need access file", "We required Storage permission in order to get image");

            return new Promise((resolve, reject) => {
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            });
        }
        catch (err) {
            alert(err);
        }
    }
};

export {requestExternalStoragePermission, checkExternalStorage};

