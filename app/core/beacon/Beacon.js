export default class Beacon {
    showBeacon = 0;
    hideBeacon = 0;
    stable = false;

    constructor({minor, major, rssi, accuracy, proximity, floor_no, node_name}) {
        this.minor = minor;
        this.major = major;
        this.rssi = rssi;
        this.accuracy = accuracy;
        this.proximity = proximity;
        this.floor_no = floor_no;
        this.node_name = node_name;
    }

    update({rssi, accuracy, proximity, floor_no}) {
        this.beaconReceived();

        if (rssi)
            this.rssi = rssi === 0 ? -99 : rssi;

        if (accuracy)
            this.accuracy = accuracy === -1 ? 10 : accuracy;

        if (proximity)
            this.proximity = proximity;

        if (floor_no)
            this.floor_no = floor_no;
    }

    beaconReceived = () => {
        this.showBeacon += 1;
        this.hideBeacon = 0;

        if (this.showBeacon >= 5) {
            this.stable = true;
        }
    };

    beaconNotReceive = () => {
        this.hideBeacon += 1;
        this.showBeacon = 0;

        if (this.hideBeacon >= 5) {
            this.stable = false;
        }
    };

    getJson = () => {
        return {
            minor: this.minor,
            major: this.major,
            rssi: this.rssi,
            accuracy: this.accuracy,
            proximity: this.proximity,
            stable: this.stable,
            floor_no: this.floor_no,
            node_name: this.node_name
        };
    }
}