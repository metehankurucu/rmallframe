import _ from 'lodash';
import Beacon from "./Beacon";

class BeaconManager {
    beacons = [];
    floor_no = 0;
    levelCounter = 0;

    beaconLevelData = (beaconLevelData) => {
        this.beaconExtraData = beaconLevelData;
    };

    recievedBeacons = (beacons) => {
        // first load

        if (this.beacons.length === 0) {
            this.beacons = beacons.map((beacon) => {
                const find = _.find(this.beaconExtraData, { minor: beacon.minor });
                beacon.accuracy = beacon.accuracy || beacon.distance;

                return new Beacon({ ...find, ...beacon });
            });

            return;
        }

        let ReceivedBeacons = beacons;

        if (!!beacons.length) {
            this.beacons = this.beacons.map((beacon) => {
                let newBeacon = _.find(ReceivedBeacons, { minor: beacon.minor });

                // Remove found the beacon
                _.remove(ReceivedBeacons, { minor: beacon.minor });

                if (newBeacon) {
                    newBeacon.accuracy = newBeacon.accuracy || newBeacon.distance;
                    if (!beacon.floor_no) {
                        const find = _.find(this.beaconExtraData, { minor: beacon.minor });
                        newBeacon = { ...newBeacon, ...find }
                    }

                    beacon.update(newBeacon);
                    return beacon;
                } else {
                    // beacon away to area
                    beacon.beaconNotReceive();
                    return beacon;
                }
            });

            // Received new Beacons
            ReceivedBeacons.map((beacon) => {
                const find = _.find(this.beaconExtraData, { minor: beacon.minor });
                beacon.accuracy = beacon.accuracy || beacon.distance;

                this.beacons.push(new Beacon({ ...find, ...beacon }));
            });
        }

        this.calculateStableBeaconLevel();
    };

    calculateStableBeaconLevel = () => {
        if (this.beacons) {
            const beaconsByJson = this.getJsonByBeacons(this.beacons);

            // calculate the level possibilities
            let maxLevel;

            const groupByBeacon = _(beaconsByJson.filter(beacon => beacon.stable))
                .groupBy('floor_no')
                .map((items, key) => ({ floor_no: key, count: items.length }))
                .value();


            maxLevel = _.maxBy(groupByBeacon, 'count');
            // if floor changes wait for 10 times

            if (maxLevel && maxLevel.floor_no) {
                if (this.floor_no !== maxLevel.floor_no) {
                    this.levelCounter += 1;

                    if (this.levelCounter === 3) {
                        this.floor_no = maxLevel.floor_no;
                    }
                } else {
                    this.levelCounter = 0;
                }
            }
        }
    };

    getStableBeacons = () => {
        // console.log("getStableBeacons", this.beacons);
        if (this.beacons) {
            return this.getJsonByBeacons(this.beacons).filter(beacon => beacon.stable && beacon.floor_no == this.floor_no).sort(function (a, b) {
                return a.accuracy !== -1 ? a.accuracy > b.accuracy : false;
            });
        }

        return [];
    };


    getJsonByBeacons = (beacons) => {
        return beacons && beacons.map((beacon) => {
            return beacon.getJson();
        })
    }
}


export default new BeaconManager();