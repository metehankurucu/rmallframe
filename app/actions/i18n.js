import _ from "lodash";
import {LOAD_TRANSLATIONS, SET_LOCALE} from "./types";
import AsyncStorage from "../utils/AsyncStorage";
import config from "../../config/config";

export const loadTranslations = (translations) => {
    return {
        type: LOAD_TRANSLATIONS,
        translations
    };
};

export const setLocale = (locale = 'tr') => {
    if ( locale != 'tr' && locale != 'en') {
        locale = 'tr';
    }
    const {DEFAULT_LANGUAGES} = config();
    const language = _.find(DEFAULT_LANGUAGES, (o) => {
        return locale.includes(o.sort.toLowerCase());
    });

    if (language) {
        AsyncStorage.setItem("language", locale);

        return {
            type: SET_LOCALE,
            locale
        };
    }

};
