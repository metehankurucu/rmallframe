import {SEARCH_TERM} from "./types";

export const SearchTerm = (searchTerm) => {
	return (dispatch) => {
		dispatch({
			type: SEARCH_TERM,
			searchTerm
		})
	}
};