import { Alert } from "react-native";
import { NavigationActions } from "react-navigation";

import { NEW_SP } from "./types";
import Api from "../api/Api";
import { translateText } from "../translation/translate";

export const NewSP = (from, to, type, beacon = false) => {
    const from_id = from && from.id || '';
    const to_id = to && to.id || '';

    return (dispatch) => {
        let spData;
        if (beacon) {
            if (beacon.length === 0) {
                Alert.alert(translateText("error"), translateText("location_failed"));
                return;
            }
            spData = {
                start_location: from_id,
                end_location: to_id,
                beacons: [beacon[0]],
                type
            };
        }
        else {
            spData = {
                start_location: from_id,
                end_location: to_id,
                type
            };
        }
        Api.NewSP(spData)
            .then((response) => {
                if (response.error) {
                    Alert.alert(translateText("Error"), response.error.msg);
                }
                else if (response.msg) {
                    Alert.alert(translateText("Error"), response.msg);
                }
                else {
                    dispatch({
                        type: NEW_SP,
                        payload: response
                    });

                    dispatch(NavigationActions.navigate({
                        routeName: "MallMap",
                        params: {
                            from,
                            to
                        }
                    }));
                }
            });
    };
};

export const resetSP = () => {
    return (dispatch) => {
        dispatch({
            type: NEW_SP,
            payload: false
        });
    };
};
