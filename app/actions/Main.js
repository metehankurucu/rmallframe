import _ from "lodash";
import { Platform } from "react-native";
import I18n from "react-native-i18n";
import RNFetchBlob from "rn-fetch-blob";
import Share from "react-native-share";
import { CHECK_SERVER, CONNECTION_STATUS, DEFAULT_BEACON_DATA, LOAD_SERVER_FAIL, LOAD_SERVER_SUCCESS, LOGIN_USER_SUCCESS } from "./types";
import AsyncStorage from "../utils/AsyncStorage";
import { loadTranslations, setLocale } from "./i18n";
import Api from "../api/Api";
import config from "../../config/config";
import { AllBeaconRequest, BeaconParkingSpot } from "./Beacon";
import { requestExternalStoragePermission } from "../permissions/ExternalStoragePermission";

const dataMapping = (datas, mappingData) => {
    if (Array.isArray(datas)) {
        return datas.map((data) => {
            try {
                const newData = {};
                _.forIn(mappingData, (value, key) => {
                    if (key !== "enabled") {
                        newData[key] = _.get(data, value);
                    }
                });

                if (mappingData["extra_data"]) {
                    return { data, ...newData };
                }

                return newData;
            }
            catch (err) {
                return data;
            }
        });
    } else {
        return dataMapping([datas], mappingData)
    }
};

export const checkServer = () => {
    const { languages } = config();
    return async (dispatch) => {
        return new Promise(async (resolve, reject) => {
            dispatch({ type: CHECK_SERVER });

            try {
                const versionData = await AsyncStorage.getItem("version");
                this.previousVersion = versionData ? JSON.parse(versionData) : null;
            }
            catch (error) {
                this.previousVersion = {};
            }

            try {
                const contentData = await AsyncStorage.getItem("content");
                this.previousContent = contentData ? JSON.parse(contentData) : null;
            }
            catch (error) {
                console.log("error", error);
                this.previousContent = [];
            }

            try {
                const userProfileData = await AsyncStorage.getItem("userProfile");
                this.userProfile = userProfileData ? JSON.parse(userProfileData) : null;
            }
            catch (error) {
                console.log("error", error);
                this.userProfile = {};
            }

            try {
                this.language = await AsyncStorage.getItem("language") || I18n.currentLocale();
            }
            catch (error) {
                console.log("error", error);
                this.language = I18n.currentLocale();
            }

            try {
                this.pushToken = JSON.parse(await AsyncStorage.getItem("push_token"));
            }
            catch (error) {
                console.log("error", error);

                this.pushToken = "";
            }

            try {
                this.parking_spot = JSON.parse(await AsyncStorage.getItem("parking_spot"));
            }
            catch (error) {
                this.parking_spot = "";
            }

            try {
                this.allbeacons = JSON.parse(await AsyncStorage.getItem("allbeacons"));
            }
            catch (error) {
                console.log("error", error);
                this.allbeacons = null;
            }

            if (this.allbeacons === null && config().beaconMode) {
                dispatch(AllBeaconRequest());
            }
            else {
                dispatch({
                    type: DEFAULT_BEACON_DATA,
                    payload: this.allbeacons
                });
            }

            // load parking spot data
            dispatch(BeaconParkingSpot(this.parking_spot));
            // load language
            dispatch(setLocale(this.language));
            // load translations
            dispatch(loadTranslations(languages));
            // load user
            dispatch({
                type: LOGIN_USER_SUCCESS,
                payload: this.userProfile
            });
            // set default Token
            config()
                .setPushToken(this.pushToken);

            if (this.userProfile) {
                config()
                    .setUserID(this.userProfile["_id"]);
            }

            let all = null;

            try {
                all = await Api.getAll({});
            }
            catch (error) {
                console.log("error", error);
            }

            if (all && Object.keys(all.content).length !== 0) {
                this.newContent = all.content;

                if (this.newContent && this.newContent["mall"] === {} && this.newContent["mall"] === {}) {
                    AsyncStorage.setItem("version", "");
                    dispatch(loadError({
                        key: "Error",
                        content: "Unexpected error please reload app."
                    }));
                    reject();
                }

                for (const newKey in this.newContent) {
                    if (this.newContent.hasOwnProperty(newKey)) {
                        if (newKey !== "mall" && newKey !== "parking_spots" && newKey !== "events" && newKey !== "offers") {
                            this.newContent[newKey] = dataMapping(this.newContent[newKey], config()[`${newKey}Keymap`]);
                        }
                    }
                }

                await AsyncStorage.setItem("version", JSON.stringify(all.version));
                await AsyncStorage.setItem("content", JSON.stringify(this.newContent));

                dispatch({
                    type: LOAD_SERVER_SUCCESS,
                    payload: this.newContent
                });

            }
            else {
                dispatch(loadError({
                    key: "warn",
                    content: "Check your connection"
                }));

                if (Object.keys(this.previousContent).length !== 0) {
                    dispatch({
                        type: LOAD_SERVER_SUCCESS,
                        payload: this.previousContent
                    });
                }
                else {
                    dispatch(loadError({
                        key: "Error",
                        content: "Need Internet Connection to First Load."
                    }));
                }
            }

            resolve(true);
        });
    };
};

function loadError(error) {
    return {
        type: LOAD_SERVER_FAIL,
        error
    };
}

export const shareData = (shareOptions) => {

    return () => {
        if (Platform.OS === "android") {
            requestExternalStoragePermission()
                .then((granted) => {
                    if (!granted) {
                        return;
                    }
                    RNFetchBlob.config({
                        fileCache: true
                    })
                        .fetch("GET", shareOptions.url)
                        .then((res) => {
                            return res.readFile("base64");
                        })
                        .then((str) => {
                            Share.open({
                                ...shareOptions,
                                url: `data:image/png;base64,${str}`
                            })
                                .catch(() => {
                                });
                        });
                });
        }
        else {
            RNFetchBlob.config({
                fileCache: true
            })
                .fetch("GET", shareOptions.url)
                .then((res) => {
                    return res.readFile("base64");
                })
                .then((str) => {
                    Share.open({
                        ...shareOptions,
                        url: `data:image/png;base64,${str}`
                    })
                        .catch(() => {
                        });
                });
        }
    };
};

export const setIsConnected = (connection) => {
    return (dispatch) => {
        dispatch({
            type: CONNECTION_STATUS,
            payload: connection
        });
    };
};
