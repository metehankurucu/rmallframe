/* eslint-disable import/no-unresolved,import/extensions */
import { Alert } from "react-native";
import { GoogleSignin } from "@react-native-community/google-signin";
import { NavigationActions } from "react-navigation";
import AsyncStorage from "../utils/AsyncStorage";

import FacebookSDK from "../api/FacebookSDK";
import Api from "../api/Api";
import { LOGIN_USER_SUCCESS, USER_LOGOUT } from "./types";
import config from "../../config/config";
import { translateText } from "../translation/translate";

export const saveProfilePhoto = (userid, data) => {
    return (dispatch) => {
        Api.saveImage(userid, data)
            .then((response) => {
                if (response.error && response.error.msg) {
                    Alert.alert(translateText("error"), response.error.msg);
                }
                else {
                    response.data.isLoggedIn = true;
                    AsyncStorage.setItem("userProfile", JSON.stringify(response.data));

                    dispatch({
                        type: LOGIN_USER_SUCCESS,
                        payload: response.data
                    });

                }
            });
    };
};

export const registerUser = (data) => {
    return (dispatch) => {
        Api.RegisterUser(data)
            .then((response) => {
                if (response.error && response.error.msg) {
                    Alert.alert(translateText("error"), response.error.msg);
                }
                else {
                    if (response.data.login_type === "facebook") {
                        // login with facebook auto login
                        response.data.isLoggedIn = true;
                        LoginResponse(dispatch, response);
                        dispatch(NavigationActions.back());
                    }
                    else {
                        // login with email redirect login
                        response.data.isLoggedIn = false;
                        if (response.msg) {
                            Alert.alert(translateText("success"), response.msg);
                        }

                        dispatch(NavigationActions.back());
                        return;
                    }
                    AsyncStorage.setItem("userProfile", JSON.stringify(response.data));

                }
            })
            .catch(() => {
                Alert.alert(translateText("warn"), "Check your connection!");
            });
    };
};

export const facebookLogin = () => {
    const { SocialMustRegister } = config();

    return (dispatch) => {
        FacebookSDK.login((response) => {
            FacebookSDK.api("/me", { fields: "id,email,first_name,last_name,gender,cover,link" }, (infoResponse) => {
                if (!response.error) {
                    Api.LoginWithFacebook(response, infoResponse)
                        .then((res) => {
                            if (SocialMustRegister) {
                                const user = infoResponse;
                                user.social_token = response.authResponse.accessToken;

                                dispatch(NavigationActions.navigate({
                                    routeName: "Register",
                                    params: { user }
                                }));
                            }
                            else {
                                LoginResponse(dispatch, res);
                            }
                        });
                }
            });
        }, "email,public_profile");
    };
};

export const googleLogin = () => {
    const { GoogleConfiguration } = config();

    return async (dispatch) => {
        await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
        await GoogleSignin.configure(GoogleConfiguration);
        GoogleSignin.signIn()
            .then(({ user }) => {

                Api.LoginWithGoogle(user)
                    .then((res) => {
                        const { SocialMustRegister } = config();

                        if (SocialMustRegister) {
                            dispatch(NavigationActions.navigate({
                                routeName: "Register",
                                params: { user }
                            }));
                        }
                        else {
                            LoginResponse(dispatch, res);
                        }
                    });
            })
            .catch((err) => {
                console.log("WRONG SIGNIN", err);
            })
            .done();
    };
};

export const loginUser = (data) => {
    return (dispatch) => {
        Api.LoginUser(data)
            .then((response) => {
                LoginResponse(dispatch, response);
            })
            .catch(() => {
                Alert.alert(translateText("warn"), "Check your connection!");
            });
    };
};

export const forgetPassword = (data) => {
    return () => {
        Api.ForgetPassword(data)
            .then((response) => {
                if (response.error && response.error.msg) {
                    Alert.alert(translateText("error"), response.error.msg);
                }
                else if (response.msg) {
                    Alert.alert(translateText("success"), response.msg.toString());
                }
            })
            .catch(() => {
                Alert.alert(translateText("warn"), "Check your connection!");
            });
    };
};

export const logoutUser = () => {
    return (dispatch) => {

        dispatch({
            type: USER_LOGOUT,
        });

        AsyncStorage.removeItem("userProfile");
    };
};

export const updateUserSuccess = (dispatch, user) => {
    dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: user
    });

    dispatch(NavigationActions.back());
    dispatch(NavigationActions.navigate({
        routeName: "Profile"
    }));
};

export const updateUser = (data, user) => {
    return (dispatch) => {
        Api.UpdateUser(data, user)
            .then((response) => {

                if (response.error && response.error.msg) {
                    Alert.alert(translateText("error"), response.error.msg);
                }
                else {
                    response.data.isLoggedIn = true;
                    AsyncStorage.setItem("userProfile", JSON.stringify(response.data));

                    // update user id => config
                    config()
                        .setUserID(response.data["_id"]);

                    dispatch({
                        type: LOGIN_USER_SUCCESS,
                        payload: response.data
                    });

                    dispatch(NavigationActions.back());

                    Alert.alert(translateText("success"));
                }
            })
            .catch((wee) => {
                // console.log("wee",wee);
                Alert.alert(translateText("warn"), "Check your connection!");
            });
    };
};

function LoginResponse(dispatch, response) {
    if (response.error && response.error.msg) {
        Alert.alert(translateText("error"), response.error.msg);
    }
    else {
        response.data.isLoggedIn = true;
        AsyncStorage.setItem("userProfile", JSON.stringify(response.data));

        updateUserSuccess(dispatch, response.data);
        if (response.msg) {
            Alert.alert(translateText("success"), response.msg);
        }
    }
}

