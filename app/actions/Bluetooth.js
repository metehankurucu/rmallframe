import { BLUETOOTH_STATUS } from "./types";
import { translateText } from "../translation/translate";
import { requestBluetoothPermission } from "../permissions";
import { Alert, Platform } from "react-native";
import BluetoothManager from "react-native-bluetooth-listener";
import AsyncStorage from "../utils/AsyncStorage";
import RNMallFrameClient from "../../index";

export const StatusChanged = (enabled) => {
    return (dispatch) => {
        dispatch({
            type: BLUETOOTH_STATUS,
            payload: enabled
        })
    }
};

export const ActionBluetoothPermission = (must_ask_again: boolean = false, callback) => {
    let status = {
        error: true,
        msg: ""
    };

    return async () => {
        let bluetoothPermissionDialog = "ask_again";

        try {
            bluetoothPermissionDialog = await AsyncStorage.getItem("bluetooth_permission");
        }
        catch (err) {
            bluetoothPermissionDialog = "ask_again";
        }

        if (must_ask_again && bluetoothPermissionDialog !== "OK" || bluetoothPermissionDialog === "ask_again") {
            Alert.alert(
                translateText("bluetooth_dialog_title"),
                translateText("bluetooth_permission_explanation"),
                [
                    {
                        text: "Ask me later", onPress: () => {
                            AsyncStorage.setItem("bluetooth_permission", "ask_again");
                            if (callback) {
                                callback(status);
                            }
                        }
                    },
                    {
                        text: "Cancel", onPress: () => {
                            AsyncStorage.setItem("bluetooth_permission", "cancel");
                            if (callback) {
                                callback(status);
                            }
                        }, style: "cancel"
                    },
                    {
                        text: "OK", onPress: async () => {
                            let isGranted = await permissionIsGranted(must_ask_again);
                            if (isGranted) {
                                AsyncStorage.setItem("bluetooth_permission", "OK");
                                if (BluetoothManager)
                                    await BluetoothManager.enable(true);
                                await RNMallFrameClient.gpsEnable();

                                if (BluetoothManager) {
                                    const resp = await BluetoothManager.getCurrentState();

                                    let { connectionState } = resp.type;

                                    if (Platform.OS === "ios") {
                                        callbackMessage(callback, connectionState);
                                    }
                                }

                                setTimeout(() => {
                                    status.error = false;
                                    if (callback) {
                                        callback(status);
                                    }
                                }, 500);
                            }
                            else {
                                if (callback && must_ask_again) {
                                    status.message = translateText("activate_to_location");
                                    callback(status);
                                }
                            }
                        }
                    },],
                { cancelable: true }
            );
        }
        else if (bluetoothPermissionDialog === "OK") {
            let isGranted = await permissionIsGranted();

            if (isGranted) {
                await BluetoothManager.enable(true);
                await RNMallFrameClient.gpsEnable();

                const resp = await BluetoothManager.getCurrentState();

                let { connectionState } = resp.type;
                if (Platform.OS === "ios") {
                    callbackMessage(callback, connectionState);
                }

                setTimeout(() => {
                    status.error = false;
                    if (callback) {
                        callback(status);
                    }
                }, 500);

            }
            else {
                if (callback && must_ask_again) {
                    status.message = translateText("activate_to_location");
                    callback(status);
                }
            }
        }
        else {
            if (must_ask_again && callback) {
                callback(status);
            }
        }
    }
};

export const permissionIsGranted = async (withALert) => {
    let isGranted;

    if (Platform.OS === "ios") {
        isGranted = await RNMallFrameClient.getCurrentState();

        if (!isGranted && withALert) {
            Alert.alert(translateText("error"), translateText("activate_to_location"));
        }
    }
    else {
        isGranted = await requestBluetoothPermission();
    }

    return isGranted;
};

const callbackMessage = (callback, connectionState) => {
    let status = {
        error: true,
        msg: ""
    };

    if (callback) {
        if (connectionState === "on") {
            callback({
                error: false,
                message: "success"
            });
        }
        else {
            status.message = translateText("activate_to_bluetooth");
            callback(status);
        }
    }
};
