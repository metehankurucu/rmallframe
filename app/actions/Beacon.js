import { Alert } from "react-native";
import Api from "../api/Api";
import { beacon_udid } from "../../config/config";
import AsyncStorage from "../utils/AsyncStorage";
import { BEACON_LIVE, BEACON_PARKING, BEACON_VISITED, BEACON_VISITED_SHOWED, DEFAULT_BEACON_DATA } from "./types";
import BeaconManager from "../core/beacon/BeaconManager";
import { translateText } from "../translation/translate";

export const BeaconLiveRequest = (beacons) => {
    return async () => {
        if (beacons && beacons.length > 0) {
            await Api.BeaconLive({
                uuid: beacon_udid,
                beacons
            });

            // its background search not necessary error alert.
            // if (response.error) {
            //     Alert.alert('Error', response.error.msg);
            // } else if (response.msg) {
            //     Alert.alert('Error', response.msg)
            // }
        }
    };
};

export const AllBeaconRequest = () => {
    return async (dispatch) => {
        const response = await Api.AllBeacon({
            uuid: beacon_udid
        });

        if (response.error) {
            // Alert.alert('Error', response.error.msg);
        }
        else if (response.msg) {
            // Alert.alert('Error', response.msg);
        }
        else {
            dispatch({
                type: DEFAULT_BEACON_DATA,
                payload: response
            });

            AsyncStorage.setItem("allbeacons", JSON.stringify(response));
        }
    };
};

export const ActiveBeacons = (beacons) => {
    return (dispatch) => {
        BeaconManager.recievedBeacons([...beacons]);

        dispatch({
            type: BEACON_LIVE,
            payload: BeaconManager.getStableBeacons()
        });
    };
};

export const BeaconVisitedRequest = (beacon) => {
    return async (dispatch) => {
        if (beacon) {

            const response = await Api.BeaconVisited({
                uuid: beacon_udid,
                beacons: [
                    beacon
                ]
            });

            if (response.error) {
                Alert.alert(translateText("error"), response.error.msg);
            }
            else if (response.msg) {
                Alert.alert(translateText("error"), response.msg);
            }
            else {
                dispatch({
                    type: BEACON_VISITED,
                    payload: response
                });
            }
        }
    };
};

export const VisitedLocationShowed = () => {
    return (dispatch) => {
        dispatch({
            type: BEACON_VISITED_SHOWED
        });
    };
};

export const BeaconParkingSpot = (parkingSpot) => {
    AsyncStorage.setItem("parking_spot", JSON.stringify(parkingSpot));
    AsyncStorage.removeItem("qr_code");

    return (dispatch) => {
        dispatch({
            type: BEACON_PARKING,
            payload: parkingSpot
        });
    };
};

export const BeaconParkingRequest = (beacon, callback) => {
    return async (dispatch) => {
        if (beacon) {

            const response = await Api.BeaconParking({
                uuid: beacon_udid,
                beacons: [
                    beacon
                ]
            });

            if (callback && response.parking_spots) {
                callback(response.parking_spots);
            }

            if (response.error) {
                Alert.alert(translateText("error"), response.error.msg);
            }
            else if (response.msg) {
                Alert.alert(translateText("error"), response.msg);
            }
            else {
                dispatch({
                    type: BEACON_VISITED,
                    payload: {
                        node: response.node,
                        coordinate: response.coordinate
                    }
                });
            }
        }
    };
};
