import {applyMiddleware, createStore} from "redux";
import {AsyncStorage} from "react-native";
import _ from "lodash";
import FilesystemStorage from "redux-persist-filesystem-storage";
import RNFetchBlob from "rn-fetch-blob";
import {getStoredState, persistStore} from "redux-persist";
import thunk from "redux-thunk";
import logger from "redux-logger";
import rootReducer from "../reducers";

const Store = function configureStore(reducers, middleware) {
    const store = __DEV__ ? createStore(
        rootReducer(reducers),
        applyMiddleware(thunk, logger, middleware),
    ) : createStore(
        rootReducer(reducers),
        applyMiddleware(thunk, middleware)
    );

    const fsPersistor = persistStore(
        store,
        {
            storage: FilesystemStorage.config({
                storagePath: `${RNFetchBlob.fs.dirs.DocumentDir}/persistStore`,
            })
        },
        async (fsError, fsResult) => {
            if (_.isEmpty(fsResult)) {
                // if state from fs storage is empty try to read state from previous storage
                try {
                    const asyncState = await getStoredState({storage: AsyncStorage});
                    if (!_.isEmpty(asyncState)) {
                        // if data exists in `AsyncStorage` - rehydrate fs persistor with it
                        fsPersistor.rehydrate(asyncState, {serial: false});
                    }
                }
                catch (getStateError) {
                    console.warn("getStoredState error", getStateError);
                }
            }
        }
    );

    return store;
};

export {Store};
