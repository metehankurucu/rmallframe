import {validation} from "./validation";
import {validate} from "validate.js";

export const validatee = (state) => {
    const result = validate(state, validation());
    if (result) {
        Object.keys(result)
            .forEach((key) => {
                result[`${key}_error`] = result[key];
                delete result[key];
            });
        return result;
    }
};
