import React from 'react';
import {translateText} from "../translation/translate";

export const validation = () => {
    return {
        username: {
            length: {
                minimum: 2,
                message: translateText('required_field')
            }
        },
        lastname: {
            length: {
                minimum: 2,
                message: translateText('required_field')
            }
        },
        email: {
            presence: {
                message: translateText('required_field')
            },
            format: {
                pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                message: translateText('valid_email')
            }
        },
        password: {
            presence: {
                message: translateText('required_field')
            },
            length: {
                minimum: 6,
                message: translateText('min_six_char_password')
            }
        },
        password_again: {
            // You need to confirm your password
            presence: true,
            // and it needs to be equal to the other password
            equality: {
                attribute: "password",
                message: translateText('password_dont_match')
            }
        },
    }
}