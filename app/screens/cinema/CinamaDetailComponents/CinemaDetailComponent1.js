import React, { Component } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import Communications from "react-native-communications";
import PropTypes from "prop-types";
import VideoPlayer from "react-native-af-video-player-updated";
import { getDefaultLanguageText, translateText } from "../../../translation/translate";
import config from "../../../../config/config";
import { Button } from "../../../components/common";
import Orientation from "react-native-orientation";

class CinemaDetailComponent1 extends Component {
    UNSAFE_componentWillMount() {
        this.props.navigation.setParams({
            fullscreen: false
        });
    }
    componentDidMount() {
        Orientation.unlockAllOrientations()
    }
    componentWillUnmount() {
        Orientation.lockToPortrait()
    }
    onFullScreen(status) {
        // Set the params to pass in the fullscreen status to navigationOptions
        this.props.navigation.setParams({
            fullscreen: status
        });
    }

    styles = StyleSheet.create({
        button: {
            color: "#fff",
            backgroundColor: config().BASE_COLOR,
            padding: 15,
            textAlign: "center",
            fontSize: 15,
            flex: 1,
            width: "100%"

        },
        buttonGroup: {
            flexDirection: "row",
            paddingTop: 16,
            width: "100%"
        },
        title: {
            fontSize: 18,
            fontWeight: "400",
            alignSelf: "flex-start",
            textAlign: "left"
        },
        explanation: {
            fontSize: 14,
            marginTop: 10
        },
        sessionText: {
            color: "#555",
            fontWeight: "bold",
            padding: 5
        },
        partContainer: {
            marginTop: 15,
            flexDirection: "row"
        },
        defaultText: {
            fontSize: 13,
            color: "#555",
            marginLeft: 10
        },
        defaultTitle: {
            fontSize: 15,
            fontWeight: "600"
        },
        navigationBarStyle: {
            paddingLeft: 10,
            paddingRight: 10
        },
        container: {
            flex: 1
        },
        scrollContainer: {
            paddingLeft: 15,
            paddingRight: 15
        }
    });

    render() {
        const {
            poster, name, explanation, director, genre, cast, sessions, buyTicket, trailer
        } = this.props.navigation.state.params.data;

        return (
            <View style={this.styles.container}>
                <VideoPlayer
                    autoPlay={false}
                    url={trailer}
                    title={getDefaultLanguageText(name)}
                    placeholder={poster}
                    logo={poster}
                    onFullScreen={(status) => {
                        return this.onFullScreen(status);
                    }}
                    rotateOnFullScreen
                />
                <ScrollView style={this.styles.scrollContainer}>
                    <View style={this.styles.partContainer}>

                        <Text style={this.styles.defaultTitle}>{translateText("Genre")}:</Text>
                        <Text style={this.styles.defaultText}>{getDefaultLanguageText(genre)}</Text>

                    </View>
                    <View style={this.styles.partContainer}>
                        <Text style={this.styles.defaultTitle}>{translateText("Director")}:</Text>
                        <Text style={this.styles.defaultText}>{director}</Text>
                    </View>
                    <View style={this.styles.partContainer}>
                        <Text style={this.styles.defaultTitle}>{translateText("Cast")}:</Text>
                        <Text style={this.styles.defaultText}>{cast}</Text>
                    </View>
                    <View style={this.styles.partContainer}>
                        <Text style={this.styles.sessionText}>{sessions.split("/")
                            .join(" ")}
                        </Text>
                    </View>

                    <Text style={this.styles.explanation}>{getDefaultLanguageText(explanation)}</Text>

                    {
                        !!buyTicket && (
                            <View style={{ marginTop: 15 }}>
                                <View style={this.styles.buttonGroup}>
                                    <Button
                                        style={{ width: "100%" }}
                                        onPress={() => {
                                            Communications.web(buyTicket);
                                        }}
                                    >
                                        <Text style={this.styles.button}>
                                            {translateText("Buy Ticket")
                                                .toUpperCase()}
                                        </Text>
                                    </Button>
                                </View>
                            </View>
                        )
                    }

                </ScrollView>
            </View>
        );
    }
}

CinemaDetailComponent1.propTypes = {
    navigation: PropTypes.instanceOf(Object)
};

export default CinemaDetailComponent1;
