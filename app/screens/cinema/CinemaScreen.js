import React from "react";
import {connect} from "react-redux";
import {TouchableOpacity} from "react-native";
import {DrawerActions} from "react-navigation";
import PropTypes from "prop-types";

import config from "../../../config/config";
import {translateText} from "../../translation/translate";
import {CinemaItem} from "../../components/cinema/CinemaItem";
import {GridView, Icon} from "../../components/common";

class CinemaScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();
        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("CINEMA")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("CINEMA")
                .toUpperCase(),
        };
    };

    renderItem = (cinema) => {
        return (
            <CinemaItem
                cinema={cinema}
                onPress={() => {
                    this.props.navigation.navigate("CinemaDetail", {
                        data: cinema,
                        title: cinema.name
                    });
                }}
            />
        );
    };

    render() {
        const {Enabled, Component} = config().CinemaScreen;

        if (!Enabled) {
            return (
                <GridView
                    items={this.props.cinemas}
                    itemsPerRow={2}
                    renderItem={this.renderItem}
                />
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    const {payload} = state.main;
    if (payload !== null) {
        return {
            cinemas: payload.cinemas
        };
    }
    return {cinemas: null};
};

CinemaScreen.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    cinemas: PropTypes.instanceOf(Array),
};

const CinemaRedux = connect(mapStateToProps)(CinemaScreen);

// eslint-disable-next-line import/prefer-default-export
export {CinemaRedux as CinemaScreen};
