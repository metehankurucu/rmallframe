/* eslint-disable import/prefer-default-export */
import React from "react";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import {connect} from "react-redux";
import config from "../../../config/config";
import {getDefaultLanguageText} from "../../translation/translate";

class CinemaDetailScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;

        if (state.params && state.params.fullscreen) {
            return {
                header: null
            };
        }

        return {
            title: `${getDefaultLanguageText(state.params.title)}`.toUpperCase()
        };
    };

    render() {
        const {Component} = config().CinemaDetailScreen;

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

const CinemaDetailRedux = connect(mapStateToProps)(CinemaDetailScreen);

export {CinemaDetailRedux as CinemaDetailScreen};
