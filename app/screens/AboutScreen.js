import React from "react";
import {TouchableOpacity, WebView} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {DrawerActions} from "react-navigation";

import {getDefaultLanguageText, translateText} from "../translation/translate";
import config from "../../config/config";
import {Icon} from "../components/common/";

class AboutScreen extends React.Component {
    static defaultProps = {
        mall: {
            aboutus: ""
        }
    };

    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("ABOUTUS")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("AboutUs")
                .toUpperCase(),
        };
    };

    render() {
        const {Enabled, Component} = config().AboutScreen;

        if (!Enabled) {
            return (
                <WebView
                    source={{uri: config().BASE_URL + getDefaultLanguageText(this.props.mall.aboutus)}}/>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({main}) => {
    const {payload} = main;
    return {mall: payload.mall};
};

AboutScreen.propTypes = {
    mall: PropTypes.object
};

const AboutUsRedux = connect(mapStateToProps)(AboutScreen);

export {AboutUsRedux as AboutScreen};
