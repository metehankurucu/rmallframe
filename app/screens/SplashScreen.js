import React from "react";
import {View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {NavigationActions} from "react-navigation";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import config from "../../config/config";
import {checkServer} from "../actions/Main";

const styles = {
    rootContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        position: "absolute",
        top: 0,
        left: 0,
        height: "100%",
        width: "100%",
    },
    introStyle: {
        height: "100%",
        width: "100%",
    }
};

class SplashScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        this.props.checkServer()
            .then(() => {
                this.props.navigation.dispatch(NavigationActions.navigate({routeName: "Drawer"}));
            });
    }

    render() {
        const {Enabled, Component, BackgroundImage} = config().SplashScreen;

        if (!Enabled) {
            if (!BackgroundImage) {
                throw new Error("You must declare backgroundImage on config file.");
            }

            return (
                <View style={styles.rootContainer}>
                    <View style={styles.container}>
                        <CachedImage
                            source={BackgroundImage}
                            style={styles.introStyle}
                        />
                    </View>

                </View>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

SplashScreen.propTypes = {
    checkServer: PropTypes.func,
    navigation: PropTypes.instanceOf(Object)
};

const mapStateToProps = (state) => {
    const {error, loading, payload} = state.main;
    const {notification} = state.notification;
    const {user} = state.auth;

    return {
        error,
        loading,
        payload,
        nav: state.nav,
        notification,
        user
    };
};

const SplashScreenRedux = connect(mapStateToProps, {checkServer})(SplashScreen);

export {SplashScreenRedux as SplashScreen};
