import React from "react";
import {connect} from "react-redux";
import config from "../../../config/config";
import {translateText} from "../../translation/translate";
import {updateUser} from "../../actions/Profile";


class UpdateProfileScreen extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText("UpdateProfile")
                .toUpperCase(),
            headerRight: null
        };
    };

    render() {
        const {Component} = config().UpdateProfileScreen;

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({auth, main}) => {
    return {
        mall: main.payload.mall,
        user: auth.user
    };
};

const UpdateProfileRedux = connect(mapStateToProps, {
    updateUser,
    withRef: true
})(UpdateProfileScreen);

export {UpdateProfileRedux as UpdateProfileScreen};
