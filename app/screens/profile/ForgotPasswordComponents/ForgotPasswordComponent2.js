import React from "react";
import {Platform, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import PropTypes from "prop-types";
import {CachedImage} from "react-native-img-cache";
import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {Input} from "../../../components/common";

class ForgotPasswordComponent2 extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText("ForgotPassword")
                .toUpperCase(),
            headerRight: null
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            email_error: ""
        };
    }

    styles = {
        loginButton: {
            padding: 8,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: config().BASE_COLOR,
            borderRadius: 20,
        },
        buttonText: {
            color: "#fff",
            fontSize: 15,
            alignSelf: "center"
        },
        container: {
            flex: 1,
            justifyContent: "center",
            flexDirection: "row",
            backgroundColor: "#fff"
        },
        defaultInputStyle: {
            backgroundColor: "#fff",
            borderColor: "#555",
            width: "100%",
            height: 40,
            borderWidth: 1,
            justifyContent: "center",
        },
        defaultInputTextStyle: {
            fontSize: 13
        },
        cardContainer: {
            margin: 8
        }
    };

    render() {
        const {PlaceHolderImage} = config();

        return (
            <View style={this.styles.container}>
                {Platform.OS === "ios" && (
                    <CachedImage
                        style={{
                            ...StyleSheet.absoluteFillObject,
                            width: "100%",
                            height: "100%",
                            opacity: 0.05
                        }}
                        source={require("../../../../assets/images/pattern.png")}
                        resizeMode="repeat"
                    />
                )}
                <View style={{flex: 0.7}}>
                    <CachedImage
                        source={PlaceHolderImage}
                        resizeMode="contain"
                        style={{
                            width: "70%",
                            height: 100,
                            alignSelf: "center"
                        }}
                    />
                    <View style={this.styles.cardContainer}>
                        <Input
                            placeholder={translateText("E-Mail")}
                            value={this.state.email}
                            style={this.styles.defaultInputStyle}
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            keyboardType="email-address"
                            error={this.state.email_error}
                            onChangeText={(email) => {
                                return this.setState({email});
                            }}
                        />
                    </View>
                    <View style={this.styles.cardContainer}>
                        <TouchableOpacity
                            style={this.styles.loginButton}
                            onPress={() => {
                                this.props.forgetPassword(this.state.email);
                            }}
                        >
                            <Text style={this.styles.buttonText}>{translateText("SEND")}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

ForgotPasswordComponent2.propTypes = {
    forgetPassword: PropTypes.func
};

ForgotPasswordComponent2.defaultProps = {
    forgetPassword: () => {
    }
};

export default ForgotPasswordComponent2;
