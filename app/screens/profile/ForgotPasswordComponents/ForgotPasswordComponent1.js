import React from "react";
import {StyleSheet, Platform, Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import {CachedImage} from "react-native-img-cache";
import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {Icon, Input} from "../../../components/common";

class ForgotPasswordComponent1 extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText("ForgotPassword")
                .toUpperCase(),
            headerRight: null
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            email_error: ""
        };
    }

    styles = {
        linearGradient: {
            padding: 10,
            borderRadius: 30,
            flexDirection: "row",
            alignSelf: "center",
            justifyContent: "space-between",
            flex: 0.6
        },
        loginButton: {
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
        },
        buttonText: {
            color: "#fff",
            fontSize: 15,
            alignSelf: "center"
        },
        container: {
            flex: 1,
            justifyContent: "center",
            flexDirection: "row",
            backgroundColor: "#fff"
        },
        defaultInputStyle: {
            borderColor: "#555",
            width: "100%",
            height: 40,
            borderBottomWidth: StyleSheet.hairlineWidth,
            padding: 8,
            justifyContent: "center",
        },
        defaultInputTextStyle: {
            fontSize: 13
        },
        cardContainer: {
            margin: 8
        },
        defaultLogoImage: {
            width: 100,
            height: 100,
            marginTop: 20,
            alignSelf: "center"
        }
    };

    render() {
        const {PlaceHolderImage} = config();

        return (
            <View style={this.styles.container}>
                {Platform.OS === "ios" && <CachedImage
                    style={{
                        ...StyleSheet.absoluteFillObject,
                        width: "100%",
                        height: "100%",
                        opacity: 0.05
                    }}
                    source={require("../../../../assets/images/pattern.png")}
                    resizeMode="repeat"
                />}
                <View style={{flex: 0.7}}>
                    <CachedImage
                        source={PlaceHolderImage}
                        style={this.styles.defaultLogoImage}
                    />
                    <View style={this.styles.cardContainer}>
                        <Input
                            placeholder={translateText("E-Mail")}
                            value={this.state.email}
                            style={this.styles.defaultInputStyle}
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            keyboardType="email-address"
                            error={this.state.email_error}
                            onChangeText={(email) => {
                                return this.setState({email});
                            }}
                        />
                    </View>
                    <View style={this.styles.cardContainer}>
                        <TouchableOpacity
                            style={this.styles.loginButton}
                            onPress={() => {
                                this.props.forgetPassword(this.state.email);
                            }}
                        >
                            <LinearGradient
                                colors={[config().BASE_FOUR_COLOR, config().BASE_THIRD_COLOR]}
                                style={this.styles.linearGradient}
                            >
                                <Text style={this.styles.buttonText}>{translateText("SEND")}</Text>
                                <Icon name="left-arrow" size={18} color="#fff"/>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

ForgotPasswordComponent1.propTypes = {
    forgetPassword: PropTypes.func
};

ForgotPasswordComponent1.defaultProps = {
    forgetPassword: () => {
    }
};

export default ForgotPasswordComponent1;
