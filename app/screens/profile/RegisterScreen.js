import React from "react";
import {connect} from "react-redux";
import config from "../../../config/config";
import {translateText} from "../../translation/translate";
import RegisterComponent1 from "./RegisterComponents/RegisterComponent1";
import {registerUser} from "../../actions/Profile";

class RegisterScreen extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText("REGISTER")
                .toUpperCase(),
            headerRight: null
        };
    };

    render() {
        const {Enabled, Component} = config().RegisterScreen;

        if (!Enabled) {
            return (
                <RegisterComponent1 {...this.props}/>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({auth, main}) => {
    const {error, loading} = auth;
    const {payload} = main;
    return {
        error,
        loading,
        mall: payload.mall
    };
};

const RegisterRedux = connect(mapStateToProps, {registerUser})(RegisterScreen);

export {RegisterRedux as RegisterScreen};
