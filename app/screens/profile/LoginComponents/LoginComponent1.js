import React, {Component} from "react";
import {StyleSheet, Text, TouchableOpacity,Platform, View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {Button, Icon, Input} from "../../../components/common";
import {DefaultButton} from "../../../components/buttons";

class LoginComponent1 extends Component {
    state = {
        email: "",
        password: "",
        email_error: [],
        password_error: []
    };

    styles = {
        defaultInputStyle: {
            backgroundColor: "#fff",
            borderColor: "#555",
            width: "100%",
            height: 40,
            borderWidth: 1,
            padding: 8,
            justifyContent: "center",
        },
        defaultButtonStyle: {
            width: "100%",
            height: 40,
            backgroundColor: config().BASE_SECOND_COLOR,
        },
        borderButtonStyle: {
            backgroundColor: "transparent",
            borderColor: config().BASE_SECOND_COLOR,
            borderWidth: StyleSheet.hairlineWidth
        },
        backgroundImage: {
            backgroundColor: "#ccc",
            resizeMode: "cover",
            position: "absolute",
            width: "100%",
            height: "100%",
            justifyContent: "center",
        },
        facebookButton: {
            padding: 8,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#003471",
        },
        socialContainer: {
            flexDirection: "row",
            margin: 8,
        },
        loginButton: {
            padding: 8,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#222"
        },
        buttonText: {
            color: "#fff",
            fontSize: 15,
            alignSelf: "center"
        },
        container: {
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
        },
        cardContainer: {
            marginBottom: 12
        },
        linkText: {
            fontSize: 13,
            margin: 15,
            color: config().BASE_COLOR,
            backgroundColor: "transparent"
        },
        facebookIcon: {
            height: 20,
            resizeMode: "contain"
        },
        borderlessButton: {
            fontSize: 13
        }
    };

    render() {
        const {LoginWithFacebook, LoginWithGoogle} = config();

        return (
            <View style={this.styles.container}>
                {Platform.OS === "ios" && <CachedImage
                    style={{
                        ...StyleSheet.absoluteFillObject,
                        width: "100%",
                        height: "100%",
                        opacity: 0.05
                    }}
                    source={require("../../../../assets/images/pattern.png")}
                    resizeMode="repeat"
                />}
                <View style={this.styles.socialContainer}>
                    {LoginWithFacebook && <View style={{margin: 5}}>
                        <Icon.Button
                            name="facebook"
                            backgroundColor="#3b5998"
                            onPress={this.props.facebookLogin}
                        >
                            {translateText("login_facebook")}
                        </Icon.Button>
                    </View>}
                    {LoginWithGoogle && <View style={{margin: 5}}>
                        <Icon.Button
                            name="google"
                            backgroundColor="#D62D20"
                            onPress={this.props.googleLogin}
                        >
                            {translateText("login_google")}
                        </Icon.Button>
                    </View>}
                </View>

                <View style={{
                    flex: 1,
                    flexDirection: "row"
                }}>

                    <View style={{flex: 0.7}}>
                        <View style={this.styles.cardContainer}>
                            <Text style={{alignSelf: "center"}}>{translateText("OR")}</Text>
                        </View>
                        <View style={this.styles.cardContainer}>
                            <Input
                                placeholder={translateText("E-Mail")}
                                value={this.state.email}
                                style={this.styles.defaultInputStyle}
                                keyboardType="email-address"
                                onChangeText={(email) => {
                                    return this.setState({email});
                                }}
                            />
                        </View>
                        <View style={this.styles.cardContainer}>
                            <Input
                                secureTextEntry
                                placeholder={translateText("Password")}
                                value={this.state.password}
                                style={this.styles.defaultInputStyle}
                                onChangeText={(password) => {
                                    return this.setState({password});
                                }}
                            />
                        </View>
                        <View style={this.styles.cardContainer}>
                            <Button
                                style={this.styles.loginButton}
                                onPress={() => {
                                    this.props.loginUser({
                                        email: this.state.email,
                                        password: this.state.password
                                    });
                                }}
                            >
                                <Text style={this.styles.buttonText}>{translateText("Login")}</Text>
                            </Button>
                        </View>
                        <View style={{
                            alignItems: "center",
                            justifyContent: "space-around",
                            height: 200
                        }}
                        >
                            <Text
                                style={this.styles.linkText}
                                onPress={() => {
                                    this.props.navigation.navigate("ForgetPassword");
                                }}
                            >
                                {translateText("ForgotPassword")}
                            </Text>
                            <View style={{width: "100%"}}>
                                <TouchableOpacity
                                    style={[this.styles.defaultButtonStyle, {
                                        alignItems: "center",
                                        justifyContent: "center",
                                        backgroundColor: config().BASE_COLOR
                                    }]}
                                    onPress={() => {
                                        this.props.navigation.navigate("Register");
                                    }}
                                >
                                    <Text>
                                        {translateText("Register")}
                                    </Text>

                                </TouchableOpacity>
                                <DefaultButton
                                    title={translateText("Privacy")}
                                    titleStyle={this.styles.borderlessButton}
                                    style={{backgroundColor: "transparent"}}
                                    onPress={() => {
                                        this.props.navigation.navigate("TermAndConditions");
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

LoginComponent1.propTypes = {};

export default LoginComponent1;
