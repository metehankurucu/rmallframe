import React, {Component} from "react";
import {Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import PropTypes from "prop-types";
import {CachedImage} from "react-native-img-cache";
import LinearGradient from "react-native-linear-gradient";
import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {Icon, Input} from "../../../components/common";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

class LoginComponent2 extends Component {
    state = {
        email: "",
        password: ""
    };

    styles = {
        linearGradient: {
            padding: 10,
            borderRadius: 30,
            flexDirection: "row",
            alignSelf: "center",
            justifyContent: "space-between",
            flex: 0.6
        },
        defaultInputStyle: {
            backgroundColor: "#fff",
            borderColor: "#555",
            width: "100%",
            height: 36,
            borderBottomWidth: StyleSheet.hairlineWidth,
            padding: 0,
            justifyContent: "center"
        },
        defaultInputTextStyle: {
            fontSize: 13
        },
        defaultButtonStyle: {
            width: "100%",
            height: 40,
            backgroundColor: config().BASE_SECOND_COLOR,
        },
        socialContainer: {
            margin: 8
        },
        loginButton: {
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
        },
        buttonText: {
            color: "#fff",
            fontSize: 13,
            alignSelf: "center"
        },
        container: {
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#fff"
        },
        cardContainer: {
            marginBottom: 12
        },
        linkText: {
            fontSize: 12,
            margin: 12,
            color: config().BASE_SECOND_COLOR,
        },
        socialLoginButton: {
            backgroundColor: "#D62D20",
            borderRadius: 25,
            width: "100%",
            alignItems: "center",
            flexDirection: "row",
            padding: 10
        },
        socialButtonContainer: {
            margin: 5,
            flexDirection: "row",
            width: "70%"
        },
        socialLoginButtonText: {
            color: "#fff",
            fontSize: 14,
            fontWeight: "100",
            marginLeft: 5
        },
        welcomeText: {
            fontSize: 25,
            fontWeight: "200",
            margin: 5
        }
    };

    render() {
        const {LoginWithFacebook, LoginWithGoogle, PlaceHolderImage} = config();

        return (
            <KeyboardAwareScrollView>
                <View style={this.styles.container}>
                    {Platform.OS === "ios" && <CachedImage
                        style={{
                            ...StyleSheet.absoluteFillObject,
                            width: "100%",
                            height: "100%",
                            opacity: 0.05
                        }}
                        source={require("../../../../assets/images/pattern.png")}
                        resizeMode="repeat"
                    />}
                    <CachedImage
                        source={PlaceHolderImage}
                        resizeMode="contain"
                        style={{
                            width: "50%",
                            height: 100
                        }}
                    />
                    <Text style={this.styles.welcomeText}>{translateText("Welcome")}</Text>
                    <View style={this.styles.socialContainer}>
                        {LoginWithFacebook && (
                            <View style={this.styles.socialButtonContainer}>
                                <TouchableOpacity
                                    style={[this.styles.socialLoginButton, {backgroundColor: "#3b5998"}]}
                                    onPress={this.props.facebookLogin}
                                >
                                    <Icon
                                        name="facebook"
                                        size={18}
                                        color="#fff"
                                        style={{
                                            marginLeft: 8,
                                            marginRight: 8
                                        }}
                                    />
                                    <Text style={this.styles.socialLoginButtonText}>
                                        {translateText("login_facebook")}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        )}
                        {
                            LoginWithGoogle && (
                                <View style={this.styles.socialButtonContainer}>
                                    <TouchableOpacity
                                        style={this.styles.socialLoginButton}
                                        onPress={this.props.googleLogin}
                                    >
                                        <Icon
                                            name="google"
                                            size={18}
                                            color="#fff"
                                            style={{
                                                marginLeft: 8,
                                                marginRight: 8
                                            }}
                                        />
                                        <Text style={this.styles.socialLoginButtonText}>
                                            {translateText("login_google")}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }
                    </View>

                    <View style={{
                        flex: 1,
                        flexDirection: "row"
                    }}
                    >
                        <View style={{flex: 0.7}}>
                            <View style={this.styles.cardContainer}>
                                <Text style={{
                                    alignSelf: "center",
                                    fontSize: 11,
                                    fontWeight: "700"
                                }}
                                >
                                    {translateText("OR")}
                                </Text>
                            </View>
                            <View style={this.styles.cardContainer}>
                                <Input
                                    placeholder={translateText("E-Mail")}
                                    value={this.state.email}
                                    style={this.styles.defaultInputStyle}
                                    inputTextStyle={this.styles.defaultInputTextStyle}
                                    keyboardType="email-address"
                                    onChangeText={(email) => {
                                        return this.setState({email});
                                    }}
                                />
                            </View>
                            <View style={this.styles.cardContainer}>
                                <Input
                                    secureTextEntry
                                    placeholder={translateText("Password")}
                                    value={this.state.password}
                                    style={this.styles.defaultInputStyle}
                                    inputTextStyle={this.styles.defaultInputTextStyle}
                                    onChangeText={(password) => {
                                        return this.setState({password});
                                    }}
                                />
                            </View>
                            <View style={this.styles.cardContainer}>
                                <TouchableOpacity
                                    style={this.styles.loginButton}
                                    onPress={() => {
                                        this.props.loginUser({
                                            email: this.state.email,
                                            password: this.state.password
                                        });
                                    }}
                                >
                                    <LinearGradient
                                        colors={[config().BASE_FOUR_COLOR, config().BASE_THIRD_COLOR]}
                                        style={this.styles.linearGradient}
                                    >
                                        <Text style={[this.styles.buttonText, {width:"100%", textAlign: "center"}]}>{translateText("Login")}</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View>
                            <View style={this.styles.cardContainer}>
                                <TouchableOpacity
                                    style={this.styles.loginButton}
                                    onPress={() => {
                                        this.props.navigation.navigate("Register");
                                    }}
                                >
                                    <LinearGradient
                                        colors={[config().BASE_FOUR_COLOR, config().BASE_THIRD_COLOR]}
                                        style={this.styles.linearGradient}
                                    >
                                        <Text
                                            style={[this.styles.buttonText, {width:"100%", textAlign: "center"}]}>
                                            {translateText("Register")}
                                        </Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                alignItems: "center",
                                flex: 1,
                                justifyContent: "space-around"
                            }}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("ForgetPassword");
                                    }}
                                >
                                    <Text style={this.styles.linkText}>
                                        {translateText("ForgotPassword")}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

LoginComponent2.propTypes = {
    facebookLogin: PropTypes.func,
    loginUser: PropTypes.func,
    googleLogin: PropTypes.func,
    navigation: PropTypes.instanceOf(Object)
};

LoginComponent2.defaultProps = {
    facebookLogin: () => {
    },
    loginUser: () => {
    },
    googleLogin: () => {
    },
    navigation: {}
};

export default LoginComponent2;
