import React from "react";
import {connect} from "react-redux";
import {CachedImage} from "react-native-img-cache";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import ImagePicker from "react-native-image-picker";
import ScrollableTabView from "react-native-scrollable-tab-view";
import PropTypes from "prop-types";
import {DrawerActions} from "react-navigation";
import {logoutUser, saveProfilePhoto} from "../../actions/Profile";
import ListItem from "../../components/stores/ListItem";
import {favoriteAction} from "../../actions/Favorite";
import {Button, GridView, Icon} from "../../components/common";
import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import OptimizedFlatList from "../../components/common/OptimizedFlatList";
import {DefaultTabBar} from "../../components/common/tabbar/DefaultTabBar";
import {OfferListItem} from "../../components/offers/OfferListItem";
import {EventListItem} from "../../components/events";

const {
    storesKeymap, promotionsKeymap, eventsKeymap, offersKeymap, newsKeymap
} = config();

class ProfileScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("PROFILE")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;
                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("PROFILE")
                .toUpperCase(),
        };
    };

    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this);
    }

    UNSAFE_componentWillMount() {
        this.props.navigation.setParams({
            title: translateText("Profile")
        });
    }

    styles = {
        headerContainer: {
            marginTop: 15,
            marginBottom: 15
        },
        defaultText: {
            fontSize: 13,
            color: "#555",
            alignSelf: "center"
        },
        defaultButton: {
            borderColor: "#555",
            borderWidth: StyleSheet.hairlineWidth,
            borderRadius: 15,
            padding: 8,
            marginRight: 8,
            height: 32,
        },
        textStyle: {
            color: config().BASE_COLOR,
            fontSize: 12
        },
        tabBar: {
            padding: 15
        },
        defaultUserImage: {
            resizeMode: "cover",
            width: 96,
            height: 96,
            borderRadius: 48
        }
    };

    _renderItem = (item, key) => {
        if (key === "stores") {
            return (
                <ListItem
                    item={item}
                    key={item["_id"]}
                    onPress={() => {
                        this.props.navigation.navigate("StoreDetail", {
                            data: item,
                            title: item.name
                        });
                    }}
                />
            );
        }
        else if (key === "events") {
            return (
                <EventListItem
                    data={item}
                    onPress={() => {
                        this.props.navigation.navigate("EventDetail", {
                            data: item,
                            title: item.title
                        });
                    }}
                />
            );
        }
        else if (key === "offers") {
            return (
                <OfferListItem
                    offer={item}
                    onPress={() => {
                        this.props.navigation.navigate("OfferDetail", {
                            data: item,
                            title: item.title
                        });
                    }}
                />
            );
        }
        else if (key === "whatsnew") {
            return (
                <OfferListItem
                    offer={item}
                    onPress={() => {
                        this.props.navigation.navigate("WhatsNewDetail", {
                            whatsNew: item,
                            title: item.title
                        });
                    }}
                />
            );
        }

        return null;
    };

    render() {
        const {ProfileScreen, AnonymousUserImage} = config();
        const {Enabled, Component} = ProfileScreen;

        if (!Enabled) {
            return (
                <View style={{height: "100%"}}>
                    <View style={{
                        padding: 15,
                        paddingBottom: 0
                    }}
                    >
                        <View style={{
                            flexDirection: "row",
                            height: 100
                        }}
                        >
                            <Button
                                onPress={() => {
                                    const options = {
                                        quality: 1.0,
                                        maxWidth: 500,
                                        maxHeight: 500,
                                        storageOptions: {
                                            skipBackup: true
                                        },
                                    };

                                    ImagePicker.showImagePicker(options, (response) => {
                                        if (!response.didCancel && !response.error && !response.customButton) {
                                            this.props.saveProfilePhoto(this.props.user._id, response);
                                        }
                                    });
                                }}
                                activeOpacity={0.8}
                                style={{width: 100,}}
                            >
                                {this.props.user && this.props.user.profile_picture !== "" ? (
                                    <CachedImage
                                        source={{uri: this.props.user.profile_picture}}
                                        defaultSource={AnonymousUserImage}
                                        style={this.styles.defaultUserImage}
                                    />
                                ) : (
                                    <CachedImage
                                        source={AnonymousUserImage}
                                        style={this.styles.defaultUserImage}
                                    />
                                )
                                }

                            </Button>
                            <View style={{
                                marginLeft: 15,
                                justifyContent: "center",
                                flex: 1,
                            }}
                            >
                                {
                                    this.props.user && (
                                        <Text style={{
                                            fontSize: 18,
                                        }}
                                        >
                                            {`${this.props.user.name} ${this.props.user.surname}`}
                                        </Text>
                                    )
                                }

                                <View style={{
                                    flexDirection: "row",
                                    marginTop: 10,
                                    alignItems: "center",
                                    justifyContent: "space-between"
                                }}
                                >
                                    <Button
                                        onPress={() => {
                                            this.props.navigation.navigate("UpdateProfile");
                                        }}
                                        activeOpacity={0.8}
                                        style={[this.styles.defaultButton, {flex: 1}]}
                                    >
                                        <Text style={this.styles.defaultText}>{translateText("Edit")}</Text>
                                    </Button>
                                    <Button
                                        onPress={() => {
                                            this.props.navigation.navigate("Login", {
                                                type: "REPLACE"
                                            });

                                            this.props.logoutUser();
                                        }}
                                        style={[this.styles.defaultButton, {
                                            flex: 1,
                                            borderColor: config().BASE_THIRD_COLOR
                                        }]}
                                    >
                                        <Text
                                            style={[this.styles.defaultText, {color: config().BASE_THIRD_COLOR}]}>{translateText("Logout")}</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </View>

                    <ScrollableTabView
                        style={{height: "100%"}}
                        renderTabBar={() => {
                            return (<DefaultTabBar
                                textStyle={this.styles.textStyle}
                                style={this.styles.tabBar}
                                underlineStyle={{backgroundColor: config().BASE_COLOR}}
                            />);
                        }
                        }
                    >

                        {storesKeymap.enabled && <OptimizedFlatList
                            key="stores"
                            tabLabel={translateText("Brands")
                                .toUpperCase()}
                            keyExtractor={(item, key) => {
                                return key.toString();
                            }}
                            data={this.props.stores}
                            style={{height: "100%"}}
                            renderItem={(data) => {
                                return this._renderItem(data.item, "stores");
                            }}
                        />}

                        {eventsKeymap.enabled && <GridView
                            itemsPerRow={2}
                            key="events"
                            tabLabel={translateText("Events")
                                .toUpperCase()}
                            items={this.props.events}
                            renderItem={(item) => {
                                return this._renderItem(item, "events");
                            }}
                        />}

                        {offersKeymap.enabled && <GridView
                            key="offers"
                            itemsPerRow={2}
                            tabLabel={translateText("Offers")
                                .toUpperCase()}
                            items={this.props.offers}
                            renderItem={(item) => {
                                return this._renderItem(item, "offers");
                            }}
                        />}

                        {newsKeymap.enabled && <GridView
                            key="whatsnew"
                            itemsPerRow={2}
                            tabLabel={translateText("WhatsNews")
                                .toUpperCase()}
                            items={this.props.whatsNews}
                            renderItem={(item) => {
                                return this._renderItem(item, "whatsnew");
                            }}
                        />}

                    </ScrollableTabView>
                </View>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({auth, main}) => {
    const {user} = auth;
    const {payload} = main;

    if (user && user.isLoggedIn) {
        const state = {};

        if (storesKeymap.enabled) {
            const stores = [];

            payload.stores.forEach((data) => {
                const store = data;
                const match = user["favorite_stores"].find((obj) => {
                    return obj.s === store._id;
                });

                store["fav"] = !!match;
                store["userid"] = user["_id"];

                if (match) {
                    stores.push(store);
                }
            });

            state["stores"] = stores;
        }

        if (eventsKeymap.enabled) {
            const events = [];

            payload.events.forEach((data) => {
                const event = data;
                const match = user["favorite_events"].find((obj) => {
                    return obj.s === event._id;
                });

                event["fav"] = !!match;
                event["userid"] = user["_id"];

                if (match) {
                    events.push(event);
                }
            });

            state["events"] = events;
        }

        if (offersKeymap.enabled) {
            const offers = [];

            payload.offers.forEach((data) => {
                const promotion = data;
                const match = user["favorite_offers"].find((obj) => {
                    return obj["s"] === promotion["_id"];
                });

                promotion["fav"] = !!match;
                promotion["userid"] = user["_id"];

                if (match) {
                    offers.push(promotion);
                }

                return false;
            });

            state["offers"] = offers;
        }

        if (newsKeymap.enabled) {
            const whatsNews = [];

            payload.news.forEach((data) => {
                const whatsNew = data;

                const match = user["favorite_news"].find((obj) => {
                    return obj["s"] === whatsNew["_id"];
                });

                whatsNew["fav"] = !!match;
                whatsNew["userid"] = user["_id"];

                if (match) {
                    whatsNews.push(whatsNew);
                }

            });

            state["whatsNews"] = whatsNews;

        }

        state["user"] = user;

        return state;
    }

    return {user: auth.user};
};

const ProfileRedux = connect(mapStateToProps, {
    logoutUser,
    favoriteAction,
    saveProfilePhoto
})(ProfileScreen);

ProfileScreen.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    offers: PropTypes.instanceOf(Array),
    whatsNews: PropTypes.instanceOf(Array),
    events: PropTypes.instanceOf(Array),
    stores: PropTypes.instanceOf(Array),
    logoutUser: PropTypes.func,
    saveProfilePhoto: PropTypes.func,
    favoriteAction: PropTypes.func,
    user: PropTypes.instanceOf(Object),
};

ProfileScreen.defaultProps = {
    navigation: {},
    offers: [],
    whatsNews: [],
    events: [],
    stores: [],
    logoutUser: () => {
    },
    saveProfilePhoto: () => {
    },
    favoriteAction: () => {
    },
    user: {},
};

export {ProfileRedux as ProfileScreen};
