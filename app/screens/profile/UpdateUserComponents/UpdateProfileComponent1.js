import React, { Component } from "react";
import { Alert, Platform, ScrollView, StyleSheet, View } from "react-native";
import SelectInput from "react-native-select-input-ios";
import DatePicker from "react-native-datepicker";
import { TextInputMask } from "react-native-masked-text";

import config from "../../../../config/config";
import { translateText } from "../../../translation/translate";
import { Input } from "../../../components/common";
import { BorderlessButton, DefaultButton } from "../../../components/buttons";

const email_filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;

class UpdateProfileComponent1 extends Component {
    state = {
        name: this.props.user.name,
        surname: this.props.user.surname,
        email: this.props.user.email,
        gender: this.props.user.gender,
        phone: this.props.user.phone,
        birthdate: this.props.user.birthdate,
    };

    styles = {
        defaultInputStyle: {
            backgroundColor: "#fff",
            borderColor: "#555",
            width: "100%",
            height: 40,
            fontSize: 14,
            borderWidth: 1,
            color: "#000",
            justifyContent: "center",
        },
        buttonContainer: {
            marginTop: 15,
            width: "70%"
        },
        cardContainer: {
            marginBottom: 30,
            alignItems: "center",
            justifyContent: "center",
            width: "100%"
        },
        defaultButtonStyle: {
            width: "100%",
            height: 40,
            backgroundColor: config().BASE_SECOND_COLOR
        },
        borderlessButton: {
            color: config().BASE_SECOND_COLOR
        },
        scrollView: {
            padding: 15
        },
        backgroundImage: {
            backgroundColor: "#ccc",
            resizeMode: "cover",
            position: "absolute",
            width: "100%",
            height: "100%",
            justifyContent: "center",
        },
        SectionStyle: {
            marginTop: 10
        },
        container: {
            flex: 1
        },
        helpText: {
            fontSize: 12,
            color: config().BASE_SECOND_COLOR,
            fontStyle: "italic",
            fontWeight: "100"
        }
    };

    validateData = (data) => {
        let name_error = false,
            surname_error = false,
            email_error = false,
            birthday_error = false,
            phone_error = false,
            password_error = false,
            password_again_error = false,
            errorText = "";

        if (data.name === "") {
            name_error = true;
            errorText = translateText("empty_value");
        }
        if (data.surname === "") {
            surname_error = true;
            errorText = translateText("empty_value");
        }
        if (!email_filter.test(data.email)) {
            email_error = true;
            if (errorText === "") {
                errorText = translateText("email_not_valid");
            }
        }
        if (data.birthdate === "") {
            birthday_error = true;
            errorText = translateText("empty_value");
        }
        if (data.phone.length !== 13) {
            phone_error = true;
            errorText = translateText("empty_value");
        }
        if (data.password === "") {
            password_error = true;
            errorText = translateText("empty_value");
        }
        if (data.password_again === "" || data.password !== data.password_again) {
            password_again_error = true;
            if (errorText === "") {
                errorText = translateText("passwords_not_match");
            }
        }

        if (errorText !== "") {
            Alert.alert(translateText("error"), errorText);
        }

        this.setState({
            name_error,
            surname_error,
            email_error,
            birthday_error,
            phone_error,
            password_error,
            password_again_error
        });

        this.scroll.scrollTo({
            x: 0,
            y: 0,
            animated: true
        });

        return !(name_error || surname_error || email_error || birthday_error || phone_error || password_again_error || password_error);
    };

    render() {
        const {
            name, name_error, surname, surname_error, phone, phone_error, birthdate, birthdate_error, email, email_error, gender
        } = this.state;

        return (
            <View style={this.styles.container}>
                <ScrollView
                    style={this.styles.scrollView}
                    ref={(ref) => {
                        this.scroll = ref;
                    }}
                >
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("Name")} *`}
                            value={name}
                            error={name_error}
                            style={this.styles.defaultInputStyle}
                            onChangeText={(name) => {
                                return this.setState({ name });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("Surname")} *`}
                            value={surname}
                            error={surname_error}
                            style={this.styles.defaultInputStyle}
                            onChangeText={(surname) => {
                                this.setState({ surname });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("E-Mail")} *`}
                            value={email}
                            error={email_error}
                            keyboardType="email-address"
                            style={this.styles.defaultInputStyle}
                            onChangeText={(email) => {
                                this.setState({ email });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <SelectInput
                            value={gender}
                            keyboardBackgroundColor="#fff"
                            buttonsBackgroundColor="#fff"
                            buttonsTextColor="#46cf98"
                            buttonsTextSize={16}
                            style={this.styles.defaultInputStyle}
                            labelStyle={[this.styles.defaultInputTextStyle, {
                                padding: 10
                            }]}
                            options={[
                                { value: "male", label: translateText("Male") },
                                { value: "female", label: translateText("Female") },
                            ]}
                            onSubmitEditing={(itemValue) => {
                                return this.setState({ gender: itemValue });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <DatePicker
                            style={{ width: "100%", }}
                            date={birthdate}
                            mode="date"
                            error={birthdate_error}
                            format="DD/MM/YYYY"
                            minDate="01/01/1920"
                            maxDate={new Date()}
                            showIcon={false}
                            placeholder={`${translateText("Birthdate")} *`}
                            confirmBtnText={translateText("Confirm")}
                            cancelBtnText={translateText("Cancel")}
                            customStyles={{
                                dateInput: [this.styles.defaultInputStyle, {
                                    alignItems: "flex-start"
                                }],
                                placeholderText: {
                                    paddingLeft: 10,
                                    color: "#6b6b6b",
                                    fontSize: 14,
                                },
                                dateText: {
                                    paddingLeft: 10,
                                    color: "#000",
                                    fontSize: 14,
                                },
                                btnTextConfirm: {
                                    color: "#46cf98"
                                },
                                btnTextCancel: {
                                    color: "#46cf98"
                                },
                            }}
                            onDateChange={(birthdate) => {
                                this.setState({ birthdate });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <TextInputMask
                            type="custom"
                            placeholderTextColor="#6b6b6b"
                            value={this.state.phone}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            onChangeText={(phone, extracted) => {
                                if (phone.length < 15) {
                                    this.setState({ phone });
                                }
                            }}
                            style={[this.styles.defaultInputStyle, { paddingLeft: 10, }]}
                            options={{
                                /**
                                 * mask: (String | required | default '')
                                 * the mask pattern
                                 * 9 - accept digit.
                                 * A - accept alpha.
                                 * S - accept alphanumeric.
                                 * * - accept all, EXCEPT white space.
                                 */
                                mask: "(999) 999-9999"
                            }}
                            placeholder="Ex 512 345 6789"
                        />
                    </View>

                    <View style={this.styles.cardContainer}>
                        <View style={this.styles.buttonContainer}>
                            <BorderlessButton
                                title={translateText("Save")}
                                style={this.styles.defaultButtonStyle}
                                onPress={() => {

                                    const registerData = {
                                        name,
                                        surname,
                                        birthdate,
                                        gender,
                                        email,
                                        phone: `+90${phone.match(/\d+/g).join("")}`
                                    };
                                    if (this.validateData(registerData)) {
                                        this.props.updateUser(registerData, this.props.user);
                                    }
                                }}
                            />
                        </View>

                        <DefaultButton
                            title={translateText("Privacy")}
                            titleStyle={this.styles.borderlessButton}
                            style={{ backgroundColor: "transparent" }}
                            onPress={() => {
                                this.props.navigation.navigate("TermAndConditions", {
                                    "mall": this.props.mall
                                });
                            }}
                        />

                    </View>

                </ScrollView>
            </View>

        );
    }
}

UpdateProfileComponent1.propTypes = {};

export default UpdateProfileComponent1;
