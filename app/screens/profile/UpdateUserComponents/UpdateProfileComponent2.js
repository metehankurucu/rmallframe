import React, {Component} from "react";
import {Alert, Platform, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import SelectInput from "react-native-select-input-ios";
import DatePicker from "react-native-datepicker";
import PropTypes from "prop-types";
import {TextInputMask} from "react-native-masked-text";
import {CachedImage} from "react-native-img-cache";
import LinearGradient from "react-native-linear-gradient";

import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {Icon, Input} from "../../../components/common";

const email_filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;

class UpdateProfileComponent2 extends Component {
    state = {
        name: this.props.user.name,
        surname: this.props.user.surname,
        email: this.props.user.email,
        gender: this.props.user.gender,
        phone: this.props.user.phone && `(${this.props.user.phone.substring(3, 6)})${this.props.user.phone.substring(6, 13)}`,
        birthdate: this.props.user.birthdate,
    };

    styles = {
        welcomeText: {
            fontSize: 16,
            fontWeight: "800",
            color: config().BASE_FOUR_COLOR,
            margin: 5
        },
        linkText: {
            fontSize: 13,
            color: "#fff"
        },
        defaultInputStyle: {
            borderColor: "#555",
            width: "100%",
            height: 32,
            borderBottomWidth: StyleSheet.hairlineWidth,
            justifyContent: "center",
        },
        defaultInputTextStyle: {
            fontSize: 12,
            color: "#000",
        },
        cardContainer: {
            alignItems: "center",
            justifyContent: "center",
            alignSelf: "center"
        },
        defaultButtonStyle: {
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginTop: 15
        },
        linearGradient: {
            padding: 10,
            borderRadius: 30,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            flex: 0.35
        },
        scrollView: {
            width: "60%"
        },
        SectionStyle: {
            width: "100%",
            alignSelf: "center"
        },
        container: {
            flex: 1,
            alignItems: "center",
            paddingTop: "30%",
            backgroundColor: "#fff"
        },
        helpText: {
            fontSize: 12,
            color: config().BASE_SECOND_COLOR,
            fontStyle: "italic",
            fontWeight: "100"
        }
    };

    validateData = (data) => {
        let name_error = false,
            surname_error = false,
            email_error = false,
            errorText = "";

        if (data.name === "") {
            name_error = true;
            errorText = translateText("empty_value");
        }
        if (data.surname === "") {
            surname_error = true;
            errorText = translateText("empty_value");
        }
        if (!email_filter.test(data.email)) {
            email_error = true;
            if (errorText === "") {
                errorText = translateText("email_not_valid");
            }
        }

        if (errorText !== "") {
            Alert.alert(translateText("error"), errorText);
        }

        this.setState({
            name_error,
            surname_error,
            email_error,
        });

        return !(name_error || surname_error || email_error);
    };

    render() {
        const {
            name,
            name_error,
            surname,
            surname_error,
            phone,
            birthdate,
            email,
            email_error,
            gender,
        } = this.state;

        const {PlaceHolderImage, UpdateProfileScreen} = config();
        const {PhoneVerify} = UpdateProfileScreen;

        return (
            <View style={this.styles.container}>
                {Platform.OS === "ios" && <CachedImage
                    style={{
                        ...StyleSheet.absoluteFillObject,
                        width: "100%",
                        height: "100%",
                        opacity: 0.05
                    }}
                    source={require("../../../../assets/images/pattern.png")}
                    resizeMode="repeat"
                />}

                {PhoneVerify.Enabled && <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() => {
                        this.props.navigation.navigate("Verified", {
                            PhoneSection: true
                        });
                    }}
                    style={{
                        position: "absolute",
                        top: 0,
                        right: 0,
                        padding: 10,
                        backgroundColor: config().BASE_COLOR,
                        borderBottomLeftRadius: 15
                    }}>
                    <Text style={{color: "#fff", fontSize: 12}}>
                        {translateText("change_phone_number")}
                    </Text>
                </TouchableOpacity>}

                <View
                    style={this.styles.scrollView}
                >
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("Name")} *`}
                            value={name}
                            error={name_error}
                            style={this.styles.defaultInputStyle}
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            onChangeText={(name) => {
                                return this.setState({name});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("Surname")} *`}
                            value={surname}
                            error={surname_error}
                            style={this.styles.defaultInputStyle}
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            onChangeText={(surname) => {
                                this.setState({surname});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("E-Mail")} *`}
                            value={email}
                            error={email_error}
                            keyboardType="email-address"
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            style={this.styles.defaultInputStyle}
                            onChangeText={(email) => {
                                this.setState({email});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <SelectInput
                            value={gender}
                            keyboardBackgroundColor="#fff"
                            buttonsBackgroundColor="#fff"
                            buttonsTextColor="#46cf98"
                            buttonsTextSize={16}
                            style={[this.styles.defaultInputStyle, {
                                paddingLeft: Platform.select({
                                    "ios": 10,
                                    "android": 15
                                }),
                            }]}
                            labelStyle={[this.styles.defaultInputTextStyle]}
                            options={[
                                {
                                    value: "",
                                    label: translateText("undefined")
                                },
                                {
                                    value: "male",
                                    label: translateText("Male")
                                },
                                {
                                    value: "female",
                                    label: translateText("Female")
                                },
                            ]}
                            onSubmitEditing={(itemValue) => {
                                return this.setState({gender: itemValue});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        {Platform.OS === "ios" ? (
                            <DatePicker
                                style={{width: "100%",}}
                                date={birthdate}
                                mode="date"
                                format="DD/MM/YYYY"
                                minDate="01/01/1920"
                                maxDate={new Date()}
                                showIcon={false}
                                placeholder={`${translateText("Birthdate")}`}
                                confirmBtnText={translateText("Confirm")}
                                cancelBtnText={translateText("Cancel")}
                                customStyles={{
                                    dateInput: [this.styles.defaultInputStyle, {
                                        alignItems: "flex-start",
                                        borderTopWidth: 0,
                                        borderLeftWidth: 0,
                                        borderRightWidth: 0,
                                    }],
                                    placeholderText: {
                                        paddingLeft: Platform.select({
                                            "ios": 10,
                                            "android": 15
                                        }),
                                        color: "#6b6b6b",
                                        fontSize: 12,
                                    },
                                    dateText: {
                                        paddingLeft: Platform.select({
                                            "ios": 10,
                                            "android": 15
                                        }),
                                        color: "#000",
                                        fontSize: 12,
                                    },
                                    btnTextConfirm: {
                                        color: "#46cf98"
                                    },
                                    btnTextCancel: {
                                        color: "#46cf98"
                                    },
                                }}
                                onDateChange={(birthdate) => {
                                    this.setState({birthdate});
                                }}
                            />
                        ) : (
                            <TextInputMask
                                type="custom"
                                placeholderTextColor="#6b6b6b"
                                value={this.state.birthdate}
                                onChangeText={(birthdate, extracted) => {

                                    this.setState({birthdate});

                                }}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                style={[this.styles.defaultInputStyle, {
                                    paddingLeft: Platform.select({
                                        "ios": 10,
                                        "android": 15
                                    }),
                                    fontSize: 12,
                                    padding: 0,
                                    zIndex: 1000
                                }]}
                                type={"datetime"}
                                options={{
                                    format: "DD/MM/YYYY"
                                }}
                                placeholder={translateText("Birthdate")}
                            />
                        )}
                    </View>
                    {!PhoneVerify.Enabled && (
                        <View style={this.styles.SectionStyle}>
                            <TextInputMask
                                type="custom"
                                placeholderTextColor="#6b6b6b"
                                value={this.state.phone}
                                onChangeText={(phone, extracted) => {
                                    if (phone.length < 15) {
                                        this.setState({phone});
                                    }
                                }}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                style={[this.styles.defaultInputStyle, {
                                    paddingLeft: Platform.select({
                                        "ios": 10,
                                        "android": 15
                                    }),
                                    fontSize: 12,
                                    padding: 0,
                                    zIndex: 1000
                                }]}
                                options={{
                                    mask: "(999) 999-9999"
                                }}
                                placeholder="Ex 512 345 6789"
                            />
                        </View>
                    )}
                    <View style={this.styles.cardContainer}>
                        <TouchableOpacity
                            style={this.styles.defaultButtonStyle}
                            onPress={() => {

                                const registerData = {
                                    name,
                                    surname,
                                    birthdate,
                                    gender,
                                    email,
                                    phone: phone && `+90${phone.match(/\d+/g)
                                        .join("")}`,
                                };

                                if (this.validateData(registerData)) {
                                    this.props.updateUser(registerData, this.props.user);
                                }
                            }}
                        >
                            <LinearGradient
                                colors={[config().BASE_FOUR_COLOR, config().BASE_THIRD_COLOR]}
                                style={this.styles.linearGradient}
                            >
                                <Text style={this.styles.linkText}>{translateText("Save")}</Text>
                                <Icon name="left-arrow" size={18} color="#fff"/>
                            </LinearGradient>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={this.styles.defaultButtonStyle}
                            onPress={() => {
                                this.props.navigation.navigate("TermAndConditions", {
                                    "mall": this.props.mall
                                });
                            }}
                        >
                            <Icon name="question" size={16} color={config().BASE_THIRD_COLOR}/>
                            <Text
                                style={[this.styles.linkText, {
                                    marginLeft: 10,
                                    color: config().BASE_THIRD_COLOR
                                }]}
                            >
                                {translateText("Privacy")}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        );
    }
}

UpdateProfileComponent2.propTypes = {
    updateUser: PropTypes.func,
    navigation: PropTypes.instanceOf(Object)
};
UpdateProfileComponent2.defaultProps = {
    updateUser: () => {
    },
    navigation: {}
};

export default UpdateProfileComponent2;
