import React from "react";
import {WebView} from "react-native-webview";
import {connect} from "react-redux";
import config from "../../../config/config";
import {getDefaultLanguageText, translateText} from "../../translation/translate";


class TermAndConditionScreen extends React.Component {
    static navigationOptions = () => {
        return ({
                title: translateText("Privacy")
                    .toUpperCase(),
                headerRight: null
            }
        );
    };

    state = {
        url: ""
    }

    UNSAFE_componentWillMount() {
        const url = getDefaultLanguageText(this.props.mall.privacy.url).indexOf(config().BASE_URL) === -1 ? config().BASE_URL + getDefaultLanguageText(this.props.mall.privacy.url) : getDefaultLanguageText(this.props.mall.privacy.url);

        if (url) {
            this.setState({
                url
            });
        }

    }


    render() {
        return (
            <WebView
                style={{flex: 1}}
                source={{uri: this.state.url}}
            />
        );
    }
}

const mapStateToProps = ({main}) => {
    return {mall: main.payload.mall};
};

const TermAndConditionScreenRedux = connect(mapStateToProps)(TermAndConditionScreen);

export {TermAndConditionScreenRedux as TermAndConditionScreen};
