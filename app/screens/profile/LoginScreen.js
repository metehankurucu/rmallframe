import {TouchableOpacity} from "react-native";
import React from "react";
import {CachedImage} from "react-native-img-cache";
import {connect} from "react-redux";
import {DrawerActions} from "react-navigation";
import {facebookLogin, googleLogin, loginUser} from "../../actions/Profile";
import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import LoginComponent1 from "./LoginComponents/LoginComponent1";
import {Icon} from "../../components/common/";

class LoginScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("LOGIN")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("LOGIN")
                .toUpperCase(),
        };
    };

    render() {
        const {Enabled, Component} = config().LoginScreen;

        if (!Enabled) {
            return (
                <LoginComponent1 {...this.props}/>

            );
        }
        return (
            <Component {...this.props}/>
        );
    }

}

const mapStateToProps = (state) => {
    return state;
};

const LoginRedux = connect(mapStateToProps, {
    loginUser,
    facebookLogin,
    googleLogin
})(LoginScreen);

export {LoginRedux as LoginScreen};
