import React from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import config from "../../../config/config";
import {translateText} from "../../translation/translate";
import {forgetPassword} from "../../actions/Profile";
import ForgotPasswordComponent2 from "./ForgotPasswordComponents/ForgotPasswordComponent2";

class ForgotPasswordScreen extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText("ForgotPassword")
                .toUpperCase(),
            headerRight: null
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            email_error: ""
        };
    }

    styles = {
        linearGradient: {
            padding: 10,
            borderRadius: 30,
            flexDirection: "row",
            alignSelf: "center",
            justifyContent: "space-between",
            flex: 0.6
        },
        loginButton: {
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
        },
        buttonText: {
            color: "#fff",
            fontSize: 15,
            alignSelf: "center"
        },
        container: {
            flex: 1,
            justifyContent: "center",
            flexDirection: "row",
            backgroundColor: "#fff"
        },
        defaultInputStyle: {
            borderColor: "#555",
            width: "100%",
            height: 40,
            borderBottomWidth: StyleSheet.hairlineWidth,
            padding: 8,
            justifyContent: "center",
        },
        defaultInputTextStyle: {
            fontSize: 13
        },
        cardContainer: {
            margin: 8
        },
        defaultLogoImage: {
            width: 100,
            height: 100,
            marginTop: 20,
            alignSelf: "center"
        }
    };

    render() {
        const {Enabled, Component} = config().ForgetPasswordScreen;

        if (!Enabled) {
            return (
                <ForgotPasswordComponent2 {...this.props}/>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

ForgotPasswordScreen.propTypes = {
    forgetPassword: PropTypes.func
};

const mapStateToProps = ({auth}) => {
    const {
        email, password, error, loading
    } = auth;
    return {
        email,
        password,
        error,
        loading
    };
};

const ForgetPasswordRedux = connect(mapStateToProps, {
    forgetPassword
})(ForgotPasswordScreen);

export {ForgetPasswordRedux as ForgetPasswordScreen};
