import React, { Component } from "react";
import { Alert, Platform, ScrollView, StyleSheet, View } from "react-native";
import SelectInput from "react-native-select-input-ios";
import DatePicker from "react-native-datepicker";
import { TextInputMask } from "react-native-masked-text";
import { CachedImage } from "react-native-img-cache";

import config from "../../../../config/config";
import { translateText } from "../../../translation/translate";
import { Input } from "../../../components/common";
import { BorderlessButton } from "../../../components/buttons";
import RoundedCheckBox, { LABEL_POSITION } from "../../../components/common/RoundedCheckBox";

const email_filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;

class RegisterComponent1 extends Component {
    state = {
        name: "",
        surname: "",
        email: "",
        password: "",
        password_again: "",
        gender: "male",
        phone: "",
        birthdate: "",
        checked: false,

        birthdate_error: false,
        name_error: false,
        surname_error: false,
        email_error: false,
        password_error: false,
        phone_error: false,
        password_again_error: false,
    };

    styles = {
        defaultInputStyle: {
            backgroundColor: "#fff",
            borderColor: "#555",
            width: "100%",
            height: 40,
            fontSize: 14,
            borderWidth: 1,
            color: "#000",
            justifyContent: "center",
        },
        buttonContainer: {
            marginTop: 15,
            width: "70%"
        },
        cardContainer: {
            marginTop: 10,
            marginBottom: 30,
            alignItems: "center",
            justifyContent: "center",
            width: "100%"
        },
        defaultButtonStyle: {
            width: "100%",
            height: 40,
            backgroundColor: config().BASE_SECOND_COLOR,
            justifyContent: "center"
        },
        borderlessButton: {
            color: config().BASE_SECOND_COLOR
        },
        scrollView: {
            padding: 15
        },
        backgroundImage: {
            backgroundColor: "#ccc",
            resizeMode: "cover",
            position: "absolute",
            width: "100%",
            height: "100%",
            justifyContent: "center",
        },
        SectionStyle: {
            marginTop: 10
        },
        container: {
            flex: 1
        },
        helpText: {
            fontSize: 12,
            color: config().BASE_SECOND_COLOR,
            fontStyle: "italic",
            fontWeight: "100"
        }
    };

    validateData = (data) => {
        let name_error = false,
            surname_error = false,
            email_error = false,
            birthday_error = false,
            phone_error = false,
            password_error = false,
            password_again_error = false,
            errorText = "";

        if (data.name === "") {
            name_error = true;
            errorText = translateText("empty_value");
        }
        if (data.surname === "") {
            surname_error = true;
            errorText = translateText("empty_value");
        }
        if (!email_filter.test(data.email)) {
            email_error = true;
            if (errorText === "") {
                errorText = translateText("email_not_valid");
            }
        }
        if (data.birthdate === "") {
            birthday_error = true;
            errorText = translateText("empty_value");
        }
        if (data.phone.length !== 13) {
            phone_error = true;
            errorText = translateText("empty_value");
        }
        if (data.password === "") {
            password_error = true;
            errorText = translateText("empty_value");
        }
        if (data.password_again === "" || data.password !== data.password_again) {
            password_again_error = true;
            if (errorText === "") {
                errorText = translateText("passwords_not_match");
            }
        }

        if (errorText !== "") {
            Alert.alert(translateText("error"), errorText);
        }

        this.setState({
            name_error,
            surname_error,
            email_error,
            birthday_error,
            phone_error,
            password_error,
            password_again_error
        });

        this.scroll.scrollTo({
            x: 0,
            y: 0,
            animated: true
        });

        return !(name_error || surname_error || email_error || birthday_error || phone_error || password_again_error || password_error);
    };

    render() {
        const {
            name, name_error, surname, surname_error, phone, birthdate, birthdate_error, email, email_error, gender, password, password_error, password_again, password_again_error
        } = this.state;

        return (
            <View style={this.styles.container}>
                {Platform.OS === "ios" && <CachedImage
                    style={{
                        ...StyleSheet.absoluteFillObject,
                        width: "100%",
                        height: "100%",
                        opacity: 0.05
                    }}
                    source={require("../../../../assets/images/pattern.png")}
                    resizeMode="repeat"
                />}

                <ScrollView
                    style={this.styles.scrollView}
                    ref={(ref) => {
                        this.scroll = ref;
                    }}
                >
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("Name")} *`}
                            value={name}
                            error={name_error}
                            style={this.styles.defaultInputStyle}
                            onChangeText={(name) => {
                                return this.setState({ name });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("Surname")} *`}
                            value={surname}
                            error={surname_error}
                            style={this.styles.defaultInputStyle}
                            onChangeText={(surname) => {
                                this.setState({ surname });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("E-Mail")} *`}
                            value={email}
                            error={email_error}
                            keyboardType="email-address"
                            style={this.styles.defaultInputStyle}
                            onChangeText={(email) => {
                                this.setState({ email });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <SelectInput
                            value={gender}
                            keyboardBackgroundColor="#fff"
                            buttonsBackgroundColor="#fff"
                            buttonsTextColor="#46cf98"
                            buttonsTextSize={16}
                            style={{
                                borderWidth: 1,
                                borderColor: "#555",
                            }}
                            labelStyle={{
                                backgroundColor: "#fff",
                                width: "100%",
                                height: 40,
                                fontSize: 14,
                                color: "#000",
                                justifyContent: "center",
                                padding: 10,
                            }}
                            options={[
                                {
                                    value: "male",
                                    label: translateText("Male")
                                },
                                {
                                    value: "female",
                                    label: translateText("Female")
                                },
                            ]}
                            onSubmitEditing={(itemValue) => {
                                return this.setState({ gender: itemValue });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <DatePicker
                            style={{ width: "100%", }}
                            date={birthdate}
                            mode="date"
                            error={birthdate_error}
                            format="DD/MM/YYYY"
                            minDate="01/01/1920"
                            maxDate={new Date()}
                            showIcon={false}
                            placeholder={`${translateText("Birthdate")} *`}
                            confirmBtnText={translateText("Confirm")}
                            cancelBtnText={translateText("Cancel")}
                            customStyles={{
                                dateInput: [this.styles.defaultInputStyle, {
                                    alignItems: "flex-start"
                                }],
                                placeholderText: {
                                    paddingLeft: 10,
                                    color: "#6b6b6b",
                                    fontSize: 14,
                                },
                                dateText: {
                                    paddingLeft: 10,
                                    color: "#000",
                                    fontSize: 14,
                                },
                                btnTextConfirm: {
                                    color: "#46cf98"
                                },
                                btnTextCancel: {
                                    color: "#46cf98"
                                },
                            }}
                            onDateChange={(birthdate) => {
                                this.setState({ birthdate });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <TextInputMask
                            type={"custom"}
                            placeholderTextColor="#6b6b6b"
                            underlineColorAndroid="rgba(0,0,0,0)"
                            value={this.state.phone}
                            onChangeText={(phone, extracted) => {
                                if (phone.length < 15) {
                                    this.setState({ phone });
                                }
                            }}
                            style={[this.styles.defaultInputStyle, { paddingLeft: 10 }]}
                            options={{
                                /**
                                 * mask: (String | required | default '')
                                 * the mask pattern
                                 * 9 - accept digit.
                                 * A - accept alpha.
                                 * S - accept alphanumeric.
                                 * * - accept all, EXCEPT white space.
                                 */
                                mask: "(999) 999-9999"
                            }}
                            placeholder="Ex 512 345 6789"
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            secureTextEntry
                            placeholder={`${translateText("Password")} *`}
                            style={this.styles.defaultInputStyle}
                            error={password_error}
                            value={password}
                            onChangeText={(password) => {
                                this.setState({ password });
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            secureTextEntry
                            style={this.styles.defaultInputStyle}
                            error={password_again_error}
                            placeholder={`${translateText("Password Again")} *`}
                            value={password_again}
                            onChangeText={(password_again) => {
                                this.setState({ password_again });
                            }}
                        />
                    </View>


                    <View style={this.styles.cardContainer}>
                        <RoundedCheckBox
                            outerColor={config().BASE_COLOR}
                            innerColor={config().BASE_COLOR}
                            checked={this.state.checked}
                            labelPosition={LABEL_POSITION.RIGHT}
                            label={translateText("accept_term_text")}
                            onToggle={(checked) => {
                                this.setState({ checked });
                            }}
                            onChange={() => {
                                this.props.navigation.navigate("TermAndConditions");
                            }}
                        />
                        <View style={this.styles.buttonContainer}>
                            <BorderlessButton
                                title={translateText("Register")}
                                style={this.styles.defaultButtonStyle}
                                onPress={() => {
                                    if (!this.state.checked) {
                                        Alert.alert(translateText("warn"), "You must check term and conditions.");
                                        return;
                                    }

                                    const registerData = {
                                        name,
                                        surname,
                                        gender,
                                        email,
                                        phone: "+90" + phone.match(/\d+/g).join(""),
                                        birthdate,
                                        password,
                                        password_again
                                    };

                                    if (this.validateData(registerData)) {
                                        this.props.registerUser(registerData);
                                    }
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>

        );
    }
}

RegisterComponent1.propTypes = {};

export default RegisterComponent1;
