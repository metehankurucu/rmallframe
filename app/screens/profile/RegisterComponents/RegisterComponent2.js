import React, {Component} from "react";
import {Alert, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import SelectInput from "react-native-select-input-ios";
import DatePicker from "react-native-datepicker";
import PropTypes from "prop-types";
import {TextInputMask} from "react-native-masked-text";
import {CachedImage} from "react-native-img-cache";
import LinearGradient from "react-native-linear-gradient";

import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {Icon, Input} from "../../../components/common";

const email_filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;

class RegisterComponent2 extends Component {
    state = {
        name: "",
        surname: "",
        email: "",
        password: "",
        password_again: "",
        gender: "empty",
        phone: "",
        birthdate: "",

        name_error: false,
        surname_error: false,
        email_error: false,
        password_error: false,
        phone_error: false,
        password_again_error: false,
    };

    styles = {
        welcomeText: {
            fontSize: 16,
            fontWeight: "800",
            color: config().BASE_FOUR_COLOR,
            margin: 5
        },
        linkText: {
            fontSize: 13,
            color: "#fff"
        },
        defaultInputStyle: {
            borderColor: "#555",
            width: "100%",
            height: 32,
            borderBottomWidth: StyleSheet.hairlineWidth,
            justifyContent: "center",
        },
        defaultInputTextStyle: {
            fontSize: 12,
            color: "#000",
        },
        cardContainer: {
            alignItems: "center",
            justifyContent: "center",
            flex: 0.7,
            alignSelf: "center"
        },
        defaultButtonStyle: {
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginTop: 15,
        },
        linearGradient: {
            padding: 10,
            borderRadius: 30,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            flex: 0.35
        },
        scrollView: {
            padding: 15
        },
        SectionStyle: {
            width: "70%",
            alignSelf: "center"
        },
        container: {
            flex: 1,
            backgroundColor: "#fff"
        },
        helpText: {
            fontSize: 12,
            color: config().BASE_SECOND_COLOR,
            fontStyle: "italic",
            fontWeight: "100"
        }
    };

    validateData = (data) => {
        let name_error = false,
            surname_error = false,
            email_error = false,
            phone_error = false,
            password_error = false,
            password_again_error = false,
            errorText = "";

        if (data.name === "") {
            name_error = true;
            errorText = translateText("empty_value");
        }
        if (data.surname === "") {
            surname_error = true;
            errorText = translateText("empty_value");
        }
        if (!email_filter.test(data.email)) {
            email_error = true;
            if (errorText === "") {
                errorText = translateText("email_not_valid");
            }
        }
        if (data.phone.length !== 13) {
            phone_error = true;
            errorText = translateText("empty_value");
        }
        if (data.password === "") {
            password_error = true;
            errorText = translateText("empty_value");
        }
        if (data.password_again === "" || data.password !== data.password_again) {
            password_again_error = true;
            if (errorText === "") {
                errorText = translateText("passwords_not_match");
            }
        }

        if (errorText !== "") {
            Alert.alert(translateText("error"), errorText);
        }

        this.setState({
            name_error,
            surname_error,
            email_error,
            phone_error,
            password_error,
            password_again_error
        });

        this.scroll.scrollTo({
            x: 0,
            y: 0,
            animated: true
        });

        return !(name_error || surname_error || email_error || phone_error || password_again_error || password_error);
    };

    render() {
        const {
            name,
            name_error,
            surname,
            surname_error,
            phone,
            birthdate,
            email,
            email_error,
            gender,
            password,
            password_error,
            password_again,
            password_again_error
        } = this.state;

        const {PlaceHolderImage} = config();

        return (
            <View style={this.styles.container}>
                {Platform.OS === "ios" && <CachedImage
                    style={{
                        ...StyleSheet.absoluteFillObject,
                        width: "100%",
                        height: "100%",
                        opacity: 0.05
                    }}
                    source={require("../../../../assets/images/pattern.png")}
                    resizeMode="repeat"
                />}

                <ScrollView
                    style={this.styles.scrollView}
                    contentContainerStyle={{paddingBottom: 30}}
                    ref={(ref) => {
                        this.scroll = ref;
                    }}
                >
                    <View style={{alignItems: "center"}}>
                        <CachedImage
                            source={PlaceHolderImage}
                            resizeMode="contain"
                            style={{
                                width: "50%",
                                height: 100
                            }}
                        />
                        <Text style={this.styles.welcomeText}>{translateText("register_now")
                            .toUpperCase()}
                        </Text>
                    </View>

                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("Name")} *`}
                            value={name}
                            error={name_error}
                            style={this.styles.defaultInputStyle}
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            onChangeText={(name) => {
                                return this.setState({name});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("Surname")} *`}
                            value={surname}
                            error={surname_error}
                            style={this.styles.defaultInputStyle}
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            onChangeText={(surname) => {
                                this.setState({surname});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            placeholder={`${translateText("E-Mail")} *`}
                            value={email}
                            error={email_error}
                            keyboardType="email-address"
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            style={this.styles.defaultInputStyle}
                            onChangeText={(email) => {
                                this.setState({email});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <SelectInput
                            value={gender}
                            keyboardBackgroundColor="#fff"
                            buttonsBackgroundColor="#fff"
                            buttonsTextColor="#46cf98"
                            buttonsTextSize={16}
                            style={[this.styles.defaultInputStyle, {
                                paddingLeft: Platform.select({
                                    "ios": 10,
                                    "android": 15
                                }),
                            }]}
                            labelStyle={[this.styles.defaultInputTextStyle]}
                            options={[
                                {
                                    value: "empty",
                                    label: translateText("select_gender_text")
                                },
                                {
                                    value: "male",
                                    label: translateText("Male")
                                },
                                {
                                    value: "female",
                                    label: translateText("Female")
                                },
                            ]}
                            onSubmitEditing={(itemValue) => {
                                return this.setState({gender: itemValue});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        {Platform.OS === "ios" ? (
                            <DatePicker
                                style={{width: "100%",}}
                                date={birthdate}
                                mode="date"
                                format="DD/MM/YYYY"
                                minDate="01/01/1920"
                                maxDate={new Date()}
                                showIcon={false}
                                placeholder={`${translateText("Birthdate")}`}
                                confirmBtnText={translateText("Confirm")}
                                cancelBtnText={translateText("Cancel")}
                                customStyles={{
                                    dateInput: [this.styles.defaultInputStyle, {
                                        alignItems: "flex-start",
                                        borderTopWidth: 0,
                                        borderLeftWidth: 0,
                                        borderRightWidth: 0,
                                    }],
                                    placeholderText: {
                                        paddingLeft: Platform.select({
                                            "ios": 10,
                                            "android": 15
                                        }),
                                        color: "#6b6b6b",
                                        fontSize: 12,
                                    },
                                    dateText: {
                                        paddingLeft: Platform.select({
                                            "ios": 10,
                                            "android": 15
                                        }),
                                        color: "#000",
                                        fontSize: 12,
                                    },
                                    btnTextConfirm: {
                                        color: "#46cf98"
                                    },
                                    btnTextCancel: {
                                        color: "#46cf98"
                                    },
                                }}
                                onDateChange={(birthdate) => {
                                    this.setState({birthdate});
                                }}
                            />
                        ) : (
                            <TextInputMask
                                type="custom"
                                placeholderTextColor="#6b6b6b"
                                value={this.state.birthdate}
                                onChangeText={(birthdate, extracted) => {

                                    this.setState({birthdate});

                                }}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                style={[this.styles.defaultInputStyle, {
                                    paddingLeft: Platform.select({
                                        "ios": 10,
                                        "android": 15
                                    }),
                                    fontSize: 12,
                                    padding: 0,
                                    zIndex: 1000
                                }]}
                                type={"datetime"}
                                options={{
                                    format: "DD/MM/YYYY"
                                }}
                                placeholder={translateText("Birthdate")}
                            />
                        )}
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <TextInputMask
                            type="custom"
                            placeholderTextColor="#6b6b6b"
                            value={this.state.phone}
                            onChangeText={(phone, extracted) => {
                                if (phone.length < 15) {
                                    this.setState({phone});
                                }
                            }}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            style={[this.styles.defaultInputStyle, {
                                paddingLeft: Platform.select({
                                    "ios": 10,
                                    "android": 15
                                }),
                                fontSize: 12,
                                padding: 0,
                                zIndex: 1000
                            }]}
                            options={{
                                mask: "(999) 999-9999"
                            }}
                            placeholder="Ex 512 345 6789 *"
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            secureTextEntry
                            placeholder={`${translateText("Password")} *`}
                            style={this.styles.defaultInputStyle}
                            error={password_error}
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            value={password}
                            onChangeText={(password) => {
                                this.setState({password});
                            }}
                        />
                    </View>
                    <View style={this.styles.SectionStyle}>
                        <Input
                            secureTextEntry
                            style={this.styles.defaultInputStyle}
                            error={password_again_error}
                            inputTextStyle={this.styles.defaultInputTextStyle}
                            placeholder={`${translateText("Password Again")} *`}
                            value={password_again}
                            onChangeText={(password_again) => {
                                this.setState({password_again});
                            }}
                        />
                    </View>

                    <Text style={{
                        color: "#6b6b6b",
                        fontSize: 11,
                        alignSelf: "center",
                        marginTop: 10
                    }}
                    >
                        {translateText("required_fields_text")}
                    </Text>

                    <View style={this.styles.cardContainer}>
                        <TouchableOpacity
                            style={this.styles.defaultButtonStyle}
                            onPress={() => {
                                const registerData = {
                                    name,
                                    surname,
                                    gender: gender === "empty" ? "" : gender,
                                    email,
                                    phone: phone && `+90${phone.match(/\d+/g)
                                        .join("")}`,
                                    birthdate,
                                    password,
                                    password_again
                                };

                                if (this.validateData(registerData)) {
                                    this.props.registerUser(registerData);
                                }
                            }}
                        >
                            <LinearGradient
                                colors={[config().BASE_FOUR_COLOR, config().BASE_THIRD_COLOR]}
                                style={this.styles.linearGradient}
                            >
                                <Text style={this.styles.linkText}>{translateText("Register")}</Text>
                                <Icon name="left-arrow" size={18} color="#fff"/>
                            </LinearGradient>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={this.styles.defaultButtonStyle}
                            onPress={() => {
                                this.props.navigation.navigate("TermAndConditions", {
                                    "mall": this.props.mall
                                });
                            }}
                        >
                            <Icon name="question" size={16} color={config().BASE_THIRD_COLOR}/>
                            <Text
                                style={[this.styles.linkText, {
                                    marginLeft: 10,
                                    color: config().BASE_THIRD_COLOR
                                }]}
                            >
                                {translateText("Privacy")}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>

        );
    }
}

RegisterComponent2.propTypes = {
    registerUser: PropTypes.func,
    navigation: PropTypes.instanceOf(Object)
};
RegisterComponent2.defaultProps = {
    registerUser: () => {
    },
    navigation: {}
};

export default RegisterComponent2;
