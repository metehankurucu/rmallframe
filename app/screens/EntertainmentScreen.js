import React from "react";
import {connect} from "react-redux";
import {TouchableOpacity} from "react-native";
import {DrawerActions} from "react-navigation";
import _ from "lodash";

import {translateText} from "../translation/translate";
import ParentCategoryItem from "../components/stores/ParentCategoryItem";
import config from "../../config/config";
import {BrandListScreen} from "./stores/BrandListScreen";
import {Icon} from "../components/common/";

class EntertainmentScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("Entertainment")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("Entertainment")
                .toUpperCase(),
        };
    };

    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this);
    }

    _renderItem = ({item}) => {
        return (
            <ParentCategoryItem
                item={item}
                onPress={() => {
                    this.props.navigation.navigate("StoresList", {
                        stores: item.stores,
                        title: item.name
                    });
                }}
            />
        );
    };

    render() {
        const {Enabled, Component} = config().EntertainmentScreen;

        if (!Enabled) {
            const data = {...this.props.navigation, ...{state: {params: {stores: this.props.category.stores}}}};

            return (
                <BrandListScreen navigation={data}/>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({main, auth}) => {
    const {payload} = main;
    const {user} = auth;

    if (payload !== null) {
        const {categories, mall} = payload;
        const st = payload.stores;
        const {FunCategoryId} = config().EntertainmentScreen;

        if (!FunCategoryId && mall.fun_category) {
            throw new Error("You must declare FunCategoryId on config file...");
        }

        const category = _.find(categories, {"_id": FunCategoryId || mall.fun_category});

        category.stores = [...st.filter((store) => {
            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_stores"].find((obj) => {
                    return obj["s"] === store["_id"];
                });
                store["fav"] = !!match;
                store["userid"] = user["_id"];
            }

            return store.categories.filter((cat) => {
                return cat["s"] === category["_id"];
            }).length > 0;
        })];

        return {category};
    }
    return {category: null};
};

const EntertainmentRedux = connect(mapStateToProps)(EntertainmentScreen);

export {EntertainmentRedux as EntertainmentScreen};
