/* eslint-disable global-require */
import React, { Component } from "react";
import { Alert, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CachedImage } from "react-native-img-cache";
import PropTypes from "prop-types";

import { getDefaultLanguageText, translateText } from "../../../translation/translate";
import config from "../../../../config/config";
import { DefaultButton } from "../../../components/buttons";
import ModalSelector from "../../../components/common/modal/ModalSelector";
import { Button } from "../../../components/common";

class WayfinderComponent1 extends Component {

    state = {
        to: null,
        from: null,
        selectionActive: "",
    };

    returnData = (data) => {
        const params = {};
        if (data.type === "beacons") {
            params[this.state.selectionActive] = {
                title: translateText("your_location"),
                type: "beacons",
                your_location: true
            };
        }
        else if (data.type === "parking") {
            params[this.state.selectionActive] = {
                title: translateText("parking_location"),
                parking_location: true,
                id: `${this.props.parkingBeacon.n}_parkingspot`,
            };
        }
        else {
            params[this.state.selectionActive] = data;
        }
        this.setState(params);
    };
    _renderNavigationButton = () => {
        if (!this.state.from || !this.state.to) {
            return (
                <Button
                    style={this.styles.navigateButton}
                    onPress={() => {
                        if (!this.state.from || !this.state.to) {
                            Alert.alert(translateText("warn"), translateText("Please_select_locations"));
                        }
                    }}
                >
                    <Text style={this.styles.navigateButtonText}>{
                        translateText("Draw route")
                            .toUpperCase()}
                    </Text>
                </Button>
            );
        }

        const { UseMultipleNavigationWay, NavigationWay } = config();

        if (UseMultipleNavigationWay) {
            const locations = [{
                key: "disabled",
                label: translateText("use_elevators")
            }, {
                key: "pedestrian",
                label: translateText("use_escalators")
            }];

            return (
                <ModalSelector
                    data={locations}
                    cancelText={translateText("Cancel")}
                    disabled={!this.state.from || !this.state.to}
                    buttonStyle={this.styles.navigateButton}
                    onChange={(option) => {
                        if (this.props.internetConnection) {
                            const { from, to } = this.state;

                            if (from && to) {
                                if (from.type === "beacons") {
                                    const beacons = this.props.liveBeacon;

                                    this.props.NewSP("", to, option.type, beacons);
                                }
                                else if (to.type === "beacons") {
                                    const beacons = this.props.liveBeacon;
                                    this.props.NewSP(from, "", option.type, beacons);
                                }
                                else {
                                    this.props.NewSP(from, to, option.key);
                                }
                            }
                        }
                        else {
                            Alert.alert("Internet Connection", "Need Internet Connection.");
                        }
                    }}
                >
                    <Text style={this.styles.navigateButtonText}>{
                        translateText("Draw route")
                            .toUpperCase()}
                    </Text>
                </ModalSelector>
            );
        }

        return (
            <Button
                style={this.styles.navigateButton}
                onPress={() => {
                    const { from, to } = this.state;

                    if (!from || !to) {
                        Alert.alert("warn", translateText("Please_select_locations"));
                    }
                    if (this.props.internetConnection) {
                        if (from && to) {
                            if (from.type === "beacons") {
                                const beacons = this.props.liveBeacon;

                                this.props.NewSP("", to, NavigationWay, beacons);
                            }
                            else if (to.type === "beacons") {
                                const beacons = this.props.liveBeacon;
                                this.props.NewSP(from, "", NavigationWay, beacons);
                            }
                            else {
                                this.props.NewSP(from, to, NavigationWay);
                            }
                        }
                    }
                    else {
                        Alert.alert("Internet Connection", "Need Internet Connection.");
                    }
                }}
            >
                <Text style={this.styles.navigateButtonText}>{
                    translateText("Draw route")
                        .toUpperCase()}
                </Text>
            </Button>
        );
    };
    styles = StyleSheet.create({
        defaultText: {
            fontSize: 13,
            color: config().BASE_SECOND_COLOR,
        },
        navigateButtonText: {
            fontSize: 13,
            color: "#fff",
            padding: 6,
            alignSelf: "center"
        },
        navigateButton: {
            backgroundColor: config().BASE_COLOR,
            alignItems: "center",
            justifyContent: "center",
            alignSelf: "center",
            width: "50%",
            elevation: 1,
            height: 35,
            borderRadius: 20
        },
        container: {
            padding: 15,
            justifyContent: "center",
            flex: 1,
            flexDirection: "row",
            backgroundColor: "#fff",
        },
        selectRootContainer: {
            marginLeft: 20,
            marginRight: 20,
            maxWidth: "70%"
        },
        rootChild: {
            marginTop: 10,
            marginBottom: 10,
            borderWidth: StyleSheet.hairlineWidth
        },
        selectContainer: {
            height: 35,
            padding: 8,
            justifyContent: "center",
            alignItems: "flex-start",
            width: "100%",
        },
        selectText: {
            fontSize: 14,
            width: "100%",
            color: "#555"
        }
    });

    UNSAFE_componentWillMount() {
        if (this.props.navigation.state.params && (this.props.navigation.state.params.to || this.props.navigation.state.params.from)) {
            const { params } = this.props.navigation.state;
            if (params) {

                if (params.to) {
                    this.setState({
                        to: params.to
                    });
                }

                if (params.from) {
                    this.setState({
                        from: params.from
                    });
                }
            }
        }
    }

    render() {
        const { HeaderImage } = config().WayfinderScreen;
        const { from, to } = this.state
        return (
            <View style={this.styles.container}>
                <View>
                    <CachedImage
                        style={{
                            alignSelf: "center",
                            width: "100%",
                            margin: 15
                        }}
                        resizeMode="contain"
                        source={HeaderImage}
                    />
                    <View
                        style={{
                            flexDirection: "row",
                            alignItems: "center",
                        }}
                    >
                        <CachedImage
                            style={{
                                width: 25,
                                marginBottom: 20
                            }}
                            resizeMode="contain"
                            source={require("../../../../assets/icons/ic_wayfinder_icons.png")}
                        />

                        <View style={this.styles.selectRootContainer}>
                            <View style={this.styles.rootChild}>
                                <DefaultButton
                                    style={[this.styles.selectContainer, { opacity: from && from.parking_location ? 0.5 : 1 }]}
                                    titleStyle={this.styles.selectText}
                                    title={from ? getDefaultLanguageText(from.title) : translateText("choose_starting_point")}
                                    onPress={() => {
                                        this.setState({
                                            selectionActive: "from"
                                        });

                                        this.props.navigation.navigate("WayfinderStoreList", {
                                            returnData: this.returnData.bind(this),
                                            title: "choose_starting_point",
                                            selectionActive: "from",
                                            parking_location: to && !!to.parking_location
                                        });
                                    }}
                                />
                            </View>
                            <View>
                                <Text
                                    style={this.styles.defaultText}
                                >{translateText("location_of_nearest_store")}
                                </Text>
                            </View>
                            <View style={this.styles.rootChild}>
                                <DefaultButton
                                    style={[this.styles.selectContainer, { opacity: to && to.parking_location ? 0.5 : 1 }]}
                                    titleStyle={this.styles.selectText}
                                    title={to ? getDefaultLanguageText(to.title) : translateText("choose_destination")}
                                    onPress={() => {
                                        if (to !== null || !to) {
                                            this.setState({
                                                selectionActive: "to"
                                            });
                                            this.props.navigation.navigate("WayfinderStoreList", {
                                                returnData: this.returnData.bind(this),
                                                title: "choose_destination",
                                                selectionActive: "to",
                                                parking_location: from && !!from.parking_location

                                            });
                                        }
                                    }}
                                />
                            </View>
                            <View>
                                <Text
                                    style={this.styles.defaultText}
                                >{translateText("location_of_desired_store")}
                                </Text>
                            </View>
                        </View>

                        <TouchableOpacity onPress={() => {
                            if (from && !from.your_location) {
                                this.setState({
                                    from: this.state.to,
                                    to: from
                                });
                            }
                        }}
                        >
                            <View style={{
                                width: 33,
                                height: 25,
                                marginBottom: 35,
                                backgroundColor: config().BASE_COLOR,
                                alignItems: "center",
                                justifyContent: "center",
                            }}
                            >
                                <CachedImage
                                    style={{
                                        width: 25,
                                        height: 25
                                    }}
                                    resizeMode="contain"
                                    source={require("../../../../assets/icons/degistir.png")}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        marginTop: 20,
                        shadowColor: "#000",
                        shadowOpacity: 0.3,
                        shadowOffset: {
                            width: 0,
                            height: 2
                        }
                    }}
                    >
                        {this._renderNavigationButton()}
                    </View>
                </View>
            </View>
        );
    }
}

WayfinderComponent1.propTypes = {
    liveBeacon: PropTypes.instanceOf(Array),
    parkingBeacon: PropTypes.instanceOf(Object),
    navigation: PropTypes.instanceOf(Object),
    NewSP: PropTypes.func,
    internetConnection: PropTypes.bool
};

export default WayfinderComponent1;
