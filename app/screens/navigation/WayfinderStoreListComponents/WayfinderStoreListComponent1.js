/* eslint-disable global-require */
import React, {Component} from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import ScrollableTabView from "react-native-scrollable-tab-view";
import {CachedImage} from "react-native-img-cache";
import PropTypes from "prop-types";

import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {SearchView} from "../../../components/common";
import {ServiceScreen} from "../../index";
import {DefaultTabBar} from "../../../components/common/tabbar/DefaultTabBar";

class WayfinderStoreListComponent1 extends Component {
    state = {
        searchTerm: "",
    };

    _renderYourLocationButton() {
        const {selectionActive} = this.props.navigation.state.params;

        if (selectionActive !== "from") {
            return null;
        }

        return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={{
                    justifyContent: "center",
                    flexDirection: "row",
                    padding: 6,
                    opacity: config().beaconMode ? 1 : 0.7
                }}

                onPress={() => {
                    if (config().beaconMode) {
                        this.props.navigation.state.params.returnData({
                            type: "beacons",
                        });
                        this.props.navigation.goBack();
                    }
                }}
            >

                <CachedImage
                    style={{
                        width: 30,
                        height: 30,
                        resizeMode: "contain"
                    }}
                    source={require("../../../../assets/icons/bluetooth_location.png")}
                />
                <Text style={this.styles.yourLocationText}>{translateText("your_location")}</Text>
            </TouchableOpacity>
        );
    }

    _renderParkingSpotButton() {
        const {parking_location} = this.props.navigation.state.params;

        if (!this.props.parkingSpot || parking_location) {
            return null;
        }

        return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={{
                    justifyContent: "center",
                    flexDirection: "row",
                    padding: 6,
                }}

                onPress={() => {
                    this.props.navigation.state.params.returnData({
                        type: "parking",
                    });
                    this.props.navigation.goBack();
                }}
            >

                <CachedImage
                    style={{
                        width: 30,
                        height: 30,
                        resizeMode: "contain"
                    }}
                    source={require("../../../../assets/icons/wayfinder_park_icon.png")}
                />
                <Text style={this.styles.yourLocationText}>{translateText("parking_location")}</Text>
            </TouchableOpacity>
        );
    }


    styles = StyleSheet.create({
        searchInput: {
            padding: 10,
            borderColor: "#999",
            borderBottomWidth: 1,
            fontWeight: "bold",
            fontSize: 15,
            height: 40
        },
        container: {
            flex: 1,
            marginTop: 15,
        },
        rootContainer: {
            flex: 1
        },
        tabBar: {
            backgroundColor: "transparent",
            borderWidth: StyleSheet.hairlineWidth,
            height: 40,
        },
        indicator: {
            backgroundColor: "transparent"
        },
        labelStyle: {
            fontSize: 15,
        },
        underlineStyle: {
            backgroundColor: "transparent"
        },
        tab: {
            borderWidth: 0,
            margin: 8,
            height: "auto"
        },
        yourLocationText: {
            fontSize: 18,
            color: config().BASE_SECOND_COLOR
        }
    });


    render() {
        const {beaconMode, AtoZListScreen} = config();


        return (
            <View style={{
                flex: 1,
                backgroundColor: "#fff"
            }}
            >
                <SearchView
                    onChangeText={(searchTerm) => {
                        this.setState({searchTerm});
                    }}
                    value={this.state.searchTerm}
                    style={this.styles.searchInput}
                    placeholder={translateText("Search")}
                    placeholderTextColor="#000"
                />
                <View style={{
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                }}
                >
                    {beaconMode && this._renderYourLocationButton()}
                    {beaconMode && this._renderParkingSpotButton()}
                </View>
                <ScrollableTabView
                    scrollWithoutAnimation
                    onChangeTab={() => {
                        this.setState({
                            searchTerm: ""
                        });
                    }}
                    renderTabBar={() => {
                        return (
                            <DefaultTabBar
                                underlineStyle={this.styles.underlineStyle}
                                textStyle={this.styles.labelStyle}
                                style={this.styles.tab}
                                tabStyle={this.styles.tabBar}
                                activeTextColor={config().BASE_SECOND_COLOR}
                                activeBorderColor={config().BASE_SECOND_COLOR}
                                inactiveBorderColor="#ccc"
                                inactiveTextColor="#ccc"
                            />
                        );
                    }}
                >

                    <AtoZListScreen.Component
                        key="atoz"
                        tabLabel={translateText("a_z")}
                        navigation={this.props.navigation}
                        searchTerm={this.state.searchTerm}
                        wayfinderList
                    />

                    <ServiceScreen
                        key="services"
                        tabLabel={translateText("Services")}
                        navigation={this.props.navigation}
                        searchTerm={this.state.searchTerm}
                        wayfinderList
                    />

                </ScrollableTabView>
            </View>
        );
    }
}

WayfinderStoreListComponent1.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    parkingSpot: PropTypes.bool
};

export default WayfinderStoreListComponent1;

