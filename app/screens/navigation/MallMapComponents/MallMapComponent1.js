// eslint-disable-next-line import/extensions
/* eslint-disable import/extensions,import/no-unresolved */
import React from "react";
import { Alert, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { WebView } from "react-native-webview";
import PropTypes from "prop-types";
import { CachedImage } from "react-native-img-cache";
import Toast from "react-native-easy-toast";
import I18n from "react-native-i18n";

import { SlideUpView } from "../../../components/navigation/animateView";
import config from "../../../../config/config";
import { getDefaultLanguageText, translateText } from "../../../translation/translate";
import HorizontalPicker from "../../../components/common/HorizontalPicker";
import Loader from "../../../components/common/Loader";
import ModalSelector from "../../../components/common/modal/ModalSelector";

const LOCATION_KEYWORDS = {
    FIRST_MESSAGE: "please_follow_path",
    SAME_FLOOR: "same_floor",
    SAME_FLOOR_STORE: "same_floor_block",
    GO_TO_UP: "sp_up_floor",
    GO_TO_DOWN: "sp_down_floor",
    LAST_FLOOR: "sp_end_floor"
};

const UP = 1;
const DOWN = 2;

const styles = StyleSheet.create({
    searchImage: {
        position: "absolute",
        marginTop: 10,
        width: 20,
        height: 20,
        right: 10
    },
    searchTextButton: {
        width: "100%",
        height: 40,
        justifyContent: "center",
        padding: 10,
        borderColor: "#c7c7c7",
        borderBottomWidth: StyleSheet.hairlineWidth,
        marginBottom: 5
    },
    levelSelectorContainer: {
        height: 50,
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#000"
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    webView: {
        backgroundColor: "#fff",
        width: "100%",
        flex: 1
    },
    spAnimationContainer: {
        height: 120,
        bottom: 0,
        position: "absolute",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        shadowColor: "#000",
        backgroundColor: "#d5d5d5",
        shadowOpacity: 0.6,
        shadowRadius: 4,
        elevation: 3,
        padding: 15,
        flexDirection: "row",
    },
    locationWordsText: {
        textAlign: "center",
        marginLeft: 15,
        marginRight: 15,
        alignSelf: "center",
        flex: 1
    },
    imageUpDown: {
        width: 48,
        height: 48,
        resizeMode: "contain",
    }
});

const getMapHtml = () => {
    if (Platform.OS === "android") {
        return { uri: "file:///android_asset/map/map.html" };
    }
    return require('../../../../map/map.html');
};

class MallMapComponent1 extends React.Component {
    onMessage = (event) => {
        const message = JSON.parse(event.nativeEvent.data);
        switch (message.action) {
            case "mapLoaded":
                this.MapLoadAfter();
                break;
            case "locationOpened":
                this.setState({
                    locationOpened: message.data,
                    animationStarted: false
                });
                this.props.resetSP();
                break;
            case "levelSwitched":
                this.setState({
                    selectedFloor: message.data
                });
                break;
            default:
                break;
        }
    };
    webview = null;
    locationNotFound = () => {
        if (this.state.loading) {
            this.setState({
                loading: false
            }, () => {
                setTimeout(() => {
                    Alert.alert(translateText("error"), translateText("location_failed"));
                }, 1000);
            });
        }

        if (this.props.liveBeacon && this.props.liveBeacon.length === 0) {
            this.postMessage({
                action: "resetBeaconLocation",
            });
        }
    };
    visitedBeacon = () => {
        if (this.props.liveBeacon && this.props.liveBeacon.length > 0) {
            if (!this.state.parkingMode) {
                this.props.BeaconVisitedRequest(this.props.liveBeacon[0]);
            }
        }
    };
    MapLoadAfter = () => {
        const { state } = this.props.navigation;
        if (state && state.params && state.params.locationOpen) {

            this.postMessage({
                action: "selectLocation",
                payload: {
                    locationOpen: state.params.locationOpen,
                    locationType: state.params.locationType
                }
            });
        }

        if (this.props.sp && state && state.params && state.params.from && state.params.to) {
            const { sp, navigation } = this.props;

            this.postMessage({
                action: "closeNavigation"
            });
            this.setState({
                locationOpened: null
            });

            this.setState({
                animationStarted: true
            });

            if (this.toast && sp.direction.length > 1) {
                this.toast.show(translateText(LOCATION_KEYWORDS["FIRST_MESSAGE"])
                    .replace("%1", sp.direction[0])
                    .replace("%2", sp.direction[sp.direction.length - 1]), 3000);
            }

            this.postMessage({
                action: "showNavigation",
                response: sp,
                from: navigation.state.params.from,
                to: navigation.state.params.to
            });
        }

        if (this.state.parkingMode) {
            this.props.BeaconParkingRequest(this.props.liveBeacon[0], this.selectParkingLocation.bind(this));
        }
    };
    postMessage = (data) => {
        if (this.webview) {
            this.webview.postMessage(JSON.stringify(data));
        }
    };

    levelSelectorContainer = () => {
        return this.props.levels.map((floor) => {
            const { no } = floor;
            return (
                <HorizontalPicker.Item
                    label={no === 0.5 ? "MZ" : no.toString()}
                    value={no}
                    key={no}
                />
            );
        });
    };
    levelSelect = (no) => {
        this.setState({
            selectedFloor: no
        });
        this.postMessage({
            action: "selectFloor",
            floorNo: no
        });
    };
    _calculateDirection = () => {
        const { direction } = this.props.sp;

        if (direction && direction.length > 0) {
            if (direction[0] > direction[1]) {
                return DOWN;
            }

            return UP;
        }

        return UP;
    };
    _getAnimationMessages = () => {
        const { direction } = this.props.sp;

        if (direction.length === 1) {
            if (this.props.levels.length === 1) {
                const { from, to } = this.props.navigation.state.params;
                const fromBlockName = from.rawData.block_name;
                const toBlockName = to.rawData.block_name;
                return (
                    <Text
                        style={styles.locationWordsText}
                    >
                        {translateText(LOCATION_KEYWORDS["SAME_FLOOR_STORE"])
                            .replace("%1", (fromBlockName && getDefaultLanguageText(fromBlockName) !== "" && getDefaultLanguageText(fromBlockName)) || getDefaultLanguageText(from.title))
                            .replace("%2", (toBlockName && getDefaultLanguageText(toBlockName) !== "" && getDefaultLanguageText(toBlockName)) || getDefaultLanguageText(to.title))
                        }
                    </Text>
                );
            }

            return (
                <Text
                    style={styles.locationWordsText}
                >
                    {translateText(LOCATION_KEYWORDS["SAME_FLOOR"])
                        .replace("%s", this.state.selectedFloor)}
                </Text>
            );
        }
        else if (direction.length > 1 && this._calculateDirection() === UP && direction[direction.length - 1] !== this.state.selectedFloor) {
            return (
                <Text style={styles.locationWordsText}>{translateText(LOCATION_KEYWORDS["GO_TO_UP"])}</Text>
            );
        }
        else if (direction.length > 1 && this._calculateDirection() === DOWN && direction[direction.length - 1] !== this.state.selectedFloor) {
            return (
                <Text style={styles.locationWordsText}>{translateText(LOCATION_KEYWORDS["GO_TO_DOWN"])}</Text>
            );
        }
        else if (direction[direction.length - 1] === this.state.selectedFloor) {
            return (
                <Text
                    style={styles.locationWordsText}
                >
                    {translateText(LOCATION_KEYWORDS["LAST_FLOOR"])
                        .replace("%s", this.state.selectedFloor)}
                </Text>
            );
        }

        return null;
    };
    beforeButton = () => {
        const { direction } = this.props.sp;
        const index = this.findIndex(direction, this.state.selectedFloor);
        if (index !== undefined) {
            return direction[index - 1];
        }

        return undefined;

    };
    findIndex = (direction, object) => {
        const index = direction.indexOf(object);
        if (index === -1) {
            return undefined;
        }
        return index;
    };
    nextButton = () => {
        const { direction } = this.props.sp;
        const index = this.findIndex(direction, this.state.selectedFloor);
        if (index !== undefined) {
            return direction[index + 1];
        }

        return undefined;

    };
    spAnimationContainer = () => {
        // console.log(this.props.sp);
        if (!this.props.sp) {
            return null;
        }

        const { direction } = this.props.sp;

        if (!this.state.locationOpened !== null && this.state.animationStarted && this.findIndex(direction, this.state.selectedFloor) !== undefined) {
            return (
                <View style={styles.spAnimationContainer}>
                    <View style={{ width: 50 }}>
                        {this.beforeButton() !== undefined ? (
                            <TouchableOpacity onPress={() => {
                                this.levelSelect(this.beforeButton());
                            }}
                            >
                                {
                                    this._calculateDirection() === DOWN ? (
                                        <CachedImage
                                            style={styles.imageUpDown}
                                            source={require("../../../../assets/icons/floor_up.png")}
                                        />
                                    ) : (
                                            <CachedImage
                                                style={styles.imageUpDown}
                                                source={require("../../../../assets/icons/floor_down.png")}
                                            />)
                                }
                            </TouchableOpacity>
                        ) : null}
                    </View>

                    {this._getAnimationMessages()}

                    <View style={{ width: 50 }}>
                        {this.nextButton() !== undefined ? (
                            <TouchableOpacity onPress={() => {
                                this.levelSelect(this.nextButton());
                            }}
                            >
                                {this._calculateDirection() === DOWN ? (
                                    <CachedImage
                                        style={styles.imageUpDown}
                                        source={require("../../../../assets/icons/floor_down_red.png")}
                                    />
                                ) : (
                                        <CachedImage
                                            style={styles.imageUpDown}
                                            source={require("../../../../assets/icons/floor_up_red.png")}
                                        />
                                    )}

                            </TouchableOpacity>
                        ) : null}
                    </View>
                </View>
            );
        }
        return null;
    };
    selectParkingLocation = (parkingSpots) => {
        const spots = parkingSpots.map((parkingSpot, index) => {
            return {
                key: index,
                label: parkingSpot.b,
                parkingSpot
            };
        });

        this.setState({
            parkingSpots: spots,
            loading: false
        });

        setTimeout(() => {
            if (this.parkingDialog) {
                this.parkingDialog.open();
            }
            this.postMessage({
                action: "showBeaconLocation",
            });
        }, 1000);
    };
    isShowUserLocationMap = () => {
        return config().beaconMode && !this.state.animationStarted && this.props.bluetooth;
    };

    constructor(props, context) {
        super(props, context);
        this.beforeButton = this.beforeButton.bind(this);
        this.isShowUserLocationMap = this.isShowUserLocationMap.bind(this);
        this.visitedBeacon = this.visitedBeacon.bind(this);
        this.locationNotFound = this.locationNotFound.bind(this);
        this.state = {
            locationOpened: null,
            selectedFloor: 1,
            url: { uri: "" },
            animationStarted: false,
            loading: false,
            parkingMode: false,
        };
    }

    UNSAFE_componentWillMount() {
        const { beaconMode, MapHtmlFile } = config();

        if (beaconMode && !this.state.animationStarted) {
            this.userLocationWithBeacon = setInterval(this.visitedBeacon, 5000);
            this.userLocationNotFoundALongTime = setInterval(this.locationNotFound, 15000);
        }

        if (this.props.navigation.state && this.props.navigation.state.params) {
            const { parkingMode } = this.props.navigation.state.params;

            if (parkingMode) {
                this.setState({
                    parkingMode: true,
                    loading: true
                });
            }
        }
        console.log({ MapHtmlFile })
        this.setState({
            url: MapHtmlFile
        });

    }

    UNSAFE_componentWillReceiveProps(nextProps) {

        if (nextProps.visitedBeacons) {
            this.postMessage({
                action: "visitedBeacon",
                payload: nextProps.visitedBeacons
            });

            if (this.props.bluetooth && (this.state.loading || this.state.parkingMode)) {
                this.setState({
                    loading: false,
                });

                this.postMessage({
                    action: "showBeaconLocation",
                });
            }

            this.props.VisitedLocationShowed();
        }

        if (nextProps.sp) {
            this.MapLoadAfter();
        }
    }

    componentWillUnmount() {
        if (this.userLocationWithBeacon) {
            clearInterval(this.userLocationWithBeacon);
        }

        if (this.userLocationNotFoundALongTime) {
            clearInterval(this.userLocationNotFoundALongTime);
        }

        if (this.props.sp) {
            this.props.resetSP();
        }
    }

    returnData = (data) => {
        this.postMessage({
            action: "selectLocation",
            payload: {
                locationOpen: data.id,
                locationType: data.type
            }
        });
    };

    _renderShowLocationButton() {
        if (config().beaconMode) {
            return (
                <View
                    style={{
                        height: 48,
                        width: 48,
                        borderRadius: 48,
                        position: "absolute",
                        right: 15,
                        top: 15,
                        opacity: this.isShowUserLocationMap() ? 1 : 0.7
                    }}
                >
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{
                            height: 48,
                            width: 48,
                            borderRadius: 48
                        }}
                        onPress={async () => {
                            if (this.isShowUserLocationMap()) {
                                if (this.props.liveBeacon && this.props.liveBeacon.length > 0 && this.props.bluetooth) {
                                    this.postMessage({
                                        action: "showBeaconLocation",
                                    });

                                    if (this.state.parkingMode) {
                                        this.props.BeaconParkingRequest(this.props.liveBeacon[0], this.selectParkingLocation.bind(this));
                                    }

                                }
                                else {
                                    this.props.ActionBluetoothPermission(true, (data) => {
                                        if (data.error) {
                                            if (data.message) {
                                                Alert.alert(translateText("error"), data.message);
                                            }
                                        }
                                        else {
                                            this.setState({
                                                loading: true,
                                            });
                                        }
                                    });
                                }
                            }
                            else if (!this.props.bluetooth) {
                                this.props.ActionBluetoothPermission(true, (data) => {
                                    if (data.error) {
                                        if (data.message) {
                                            Alert.alert(translateText("error"), data.message);
                                        }
                                    }
                                    else {
                                        this.setState({
                                            loading: true,
                                        });
                                    }
                                });
                            }
                        }}
                    >
                        <CachedImage
                            source={require("../../../../assets/icons/map_location.png")}
                            style={{
                                width: 48,
                                height: 48,
                                resizeMode: "contain"
                            }}
                        />
                    </TouchableOpacity>

                    <Loader
                        loading={this.state.loading}
                        title={translateText("detecting_your_location")}
                        onRequestClose={() => {
                            this.setState({ loading: false });
                        }}
                    />
                </View>
            );
        }
        return null;
    }

    render() {
        const { SearchBar } = config().MallMapScreen;
        console.log(this.props.levels.length)
        return (
            <View style={styles.container}>
                {this.props.levels.length > 1 && (
                    <HorizontalPicker
                        style={{
                            borderBottomWidth: 1,
                            borderColor: "#444"
                        }}
                        itemWidth={50}
                        itemHeight={70}
                        selectedItemColor={config().BASE_SECOND_COLOR}
                        selectedValue={this.state.selectedFloor}
                        foregroundColor="#000"
                        onChange={(pickerValue) => {
                            this.levelSelect(pickerValue);
                        }}
                    >
                        {this.levelSelectorContainer()}
                    </HorizontalPicker>
                )}
                {!!SearchBar && (
                    <TouchableOpacity
                        style={styles.searchTextButton}
                        onPress={() => {
                            this.props.navigation.navigate("WayfinderStoreList", { returnData: this.returnData });
                        }}
                    >
                        <Text>{(this.state.locationOpened && this.state.locationOpened.title) || translateText("Search Store")}</Text>
                        <CachedImage
                            source={require("../../../../assets/icons/search_icon.png")}
                            style={styles.searchImage}
                        />
                    </TouchableOpacity>
                )}
                {this.state.parkingMode && <ModalSelector
                    justDialog
                    ref={(ref) => {
                        this.parkingDialog = ref;
                    }}
                    data={this.state.parkingSpots}
                    cancelText={translateText("Cancel")}
                    onChange={(option) => {
                        this.props.BeaconParkingSpot(option.parkingSpot);
                        this.props.navigation.goBack();
                    }}
                />}

                <View style={{ flex: 1 }}>
                    <WebView
                        ref={(ref) => (this.webview = ref)}
                        style={styles.webView}
                        javaScriptEnabled
                        domStorageEnabled
                        nativeConfig={{ props: { webContentsDebuggingEnabled: true } }}
                        setAllowFileAccessFromFileURLs
                        allowFileAccess
                        setAllowUniversalAccessFromFileURLs
                        useWebKit
                        originWhitelist={["*"]}
                        source={getMapHtml()}
                        onMessage={this.onMessage}
                        hideKeyboardAccessoryView={false}
                        onLoadEnd={() => {
                            const levels = this.props.levels;
                            this.postMessage({
                                action: "init",
                                language: "tr",
                                levels,
                                options: {},
                            });
                        }}
                    />
                    {this._renderShowLocationButton()}
                </View>
                <SlideUpView
                    locationOpened={this.state.locationOpened}
                    directionButtonDismiss={this.state.parkingMode}
                    navigation={this.props.navigation}
                    navigationBarOnChange={() => {
                        this.postMessage({
                            action: "closeNavigation"
                        });

                        this.setState({
                            locationOpened: null
                        });
                    }}
                />
                {this.spAnimationContainer()}
                <Toast
                    style={{ margin: 15 }}
                    position="top"
                    ref={(toast) => {
                        this.toast = toast;
                    }}
                />
            </View>
        );
    }
}

MallMapComponent1.propTypes = {
    levels: PropTypes.instanceOf(Array),
    ActionBluetoothPermission: PropTypes.func,
    liveBeacon: PropTypes.instanceOf(Array),
    resetSP: PropTypes.func,
    BeaconVisitedRequest: PropTypes.func,
    navigation: PropTypes.instanceOf(Object),
    sp: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.bool]),
    visitedBeacons: PropTypes.instanceOf(Object),
    BeaconParkingRequest: PropTypes.func,
    VisitedLocationShowed: PropTypes.func,
    BeaconParkingSpot: PropTypes.func,
    bluetooth: PropTypes.bool,
    categories: PropTypes.instanceOf(Array)
};

MallMapComponent1.defaultProps = {
    levels: [],
    ActionBluetoothPermission: () => {
    },
    liveBeacon: [],
    resetSP: () => {
    },
    BeaconVisitedRequest: () => {
    },
    navigation: {},
    sp: false,
    visitedBeacons: {},
    BeaconParkingRequest: () => {
    },
    VisitedLocationShowed: () => {
    },
    BeaconParkingSpot: () => {
    },
    bluetooth: false,
    categories: []
};

export default MallMapComponent1;
