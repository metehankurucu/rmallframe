/* eslint-disable import/prefer-default-export */
import React from "react";
import {TouchableOpacity} from "react-native";
import {DrawerActions} from "react-navigation";
import {CachedImage} from "react-native-img-cache";
import {connect} from "react-redux";

import config from "../../../config/config";
import {translateText} from "../../translation/translate";
import {NewSP} from "../../actions/Floor";
import {Icon} from "../../components/common";

class WayfinderScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("WAYFINDER")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("WAYFINDER")
                .toUpperCase(),
        };
    };

    render() {
        const {Component} = config().WayfinderScreen;

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({main, beacon}) => {
    return {
        internetConnection: main.connection,
        parkingBeacon: beacon.parking
    };
};

const WayfinderRedux = connect(mapStateToProps, {NewSP})(WayfinderScreen);
export {WayfinderRedux as WayfinderScreen};
