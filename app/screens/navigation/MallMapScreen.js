/* eslint-disable import/extensions,import/no-unresolved */
// eslint-disable-next-line import/extensions
import React from "react";
import { TouchableOpacity } from "react-native";
import { DrawerActions } from "react-navigation";
import { connect } from "react-redux";
import { getDefaultLanguageText, translateText } from "../../translation/translate";
import config from "../../../config/config";
import MallMapComponent1 from "./MallMapComponents/MallMapComponent1";
import { NewSP, resetSP } from "../../actions/Floor";

import { Icon } from "../../components/common";

class MallMapScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { state } = navigation;
        const { DrawerMenuIcon } = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("MallMap")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const { isDrawerOpen } = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({ type: DrawerActions.OPEN_DRAWER });
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon} />
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("MallMap")
                .toUpperCase(),
        };
    };

    render() {
        const { Enabled, Component } = config().MallMapScreen;
        if (!Enabled) {
            return (
                <MallMapComponent1 {...this.props} />
            );
        }

        return (
            <Component {...this.props} />
        );
    }
}

const mapStateToProps = (state) => {
    const { payload } = state.main;
    const { sp } = state.floor;
    const { visited, live } = state.beacon;
    const { enabled } = state.bluetooth;

    if (payload !== null) {
        const data = payload.floors.map((floor) => {
            let levelIdentifier;
            if (floor.no <= -1) {
                levelIdentifier = `KE${floor.no * -1}`;
            }
            else {
                levelIdentifier = `K${floor.no}_`;
            }

            /** @namespace floor.dimension */

            if (!floor.dimension || !floor.dimension.width || !floor.dimension.height) {
                if (!config().floorDimensionWidth || !config().floorDimensionHeight) {
                    throw new Error("You must declare floorDimensionWidth , floorDimensionHeight on config file...");
                }
            }

            const floorData = {
                id: `level-${floor.no}`,
                no: floor.no,
                mapWidth: config().floorDimensionWidth ? config().floorDimensionWidth : floor.dimension.width,
                mapHeight: config().floorDimensionHeight ? config().floorDimensionHeight : floor.dimension.height,
                title: `MechanicMap-${floor.no}`,
                map: floor.svg,
                initialScaleFactor: floor.scale || 1.3,
                locations: payload.stores.reduce((stores, store) => {
                    store.locations.forEach((location) => {
                        if (location.s.substring(0, 3) === levelIdentifier) {
                            stores.push({
                                id: location.s,
                                type: "store",
                                title: getDefaultLanguageText(store.name),
                                image_url: store.logo,
                                rawData: store,
                                category: store.categories.length > 0 ? store.categories[0].s : ""
                            });
                        }
                    });
                    return stores;
                }, [])
                    .concat(
                        payload.services.reduce((services, service) => {
                            service.locations.forEach((location) => {
                                if (location.s.substring(0, 3) === levelIdentifier) {
                                    services.push({
                                        id: location.s,
                                        type: "service",
                                        title: getDefaultLanguageText(service.name),
                                        image_url: service.image,
                                        rawData: service,
                                    });
                                }
                            });
                            return services;
                        }, [])
                    )
            };

            if (floor.no === 0) {
                floorData.show = true;
            }
            return floorData;
        });

        data.sort((a, b) => {
            if (a.no < b.no) {
                return -1;
            }
            if (a.no > b.no) {
                return 1;
            }
        });

        const categories = payload.categories.map((category) => {
            return {
                slug: category._id,
                color: category.color
            };
        });

        return {
            levels: data,
            categories,
            sp,
            visitedBeacons: visited,
            liveBeacon: live,
            bluetooth: enabled
        };
    }

    return {
        levels: null,
        categories: null,
        sp,
        visitedBeacons: visited,
        liveBeacon: live,
        bluetooth: enabled
    };
};

let mapStateToDispatch = {
    NewSP,
    resetSP
};


const MallMapRedux = connect(mapStateToProps, mapStateToDispatch)(MallMapScreen);

export { MallMapRedux as MallMapScreen };

