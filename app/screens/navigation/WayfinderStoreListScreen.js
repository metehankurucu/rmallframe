import React from "react";
import {connect} from "react-redux";
import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import WayfinderStoreListComponent1 from "./WayfinderStoreListComponents/WayfinderStoreListComponent1";

class WayfinderStoreListScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        if (navigation.state && navigation.state.params && navigation.state.params.title) {
            return {
                title: translateText(navigation.state.params.title)
                    .toUpperCase(),
            };
        }

        return {
            title: translateText("search_location")
        };
    };

    render() {
        const {Enabled, Component} = config().WayfinderStoreListScreen;
        if (!Enabled) {
            return (
                <WayfinderStoreListComponent1 {...this.props}/>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}


const mapStateToProps = (state) => {
    return state;
};

const WayfinderStoreListRedux = connect(mapStateToProps)(WayfinderStoreListScreen);
export {WayfinderStoreListRedux as WayfinderStoreListScreen};
