import React from "react";
import {TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import {DrawerActions} from "react-navigation";
import {CachedImage} from "react-native-img-cache";
import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import {Icon} from "../../components/common";

class ContactScreen extends React.Component {
    static defaultProps = {
        mall: {
            address: "",
            social: {
                facebook: "",
                twitter: "",
                instagram: "",
            },
            mail: "",
            phone: "",
        }
    };

    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("CONTACTUS")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("CONTACTUS")
                .toUpperCase(),
        };
    };

    render() {
        const {Component} = config().ContactScreen;

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({main}) => {
    const {payload} = main;
    return {mall: payload.mall};
};

const ContactRedux = connect(mapStateToProps)(ContactScreen);
export {ContactRedux as ContactScreen};
