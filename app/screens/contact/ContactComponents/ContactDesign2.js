import React, {Component} from "react";
import {Alert, Platform, InteractionManager, Text, TouchableOpacity, View} from "react-native";
import Communications from "react-native-communications";
import {CachedImage} from "react-native-img-cache";
import PropTypes from "prop-types";

import {getDefaultLanguageText, translateText} from "../../../translation/translate";
import {HowToGetHereScreen} from "../../HowToGetHereScreen";
import {Button, Icon} from "../../../components/common";
import config from "../../../../config/config";
import {BorderlessFAButton} from "../../../components/buttons";

class ContactDesign2 extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            renderPlaceholderOnly: true
        };
    }

    UNSAFE_componentWillMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({renderPlaceholderOnly: false});
        });
    }

    styles = {
        imageStyle: {width: 120},
        container: {flex: 1},
        image: {
            resizeMode: "cover",
            width: "100%",
            height: 220,
        },
        imageLogo: {
            resizeMode: "contain",
            width: "100%",
            height: 100,
        },
        cardContainer: {
            backgroundColor: "#fff",
            marginRight: 15,
            marginLeft: 15,
            padding: 15,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2
            },
            shadowOpacity: 0.1,
            flex: 1
        },
        logoContainer: {
            flexDirection: "row",
            justifyContent: "space-between",
        },
        titleStyle: {
            color: config().DEFAULT_BUTTON_HIGHLIGHT_COLOR,
        },
        title: {
            fontSize: 17,
            fontWeight: "300",
            marginBottom: 5,
            color: "#000",
        },
        iconStyle: {
            width: 30,
            height: 30,
            resizeMode: "contain"
        },
        LogoContainer: {
            alignSelf: "center",
            flex: 1,
            marginTop: 15,
        },
        button: {
            color: "#000",
            width: "100%",
            textAlign: "center",
            fontSize: 14,
            fontWeight: "300",
            paddingTop: 2,
            paddingBottom: 2,
            paddingLeft: 5,
            paddingRight: 5,
            alignSelf: "center",
        },
        buttonStyle: {
            flex: 1,
            borderRightWidth: 1,
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderColor: "#000",
            alignItems: "center",
            justifyContent: "center",
        },
        buttonGroupContainer: {
            flexDirection: "row",
            marginLeft: 15,
            marginRight: 15,
            marginTop: 20,
            borderLeftWidth: 1,
            borderColor: "#000",
        },
        socialButton: {
            marginRight: 8
        },
        absoluteCard: {
            zIndex: 1,
            position: "absolute",
            left: 16,
            right: 16,
            bottom: 16,
            height: 300,
            backgroundColor: "#fff",
            padding: 15,
            borderRadius: 8,
            shadowColor: "#222",
            shadowOpacity: 0.4,
            shadowOffset: {
                width: 0,
                height: 1
            },
            elevation: 2
        },
        borderLessFAButton: {
            position: "absolute",
            zIndex: 2,
            right: 32,
            bottom: 292,
            backgroundColor: config().BASE_THIRD_COLOR
        }
    };

    render() {
        return (
            <View style={{height: "100%"}}>
                <View style={{height: "100%"}}>
                    {!this.state.renderPlaceholderOnly && <HowToGetHereScreen liteMode/>}
                </View>
                <BorderlessFAButton
                    style={this.styles.borderLessFAButton}
                    onPress={() => {
                        Communications.web(`https://www.google.com/maps/search/?api=1&query=${this.props.mall.latitude},${this.props.mall.longitude}`);
                    }}
                >
                    <CachedImage
                        source={require("../../../../assets/icons/home_mallmap_icon.png")}
                        style={this.styles.iconStyle}
                    />
                </BorderlessFAButton>
                <View style={this.styles.absoluteCard}>
                    <View style={{
                        marginTop: 25,
                        flex: 1
                    }}
                    >
                        <Text style={this.styles.title}>
                            {translateText("Address")
                                .toUpperCase()}
                        </Text>
                        <Text style={this.styles.titleStyle}>
                            {getDefaultLanguageText(this.props.mall.address)}
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "row",
                            marginTop: 25
                        }}
                    >
                        <TouchableOpacity
                            activeOpacity={0.9}
                            onPress={() => {
                                Communications.phonecall(this.props.mall.phone, false);
                            }}
                        >
                            <Text style={this.styles.title}>{translateText("Phone")
                                .toUpperCase()}
                            </Text>
                            <Text style={this.styles.titleStyle}>{this.props.mall.phone}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{marginLeft: 25}}
                            activeOpacity={0.9}
                            onPress={() => {
                                if (Platform.OS === "ios") {
                                    Alert.alert(
                                        translateText("E-Mail"),
                                        this.props.mall.email,
                                        [{
                                            text: "Cancel",
                                            onPress: () => {
                                            }
                                        }, {
                                            text: "OK",
                                            onPress: () => {
                                                Communications.email([this.props.mall.email], null, null, "", "");
                                            }
                                        },
                                        ],
                                        {cancelable: true}
                                    );
                                }
                                else {
                                    Communications.email([this.props.mall.email], null, null, "", "");
                                }
                            }}
                        >
                            <Text style={this.styles.title}>{translateText("E-Mail")
                                .toUpperCase()}
                            </Text>
                            <Text style={this.styles.titleStyle}>{this.props.mall.email}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        marginTop: 15,
                        alignItems: "center",
                        flex: 1,
                        justifyContent: "center"
                    }}
                    >
                        <View style={{
                            flexDirection: "row",
                            marginTop: 15
                        }}
                        >
                            <Button
                                style={this.styles.socialButton}
                                onPress={() => {
                                    Communications.web(this.props.mall.social.facebook);
                                }}
                            >
                                <Icon name="facebook-circle" color="#3b5998" size={32}/>
                            </Button>
                            <Button
                                style={this.styles.socialButton}
                                onPress={() => {
                                    Communications.web(this.props.mall.social.instagram);
                                }}
                            >
                                <Icon name="instagram-circle" color="#e4405f" size={32}/>
                            </Button>
                            <Button
                                style={this.styles.socialButton}
                                onPress={() => {
                                    Communications.web(this.props.mall.social.twitter);
                                }}
                            >
                                <Icon name="twitter-circle" color="#00aced" size={32}/>
                            </Button>
                        </View>
                    </View>
                    <View style={this.styles.LogoContainer}>
                        <CachedImage
                            source={require("../../../../assets/images/designed_by_kns.png")}
                            resizeMode="contain"
                            style={this.styles.imageStyle}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

ContactDesign2.propTypes = {
    mall: PropTypes.instanceOf(Object)
};

ContactDesign2.defaultProps = {
    mall: {
        social: {
            twitter: "",
            facebook: "",
            instagram: "",
        }
    }
};

export default ContactDesign2;
