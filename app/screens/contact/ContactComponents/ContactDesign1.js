import React, {Component} from "react";
import {Alert, Platform, ScrollView, Text, View} from "react-native";
import Communications from "react-native-communications";
import {CachedImage} from "react-native-img-cache";
import PropTypes from "prop-types";

import {getDefaultLanguageText, translateText} from "../../../translation/translate";
import {HowToGetHereScreen} from "../../HowToGetHereScreen";
import {Button} from "../../../components/common";
import config from "../../../../config/config";

const followInstagramSource = require("../../../../assets/icons/follow_instagram.png");
const followFacebookSource = require("../../../../assets/icons/follow_facebook.png");
const followTwitterSource = require("../../../../assets/icons/follow_twitter.png");
const designedByKNSSource = require("../../../../assets/images/designed_by_kns.png");

class ContactDesign1 extends Component {
    styles = {
        imageStyle: {height: 40},
        container: {flex: 1},
        image: {
            resizeMode: "cover",
            width: "100%",
            height: 220,
        },
        imageLogo: {
            resizeMode: "contain",
            width: "100%",
            height: 100,
        },
        cardContainer: {
            backgroundColor: "#fff",
            marginRight: 15,
            marginLeft: 15,
            padding: 15,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2
            },
            shadowOpacity: 0.1,
            flex: 1
        },
        logoContainer: {
            flexDirection: "row",
            justifyContent: "space-between",
        },
        titleStyle: {
            color: "#000"
        },
        title: {
            fontSize: 18,
            fontWeight: "300",
            color: config().DEFAULT_BUTTON_HIGHLIGHT_COLOR,
            marginBottom: 5
        },
        iconStyle: {
            width: 30,
            height: 30,
            resizeMode: "contain"
        },
        LogoContainer: {
            alignSelf: "center"
        },
        button: {
            color: "#000",
            width: "100%",
            textAlign: "center",
            fontSize: 14,
            fontWeight: "300",
            paddingTop: 2,
            paddingBottom: 2,
            paddingLeft: 5,
            paddingRight: 5,
            alignSelf: "center",
        },
        buttonStyle: {
            flex: 1,
            borderRightWidth: 1,
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderColor: "#000",
            alignItems: "center",
            justifyContent: "center",
        },
        buttonGroupContainer: {
            flexDirection: "row",
            marginLeft: 15,
            marginRight: 15,
            marginTop: 20,
            borderLeftWidth: 1,
            borderColor: "#000",
        },
        socialButton: {
            marginRight: 8
        }
    };

    render() {
        return (
            <View style={{height: "100%"}}>
                <ScrollView>
                    <View style={{height: 200}}>
                        <HowToGetHereScreen liteMode/>
                    </View>
                    <View style={{
                        marginLeft: 16,
                        marginRight: 16
                    }}
                    >
                        <View style={{marginTop: 15}}>
                            <Text style={this.styles.title}>{
                                translateText("Address")
                                    .toUpperCase()}
                            </Text>
                            <Text
                                style={this.styles.titleStyle}
                            >{getDefaultLanguageText(this.props.mall.address)}
                            </Text>
                        </View>

                        <View style={{marginTop: 15}}>
                            <Text style={this.styles.title}>{
                                translateText("FollowUs")
                                    .toUpperCase()}
                            </Text>
                            <View style={{
                                flexDirection: "row",
                                marginTop: 5
                            }}
                            >
                                <Button
                                    style={this.styles.socialButton}
                                    onPress={() => {
                                        Communications.web(this.props.mall.social.facebook);
                                    }}
                                >
                                    <CachedImage
                                        source={followFacebookSource}
                                        style={this.styles.iconStyle}
                                    />
                                </Button>
                                <Button
                                    style={this.styles.socialButton}
                                    onPress={() => {
                                        Communications.web(this.props.mall.social.instagram);
                                    }}
                                >
                                    <CachedImage
                                        source={followInstagramSource}
                                        style={this.styles.iconStyle}
                                    />
                                </Button>
                                <Button
                                    style={this.styles.socialButton}
                                    onPress={() => {
                                        Communications.web(this.props.mall.social.twitter);
                                    }}
                                >
                                    <CachedImage
                                        source={followTwitterSource}
                                        style={this.styles.iconStyle}
                                    />
                                </Button>
                            </View>
                        </View>
                    </View>

                    <View style={this.styles.buttonGroupContainer}>
                        <Button
                            style={this.styles.buttonStyle}
                            onPress={() => {
                                Communications.phonecall(this.props.mall.phone, false);
                            }}
                        >
                            <Text style={this.styles.button}>{translateText("Call")}</Text>
                        </Button>
                        <Button
                            style={this.styles.buttonStyle}
                            onPress={() => {
                                if (Platform.OS === "ios") {
                                    Alert.alert(
                                        "Website",
                                        this.props.mall.web,
                                        [{
                                            text: "Cancel",
                                            onPress: () => {
                                            }
                                        }, {
                                            text: "OK",
                                            onPress: () => {
                                                Communications.web(this.props.mall.web);
                                            }
                                        }],
                                        {cancelable: true}
                                    );
                                }
                                else {
                                    Communications.web(this.props.mall.web);
                                }
                            }}
                        >
                            <Text style={this.styles.button}>{translateText("Go to website")}</Text>
                        </Button>
                        <Button
                            style={this.styles.buttonStyle}
                            onPress={() => {
                                if (Platform.OS === "ios") {
                                    Alert.alert(
                                        translateText("E-Mail"),
                                        this.props.mall.email,
                                        [{
                                            text: "Cancel",
                                            onPress: () => {
                                            }
                                        }, {
                                            text: "OK",
                                            onPress: () => {
                                                Communications.email([this.props.mall.email], null, null, "", "");
                                            }
                                        },
                                        ],
                                        {cancelable: true}
                                    );
                                }
                                else {
                                    Communications.email([this.props.mall.email], null, null, "", "");
                                }
                            }}
                        >
                            <Text style={this.styles.button}>{translateText("E-Mail")}</Text>
                        </Button>
                        <Button
                            style={this.styles.buttonStyle}
                            onPress={() => {
                                Communications.web(`https://www.google.com/maps/search/?api=1&query=${this.props.mall.latitude},${this.props.mall.longitude}`);
                            }}
                        >
                            <Text style={this.styles.button}>{translateText("GetDirections")}</Text>
                        </Button>
                    </View>
                </ScrollView>

                <View style={this.styles.LogoContainer}>
                    <CachedImage
                        source={designedByKNSSource}
                        resizeMode="contain"
                        style={this.styles.imageStyle}
                    />
                </View>

            </View>
        );
    }
}

ContactDesign1.propTypes = {
    "mall": PropTypes.instanceOf(Object)
};

ContactDesign1.defaultProps = {
    "mall": {}
};

export default ContactDesign1;
