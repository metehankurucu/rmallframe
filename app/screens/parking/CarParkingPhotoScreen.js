import React from 'react';
import {Image, PixelRatio, StyleSheet, View} from 'react-native';
import {connect} from "react-redux";
import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import ImagePicker from "react-native-image-picker";
import {Button} from "../../components/common";
import AsyncStorage from "../../utils/AsyncStorage";

const options = {
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
        skipBackup: true
    }
};

class CarParkingPhotoScreen extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText('Take Photo/View').toUpperCase(),
        };
    };

    styles = StyleSheet.create({
        container: {
            flex: 1,
            alignItems: 'center',
            backgroundColor: '#F5FCFF'
        },
        buttonContainer: {
            borderColor: '#9B9B9B',
            borderTopWidth: 1 / PixelRatio.get(),
            justifyContent: 'space-between',
            backgroundColor: config().BASE_COLOR,
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: 15,
            paddingRight: 15,
            width: '100%',
            height: 50
        },
        iconStyle: {
            resizeMode: 'contain',
            width: 24,
            height: 24
        },
        avatar: {
            flex: 1,
            resizeMode: 'contain',
        },
    });

    state = {
        avatarSource: null,
    };

    async UNSAFE_componentWillMount() {
        const file = await AsyncStorage.getItem("parking-image");

        if (file)
            this.setState({
                avatarSource: JSON.parse(file)
            });
    }

    selectFromCamera = () => {
        ImagePicker.launchCamera(options, (response) => {
            // Same code as in above section!

            this.selectPhotoTapped(response);
        });
    };

    selectFromLibrary = () => {
        ImagePicker.launchImageLibrary(options, (response) => {
            // Same code as in above section!
            this.selectPhotoTapped(response);
        });
    };


    selectPhotoTapped(response) {
        if (response.didCancel) {
        }
        else if (response.error) {
        }
        else if (response.customButton) {
        }
        else {
            let source = {uri: response.uri};

            AsyncStorage.setItem("parking-image", JSON.stringify(source));
            this.setState({
                avatarSource: source
            });
        }
    }

    getImage() {
        if (this.state.avatarSource) {
            return (
                <Image style={[this.styles.avatar, {width: "100%"}]}
                       source={this.state.avatarSource}/>
            )
        }
        return (
            <Image style={[this.styles.avatar]}
                   source={require('../../../assets/images/parking_photo.png')}/>
        )
    }

    render() {
        const {Enabled, Component} = config().CarParkingPhotoScreen;

        if (!Enabled) {
            return (
                <View style={this.styles.container}>
                    {this.getImage()}
                    <View style={this.styles.buttonContainer}>
                        <Button onPress={() => {
                            this.selectFromLibrary();
                        }}>
                            <Image style={this.styles.iconStyle}
                                   source={require('../../../assets/icons/gallery.png')}/>
                        </Button>
                        <Button onPress={() => {
                            this.selectFromCamera();
                        }}>
                            <Image style={this.styles.iconStyle}
                                   source={require('../../../assets/icons/photo-camera.png')}/>
                        </Button>
                    </View>
                </View>
            );
        }


        return (
            <Component {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return state
};

const ImagePickRedux = connect(mapStateToProps)(CarParkingPhotoScreen);
export {ImagePickRedux as CarParkingPhotoScreen};