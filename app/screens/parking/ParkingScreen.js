import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {connect} from "react-redux";
import {DrawerActions} from "react-navigation";
import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import {Button, Icon} from "../../components/common";

const styles = {
    button: {
        alignItems: "center",
        justifyContent: "center",
        margin: 10
    },
    image: {
        alignSelf: "center",
        marginBottom: 15,
        height: 80,
        width: 80,
        resizeMode: "contain"
    },
    titleStyle: {
        fontSize: 14,
        color: "#000",
        alignSelf: "center"
    }
};

class ParkingScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("Parking").toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("Parking").toUpperCase(),
        };
    };

    render() {
        const {Enabled, Component} = config().ParkingScreen;

        if (!Enabled) {
            return (
                <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                    <Button
                        style={styles.button}
                        onPress={() => {
                            this.props.navigation.navigate("PictureView");
                        }}
                    >
                        <CachedImage
                            source={require("../../../assets/images/parking_photo.png")}
                            style={styles.image}
                        />
                        <Text style={styles.titleStyle}>{translateText("Take Photo/View")}</Text>
                    </Button>
                    <Button
                        style={styles.button}
                        onPress={() => {
                            this.props.navigation.navigate("VoiceRecord");
                        }}
                    >
                        <CachedImage
                            source={require("../../../assets/images/parking_record.png")}
                            style={styles.image}
                        />
                        <Text style={styles.titleStyle}>{translateText("Record/Listen")}</Text>
                    </Button>
                </View>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

const ParkingRedux = connect(mapStateToProps)(ParkingScreen);
export {ParkingRedux as ParkingScreen};
