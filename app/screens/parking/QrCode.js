import React from 'react';
import {StyleSheet,View} from 'react-native';
import {connect} from "react-redux";
import {translateText} from "react-native-mall-frame-client/app/translation/translate";

class QrCode extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText('QrCode').toUpperCase(),
        };
    };

    render() {
        return (
            <View style={styles.container}>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    }
});

const mapStateToProps = (state) => {
    return state
};

const QrCodeRedux = connect(mapStateToProps)(QrCode);
export {QrCodeRedux as QrCode};