import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, Text, View} from "react-native";
import config from "../../../config/config";
import {translateText} from "react-native-mall-frame-client/app/translation/translate";

class LocateWithBluetooth extends Component {
    static navigationOptions = () => {
        return {
            title: translateText('LocateWithBluetooth').toUpperCase(),
        };
    };

    render() {
        return (
            <View style={styles.container}>

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    }
});

function mapStateToProps({beacon}) {
    return {parkingBeacon: beacon.parking};
}

const LocateWithBluetoothRedux = connect(
    mapStateToProps,
)(LocateWithBluetooth);

export {LocateWithBluetoothRedux as LocateWithBluetooth};