import React from 'react';
import {StyleSheet, Text, View,} from 'react-native';
import {connect} from "react-redux";
import {translateText} from "react-native-mall-frame-client/app/translation/translate";

class QrCodeHelpPage extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText('qr_btn_how').toUpperCase(),
        };
    };

    state = {
        avatarSource: null,
    };

    render() {
        return (
            <View style={styles.container}>

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    }
});

const mapStateToProps = ({main, i18n}) => {
    return {currentLocale: i18n.locale}
};

const QrCodeHelpPageRedux = connect(mapStateToProps)(QrCodeHelpPage);
export {QrCodeHelpPageRedux as QrCodeHelpPage};