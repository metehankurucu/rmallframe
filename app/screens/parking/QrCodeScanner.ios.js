'use strict';

import React, {Component} from 'react';
import {View,} from 'react-native';
import {connect} from "react-redux";

class QrCodeScanner extends Component {

    render() {
        return (
            <View>

            </View>
        );
    }
}

const mapStateToParams = ({main}) => {

    return {parking_spots: main.payload.parking_spots}
};

const QrCodeScannerRedux = connect(mapStateToParams)(QrCodeScanner);
export {QrCodeScannerRedux as QrCodeScanner};