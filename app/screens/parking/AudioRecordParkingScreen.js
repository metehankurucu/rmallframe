/* eslint-disable global-require,import/prefer-default-export */
import React from "react";
import { PermissionsAndroid, Platform, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { Player, Recorder } from "@react-native-community/audio-toolkit";

import { translateText } from "../../translation/translate";
import config from "../../../config/config";

class AudioRecordParkingScreen extends React.Component {
    static navigationOptions = () => {
        return {
            title: translateText("Record/Listen")
                .toUpperCase(),
        };
    };

    constructor() {
        super();
        this.state = {
            recordButton: "Preparing...",
        };
    }

    UNSAFE_componentWillMount() {
        this.player = null;
        this.recorder = null;

        this._reloadPlayer();
        this._reloadRecorder();

    }

    styles = StyleSheet.create({
        button: {
            padding: 10,
            backgroundColor: config().BASE_COLOR,
            width: "100%",
            alignItems: "center",
            justifyContent: "center"
        },
        buttonContainer: {
            marginTop: 20,
        },
        wave: {
            alignSelf: "center",
        }
    });

    filename = "recorded.mp4";

    _updateState() {
        this.setState({
            playPauseButton: this.player && this.player.isPlaying ? translateText("Pause") : translateText("Play"),
            recordButton: this.recorder && this.recorder.isRecording ? translateText("Stop") : translateText("Record"),
            startAnimation: (this.player && this.player.isPlaying) || (this.recorder && this.recorder.isRecording),
            stopAnimation: (this.player && !this.player.isPlaying) || (this.recorder && !!this.recorder.isRecording),
        });
    }

    _playPause() {
        this.player.playPause((err, playing) => {
            console.log("err", err);

            if (playing) {
                this._reloadPlayer();
            }
            this._updateState();
        });
    }

    _reloadPlayer() {
        if (this.player) {
            this.player.destroy();
        }

        this.player = new Player(this.filename, {
            autoDestroy: false
        }).prepare(() => {

            this._updateState();
        });

        this._updateState();

        this.player.on("ended", () => {
            this._updateState();
        });
        this.player.on("pause", () => {
            this._updateState();
        });
    }

    _reloadRecorder() {
        if (this.recorder) {
            this.recorder.destroy();
        }

        this.recorder = new Recorder(this.filename);

        this._updateState();
    }

    _toggleRecord() {
        if (this.player) {
            this.player.destroy();
        }

        this.recorder.toggleRecord((err, stopped) => {
            console.log("err", err);
            if (stopped) {
                this._reloadPlayer();
                this._reloadRecorder();
            }

            this._updateState();
        });
    }

    requestCameraPermission = () => {
        try {
            PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
            ])
                .then((data) => {
                    if (data["android.permission.WRITE_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED && data["android.permission.RECORD_AUDIO"] === PermissionsAndroid.RESULTS.GRANTED) {

                        this._toggleRecord();
                    }
                });
        }
        catch (err) {
            console.warn(err);
        }
    };

    render() {
        const { Enabled, Component } = config().AudioRecordParkingScreen;

        if (!Enabled) {
            return (
                <View style={{
                    flex: 1,
                    backgroundColor: "#fff",
                    alignItems: "center",
                    justifyContent: "center"
                }}
                >
                    <View style={{ width: "70%" }}>
                        <View style={this.styles.wave}>
                        </View>
                        <View style={this.styles.buttonContainer}>
                            <TouchableOpacity
                                style={this.styles.button}
                                onPress={() => {
                                    return this._playPause();
                                }}
                            >
                                <Text style={{
                                    color: "#fff",
                                    fontSize: 20
                                }}
                                >{this.state.playPauseButton}
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <View style={this.styles.buttonContainer}>
                            <TouchableOpacity
                                style={this.styles.button}
                                onPress={() => {
                                    if (Platform.OS === "android") {
                                        this.requestCameraPermission();
                                    }
                                    else {
                                        this._toggleRecord();
                                    }
                                }}
                            >
                                <Text style={{
                                    color: "#fff",
                                    fontSize: 20
                                }}
                                >{this.state.recordButton}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        }

        return (
            <Component />
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

const AudioRecordRedux = connect(mapStateToProps)(AudioRecordParkingScreen);
export { AudioRecordRedux as AudioRecordParkingScreen };
