'use strict';

import React, {Component} from 'react';
import {View} from "react-native";
import {connect} from "react-redux";
import _ from 'lodash';

class QrCodeScanner extends Component {
    onSuccess = (e) => {
        const parking_spot = _.find(this.props.parking_spots, {'n': e.data});



        const {qrCodeCallback} = this.props.navigation.state.params;
        qrCodeCallback(parking_spot);
        this.props.navigation.goBack();
    };

    render() {
        return (
            <View>

            </View>
        );
    }
}

const mapStateToParams = ({main}) => {

    return {parking_spots: main.payload.parking_spots}
};

const QrCodeScannerRedux = connect(mapStateToParams)(QrCodeScanner);
export {QrCodeScannerRedux as QrCodeScanner};