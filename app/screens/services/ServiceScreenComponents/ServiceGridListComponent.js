import React, {Component} from "react";
import {View} from "react-native";
import Toast, {DURATION} from "react-native-easy-toast";
import ServiceGridListItem from "../../../components/services/ServiceGridListItem";
import config from "../../../../config/config";
import {GridView} from "../../../components/common";
import ModalSelector from "../../../components/common/modal/ModalSelector";
import {translateText} from "../../../translation/translate";

class ServiceGridListComponent extends Component {
    defaultProps = {
        services: []
    };

    renderItem = (item) => {
        const {DetailEnabled} = config().ServiceScreen;

        return (
            <ServiceGridListItem
                DetailEnabled={DetailEnabled}
                data={item}
                onPress={() => {
                    if (DetailEnabled) {
                        this.props.navigation.navigate("ServiceDetail", {
                            title: item.name,
                            data: item
                        });
                    }
                    else if (item.locations.length === 0) {
                        if (this.toast) {
                            this.toast.show(translateText("location_not_found"), DURATION.LENGTH_LONG);
                        }
                    }
                    else if (this.props.navigation.state.params && this.props.navigation.state.params.returnData) {
                        this.props.navigation.state.params.returnData({
                            id: item.locations[0].s,
                            type: "service",
                            title: item.name,
                            image_url: item.image,
                            rawData: item,
                        });

                        this.props.navigation.goBack();
                    }
                    else {
                        this.props.navigation.navigate("Wayfinder", {
                            to: {
                                id: item.locations[0].s,
                                type: "service",
                                title: item.name,
                                image_url: item.image,
                                rawData: item
                            }
                        });
                    }
                }}
            />
        );
    };

    render() {
        return (
            <View>
                <GridView
                    items={this.props.services}
                    itemsPerRow={2}
                    renderItem={this.renderItem}
                />
                <ModalSelector
					ref={(modal) => {
						this.modal = modal
					}}
					cancelText={translateText("Cancel")}
					data={this.state.locations}
					onChange={(option) => {
						let data = this.state.item;
						data["id"] = option.key;
						this.props.navigation.state.params.returnData(data);
						this.props.navigation.goBack();
					}}
				/>
            </View>
        );
    }
}

ServiceGridListComponent.propTypes = {};

export default ServiceGridListComponent;
