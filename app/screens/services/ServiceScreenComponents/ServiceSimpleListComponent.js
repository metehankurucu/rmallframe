import {Text, TouchableOpacity, View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {getDefaultLanguageText, translateText} from "../../../translation/translate";
import config from "../../../../config/config";
import OptimizedFlatList from "../../../components/common/OptimizedFlatList";
import ModalSelector from "../../../components/common/modal/ModalSelector";
import PropTypes from "prop-types";
import React from "react";

class ServiceSimpleListComponent extends React.Component {
	styles = {
		itemButton: {
			marginRight: 16,
			marginLeft: 16,
			paddingTop: 16,
			paddingBottom: 16,
			borderBottomWidth: 1,
			borderColor: "#eee",
			flexDirection: "row",
			justifyContent: "space-between",
			alignItems: "center"
		},
		likeButtonContainer: {
			position: "absolute",
			right: 0,
			zIndex: 1,
			margin: 15
		},
		image: {
			resizeMode: "contain",
			width: "100%",
			height: "100%"
		},
		button: {
			color: config().DEFAULT_BUTTON_COLOR,
			fontSize: 15,
			alignSelf: "center",
			fontWeight: "300",
		},
		buttonStyle: {
			borderWidth: 1,
			flex: 1,
			padding: 10,
		},
		buttonGroup: {
			flexDirection: "row",
			position: "absolute",
			bottom: 0,
			width: "100%"
		},
		title: {
			fontSize: 21,
			fontWeight: "300",
		},
		p: {
			fontSize: 17,
			margin: 16,
			marginBottom: 0,
			fontWeight: "300",
			alignSelf: "center"
		},
		cardContainer: {
			height: "100%",
			backgroundColor: "#fff"
		},
		container: {
			height: "100%",
			backgroundColor: "#fff"
		},
		itemText: {
			fontSize: 18,
			marginLeft: 15
		},
		rightArrowIcon: {
			resizeMode: "contain",
			width: 20,
			height: 20,
		}
	};
	
	state = {
		locations: null,
		item: null,
	};
	
	UNSAFE_componentWillMount() {
		let services = this.props.services;
		if (this.props.wayfinderList) {
			services = services.filter((item) => {
				return item.hint ? !item.hint.includes("category@") : true
			})
		}
		
		this.setState({services});
	}
	
	_itemButton = (item) => {
		const {DetailEnabled} = config().ServiceScreen;
		
		if (DetailEnabled) {
			this.props.navigation.navigate("ServiceDetail", {
				title: item.name,
				data: item
			});
		}
		else {
			if (item.locations.length === 0) {
				if (this.toast) {
					// this.toast.show("Location not found.", DURATION.LENGTH_LONG)
				}
			}
			else {
				this.props.navigation.navigate("Wayfinder", {
					to: {
						id: item.locations[0].s,
						type: "service",
						title: item.name,
						image_url: item.image,
						rawData: item
					}
				})
			}
		}
	};
	
	_wayfinderItem = (service) => {
		const item = {
			id: service["_id"],
			type: "service",
			title: service.name,
			image_url: service.image,
			floor_names: service.floor_names,
			rawData: service
		};
		
		const locations = item.rawData.locations.map((location) => {
			let floor = location.s.substring(0, location.s.indexOf("_"))
			let extra = floor.length === 3 ? "-" : "";
			
			return {
				key: location.s,
				label: `${translateText("Floor") } ${ extra }${floor[floor.length - 1]}`
			}
		});
		
		if (locations.length > 1) {
			this.setState({
				locations,
				item
			}, () => {
				this.modal.open();
			});
		}
		else if (locations.length === 1) {
			item["id"] = item.rawData.locations[0].s;
			this.props.navigation.state.params.returnData(item);
			this.props.navigation.goBack();
		}
	};
	
	renderItem = ({item}) => {
		if (item) {
			return (
				<TouchableOpacity style={this.styles.itemButton}
				                  onPress={() => {
					                  this.props.wayfinderList ? this._wayfinderItem(item) : this._itemButton(item)
				                  }}>
					<View style={{flexDirection: "row", alignItems: "center", width: "70%"}}>
						<CachedImage
							source={{uri: item.image}} style={{resizeMode: "contain", width: 40, height: 40}}
						/>
						<Text numberOfLines={1} style={this.styles.itemText}>{getDefaultLanguageText(item.name)}</Text>
					</View>
					<CachedImage style={this.styles.rightArrowIcon}
					             source={require("../../../../assets/icons/ic_right_arrow.png")}/>
				</TouchableOpacity>
			)
		}
	};
	
	render() {
		return (
			<View style={{height: "100%"}}>
				<OptimizedFlatList
					style={{height: "100%"}}
					data={this.state.services}
					renderItem={this.renderItem}
					keyExtractor={(item, index) => {
						return index.toString()
					}}
				/>
				<ModalSelector
					ref={(modal) => {
						this.modal = modal
					}}
					cancelText={translateText("Cancel")}
					data={this.state.locations}
					onChange={(option) => {
						let data = this.state.item;
						data["id"] = option.key;
						this.props.navigation.state.params.returnData(data);
						this.props.navigation.goBack();
					}}
				/>
			</View>
		);
	}
}

ServiceSimpleListComponent.propTypes = {
	navigation: PropTypes.object,
	wayfinderList: PropTypes.bool,
	services: PropTypes.array
};

export default ServiceSimpleListComponent;
