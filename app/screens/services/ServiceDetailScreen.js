import React from "react";
import {ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import {getDefaultLanguageText, translateText} from "../../translation/translate";
import {Button} from "../../components/common";
import {CachedImage} from "react-native-img-cache";
import AutoHeightWebView from "../../components/common/AutoHeightWebView";
import {Normalize} from "../../../config/helpers";
import config from "../../../config/config";
import PropTypes from "prop-types";

class ServiceDetailScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;

        return {
            title: `${getDefaultLanguageText(state.params.title)}`.toUpperCase(),
        };
    };

    styles = {
        likeButtonContainer: {
            position: "absolute",
            right: 0,
            zIndex: 1,
            margin: 15
        },
        image: {
            resizeMode: "contain",
            width: "100%",
            height: "100%"
        },
        button: {
            color: config().DEFAULT_BUTTON_COLOR,
            fontSize: 15,
            alignSelf: "center",
            fontWeight: "300",
        },
        buttonStyle: {
            borderWidth: 1,
            flex: 1,
            padding: 10,
        },
        buttonGroup: {
            flexDirection: "row",
            position: "absolute",
            bottom: 0,
            width: "100%"
        },
        title: {
            fontSize: 21,
            fontWeight: "300",
        },
        p: {
            fontSize: 17,
            margin: 16,
            marginBottom: 0,
            fontWeight: "300",
            alignSelf: "center"
        },
        cardContainer: {
            height: "100%",
            backgroundColor: "#fff"
        },
        container: {
            height: "100%",
            backgroundColor: "#fff"
        }
    };

    render() {
        const {Enabled, Component} = config().ServiceDetailScreen;

        if (!Enabled) {
            const {data} = this.props.navigation.state.params;

            return (
                <View style={this.styles.container}>
                    <ScrollView style={this.styles.container}>
                        <View style={{
                            width: "100%",
                            height: Normalize(200),
                            alignSelf: "center",
                        }}>
                            <CachedImage source={{uri: data.image}} style={this.styles.image}/>
                        </View>
                        <View style={this.styles.cardContainer}>
                            <View style={{
                                borderTopWidth: 1,
                                paddingTop: 15,
                                alignItems: "center",
                            }}>
                                <Text style={this.styles.title}>{getDefaultLanguageText(data.name)}</Text>
                            </View>

                            <AutoHeightWebView
                                navigation={this.props.navigation}
                                defaultUrl={getDefaultLanguageText(data.explanation)}
                                autoHeight={true}
                            />
                        </View>

                    </ScrollView>
                    {data.locations.length !== 0 ? <View style={this.styles.buttonGroup}>
                        <Button style={this.styles.buttonStyle} onPress={() => {
                            this.props.navigation.navigate("MallMap", {
                                locationOpen: data.locations[0].s,
                                locationType: "service"
                            })
                        }}>
                            <Text style={this.styles.button}>{translateText("ShowOnMap")}</Text>
                        </Button>
                        <Button style={this.styles.buttonStyle} onPress={() => {
                            this.props.navigation.navigate("Wayfinder", {
                                to: {
                                    id: data.locations[0].s,
                                    type: "service",
                                    title: data.name,
                                    image_url: data.image,
                                    rawData: data
                                }
                            })
                        }}>
                            <Text style={this.styles.button}>{translateText("GetDirections")}</Text>
                        </Button>
                    </View> : null}
                </View>

            );
        }

        return (
            <Component {...this.props}/>
        )
    }
}

ServiceDetailScreen.propTypes = {
	navigation: PropTypes.object
};

const ServiceDetailRedux = connect()(ServiceDetailScreen);
export {ServiceDetailRedux as ServiceDetailScreen}

