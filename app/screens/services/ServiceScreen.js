import React from "react";
import {TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import {DrawerActions} from "react-navigation";

import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import {Icon} from "../../components/common";

class ServiceScreen extends React.Component {
    static defaultProps = {
        wayfinderList: false
    };

    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("SERVICES").toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("Services").toUpperCase(),
        };
    };

    render() {
        const {Component} = config().ServiceScreen;

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    const {payload} = state.main;
    if (payload !== null) {
        return {
            services: payload.services,
            stores: payload.stores
        };
    }

    return {services: null};
};

const ServicesRedux = connect(mapStateToProps)(ServiceScreen);

export {ServicesRedux as ServiceScreen};
