import React from "react";
import {connect} from "react-redux";
import {TouchableOpacity} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import {DrawerActions} from "react-navigation";
import {Icon} from "../../components/common/";

class MagazineScreen extends React.Component {
	static navigationOptions = ({navigation}) => {
		const {DrawerMenuIcon} = config();
		
		return {
			title: translateText("Magazine").toUpperCase(),
			headerLeft: <TouchableOpacity onPress={() => {
				const {isDrawerOpen} = navigation.state;
				
				if (!isDrawerOpen) {
					navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
				}
			}}>
                <Icon {...DrawerMenuIcon}/>
			</TouchableOpacity>
		};
	};
	
	render() {
		const Magazine = config().MagazineScreen;
		if (Magazine) {
			return (
				<View>
					<Text>override magazine screen.</Text>
				</View>
			);
		}
		
		return (
			<Magazine {...this.props}/>
		)
	}
}

const mapStateToProps = (state) => {
	const {payload} = state.main;
	if (payload !== null) {
		return {
			magazines: payload.magazines
		}
	}
	return {magazines: null};
};

const MagazineRedux = connect(mapStateToProps)(MagazineScreen);

export {MagazineRedux as MagazineScreen};
