import React from 'react';
import {WebView} from 'react-native';
import {connect} from "react-redux";
import config from "../../../config/config";
import {getDefaultLanguageText} from "../../translation/translate";

class MagazineDetailScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        return {
            title: `${getDefaultLanguageText(state.params.title)}`.toUpperCase(),
        };
    };

    render() {
        const MagazineDetail = config().MagazineDetailScreen;

        if (!MagazineDetail)
            return (
                <View>
                    <Text>Override Magazine Detail screen.</Text>
                </View>
            );

        return (
            <MagazineDetail {...this.props}/>
        )
    }
}

const styles = {
    container: {
        flex: 1
    }
};

const mapStateToProps = (state) => {
    return state
};

const MagazineDetailRedux = connect(mapStateToProps)(MagazineDetailScreen);

export {MagazineDetailRedux as MagazineDetailScreen};