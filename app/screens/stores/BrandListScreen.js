import React from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getDefaultLanguageText } from "../../translation/translate";
import config from "../../../config/config";
import OptimizedFlatList from "../../components/common/OptimizedFlatList";
import ListItem from "../../components/stores/ListItem";
import { Indicator } from "../../components/common";
import { favoriteAction } from "../../actions/Favorite";

class BrandListScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { state } = navigation;
        return {
            title: `${getDefaultLanguageText(state.params.title)}`.toUpperCase(),
        };
    };

    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this);
    }

    _renderItem({ item }) {
        if (this.props.type === "cinema_stores") {
            return (
                <ListItem
                    item={item}
                    onPress={() => {
                        this.props.navigation.navigate("Cinema", {
                            cinemas: item.cinemas,
                            title: item.name
                        });
                    }}
                />
            );
        }

        return (
            <ListItem
                item={item}
                onFavPress={() => {
                    this.props.favoriteAction({
                        userid: item["userid"],
                        isFav: item["fav"],
                        item_id: item["_id"],
                        item_type: "favorite_stores"
                    })
                }}
                onPress={() => {
                    this.props.navigation.navigate("StoreDetail", {
                        data: item,
                        title: item.name
                    });
                }}
            />
        );
    }

    render() {
        const { Enabled, Component } = config().BrandListScreen;

        if (!Enabled) {
            let stores = null;
            if (this.props.navigation && this.props.navigation.state && this.props.navigation.state.params) {
                stores = this.props.navigation.state.params.stores;
            }

            if (!stores && this.props.stores) {
                stores = this.props.stores;
            }

            if (stores !== null && stores.length !== 0) {
                this.renderedContent = (<OptimizedFlatList
                    renderItem={this._renderItem}
                    data={stores}
                    keyExtractor={(item, key) => {
                        return key.toString();
                    }}
                />);
            }
            else {
                this.renderedContent = <Indicator size="small" color="#fff" />;
            }

            return (
                <View style={{ height: "100%" }}>
                    {this.renderedContent}
                </View>
            );
        }

        return (
            <Component {...this.props} />
        );
    }
}

BrandListScreen.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    stores: PropTypes.instanceOf(Array),
    type: PropTypes.bool
};

const mapStateToProps = (state) => {
    return {}
}

const StoresListRedux = connect(mapStateToProps, { favoriteAction })(BrandListScreen);

export { StoresListRedux as BrandListScreen };
