/* eslint-disable no-shadow,eqeqeq */
import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";

import {getDefaultLanguageText} from "../../translation/translate";
import config from "../../../config/config";
import {favoriteAction} from "../../actions/Favorite";
import {getActiveState} from "../../../config/helpers";

class BrandDetailScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;

        return {
            title: `${getDefaultLanguageText(state.params.title)}`.toUpperCase(),
        };
    };

    render() {
        const {Component} = config().BrandDetailScreen;

        return (
            <Component {...this.props}/>
        );
    }

}

const mapStateToProps = (state) => {
    const {payload} = state.main;
    let data = null;
    const activeState = getActiveState(state.nav);

    if (activeState.routeName === "StoreDetail" && activeState.params) {
        data = activeState.params.data;
    }

    if (payload !== null && data && data.locations) {
        const filteredFloors = payload.floors.filter((object) => {
            const floor = object;

            floor.locations.map((location) => {
                const landmark = _.find(data.locations, {s: location});

                if (landmark) {
                    floor.landmark = landmark.s;
                }
                return location;
            });

            return !!floor.landmark;
        });

        const mixedFloors = filteredFloors.map((floor, index) => {
            let levelIdentifier;

            if (floor.no <= -1) {
                levelIdentifier = `KE${floor.no * -1}`;
            }
            else {
                levelIdentifier = `K${floor.no}_`;
            }

            const floorData = {
                id: `level-${floor.no}`,
                no: floor.no,
                landmark: floor.landmark,
                mapWidth: floor.dimension.width,
                mapHeight: floor.dimension.height,
                title: `MechanicMap-${floor.no}`,
                index,
                map: floor.svg,
                initialScaleFactor: 1.0,
                locations: payload.stores.reduce((stores, store) => {
                    store.locations.forEach((location) => {

                        if (location.s.substring(0, 3) === levelIdentifier) {
                            stores.push({
                                id: location.s,
                                type: "store",
                                title: store.name,
                                image_url: store.logo,
                                rawData: store,
                                zoom: 3,
                                action: "zoom",
                                explanation: store.explanation
                            });
                        }
                    });
                    return stores;
                }, [])
            };
            if (floor.no === 0) {
                floorData.show = true;
            }

            return floorData;
        });

        mixedFloors.sort((a, b) => {
            if (a.no < b.no) {
                return -1;
            }
            
            return 1;
        });

        return {
            state,
            levels: mixedFloors
        };
    }

    return state;
};

BrandDetailScreen.propTypes = {
    categories: PropTypes.instanceOf(Array),
    searchTerm: PropTypes.string,
    favoriteAction: PropTypes.func,
    navigation: PropTypes.instanceOf(Object)
};

const StoreDetailRedux = connect(mapStateToProps, {favoriteAction})(BrandDetailScreen);

export {StoreDetailRedux as BrandDetailScreen};
