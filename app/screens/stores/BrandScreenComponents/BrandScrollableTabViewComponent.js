import React, {Component} from "react";
import {InteractionManager, StyleSheet, View} from "react-native";
import ScrollableTabView from "react-native-scrollable-tab-view";
import PropTypes from "prop-types";

import {Indicator, ScrollableTabBar, SearchView} from "../../../components/common";
import CategoryListScreen from "../CategoryListScreen";
import {translateText} from "../../../translation/translate";
import config from "../../../../config/config";
import LevelListScreen from "../LevelListScreen";

const styles = {
    searchInput: {
        padding: 10,
        borderColor: "#999",
        borderBottomWidth: StyleSheet.hairlineWidth,
        fontSize: 16,
        fontWeight: "400",

        textAlign: "center",
        height: 40
    },
    rootContainer: {
        flex: 1,
        backgroundColor: "#fff"
    },
    tabBar: {
        backgroundColor: "transparent",
    },
    underlineStyle: {
        height: 2,
        backgroundColor: "#ffc752"
    }
};

class BrandScrollableTabViewComponent extends Component {
    static defaultProps = {
        navigation: {}
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            renderPlaceholderOnly: true,
            searchTerm: ""
        };
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({renderPlaceholderOnly: false});
        });
    }

    render() {
        const {BrandScreen, AtoZListScreen} = config();
        const {ListTab, CategoryTab, LevelTab} = BrandScreen;

        if (!this.state.renderPlaceholderOnly) {
            return (
                <View style={styles.rootContainer}>
                    <SearchView
                        onChangeText={(searchTerm) => {
                            this.setState({searchTerm});
                        }}
                        value={this.state.searchTerm}
                        style={styles.searchInput}
                        placeholder={translateText("Search")}
                        placeholderTextColor="#999"
                    />
                    <ScrollableTabView
                        scrollWithoutAnimation
                        onChangeTab={() => {
                            this.setState({
                                searchTerm: ""
                            });
                        }}
                        renderTabBar={() => {
                            return (<ScrollableTabBar
                                locked
                                underlineStyle={styles.underlineStyle}
                                tabStyle={styles.tabBar}
                                activeTextColor="#000"
                                inactiveTextColor="#cdcdcd"
                            />);
                        }}
                    >
                        {ListTab && <AtoZListScreen.Component
                            key="atoz"
                            tabLabel={translateText("a_z")}
                            navigation={this.props.navigation}
                            searchTerm={this.state.searchTerm}
                        />}

                        {CategoryTab && <CategoryListScreen
                            key="categories"
                            tabLabel={translateText("Categories")}
                            navigation={this.props.navigation}
                            searchTerm={this.state.searchTerm}
                        />}

                        {LevelTab && <LevelListScreen
                            key="floor"
                            tabLabel={translateText("Floors")}
                            navigation={this.props.navigation}
                            searchTerm={this.state.searchTerm}
                        />}

                    </ScrollableTabView>
                </View>
            );
        }

        return (
            <Indicator/>
        );

    }
}

BrandScrollableTabViewComponent.propTypes = {
    navigation: PropTypes.instanceOf(Object)
};

export default BrandScrollableTabViewComponent;
