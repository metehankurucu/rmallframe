import React, {Component} from "react";
import {StyleSheet, View} from "react-native";
import ScrollableTabView from "react-native-scrollable-tab-view";
import PropTypes from "prop-types";

import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {DefaultTabBar, SearchView} from "../../../components/common";
import CategoryListScreen from "../CategoryListScreen";
import LevelListScreen from "../LevelListScreen";

const styles = StyleSheet.create({
    searchInput: {
        padding: 10,
        borderColor: "#999",
        borderBottomWidth: 1,
        fontWeight: "bold",
        fontSize: 15,
        height: 40
    },
    container: {
        flex: 1,
        marginTop: 15,
    },
    rootContainer: {
        flex: 1
    },
    tabBar: {
        backgroundColor: "transparent",
        borderWidth: StyleSheet.hairlineWidth,
        height: 40,
    },
    indicator: {
        backgroundColor: "transparent"
    },
    labelStyle: {
        fontSize: 15,
    },
    underlineStyle: {
        backgroundColor: "transparent"
    },
    tab: {
        borderWidth: 0,
        margin: 8,
        height: "auto"
    }
});

class BrandTabViewComponent extends Component {
    state = {
        searchTerm: ""
    };

    render() {
        const {BASE_SECOND_COLOR, BrandScreen, AtoZListScreen} = config();
        const {ListTab, CategoryTab, LevelTab} = BrandScreen;

        return (
            <View style={{
                flex: 1,
                backgroundColor: "#fff"
            }}
            >
                <SearchView
                    onChangeText={(searchTerm) => {
                        this.setState({searchTerm});
                    }}
                    value={this.state.searchTerm}
                    style={styles.searchInput}
                    placeholder={translateText("Search")}
                    placeholderTextColor="#000"
                />
                <ScrollableTabView
                    scrollWithoutAnimation
                    onChangeTab={() => {
                        this.setState({
                            searchTerm: ""
                        });
                    }}
                    renderTabBar={() => {
                        return (<DefaultTabBar
                            underlineStyle={styles.underlineStyle}
                            textStyle={styles.labelStyle}
                            style={styles.tab}
                            tabStyle={styles.tabBar}
                            activeTextColor={BASE_SECOND_COLOR}
                            activeBorderColor={BASE_SECOND_COLOR}
                            inactiveBorderColor="#ccc"
                            inactiveTextColor="#ccc"
                        />);
                    }
                    }
                >

                    {ListTab && <AtoZListScreen.Component
                        key="atoz"
                        tabLabel={translateText("a_z")}
                        navigation={this.props.navigation}
                        searchTerm={this.state.searchTerm}
                    />}

                    {CategoryTab && <CategoryListScreen
                        key="categories"
                        tabLabel={translateText("Categories")}
                        navigation={this.props.navigation}
                        searchTerm={this.state.searchTerm}
                    />}

                    {LevelTab && <LevelListScreen
                        key="floor"
                        tabLabel={translateText("Floors")}
                        navigation={this.props.navigation}
                        searchTerm={this.state.searchTerm}
                    />}
                </ScrollableTabView>
            </View>
        );
    }
}


BrandTabViewComponent.propTypes = {
    navigation: PropTypes.instanceOf(Object),
};

export default BrandTabViewComponent;
