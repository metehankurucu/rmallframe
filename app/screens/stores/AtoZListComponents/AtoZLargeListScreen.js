import React from "react";
import { ActivityIndicator, InteractionManager, SafeAreaView, Text, View, FlatList, StyleSheet } from "react-native";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import ListHeader from "react-native-mall-frame-client/app/components/stores/ListHeader";
import { getDefaultLanguageText, translateText, } from "react-native-mall-frame-client/app/translation/translate";
import ListItem from "react-native-mall-frame-client/app/components/stores/ListItem";
import ModalSelector from "react-native-mall-frame-client/app/components/common/modal/ModalSelector";
import { Indicator } from "react-native-mall-frame-client/app/components/common";
import AlphabetPicker from "react-native-mall-frame-client/app/components/common/ATOZListView/AlphabetPicker";

class AtoZLargeListScreen extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            filteredStores: this.props.stores,
            renderPlaceholderOnly: true,
            alphabet: this.props.stores.map((object) => {
                return object.title;
            })
        };
    }

    UNSAFE_componentWillMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({ renderPlaceholderOnly: false });
        });

        if (this.props.navigation.state && this.props.navigation.state.params && this.props.navigation.state.params.notification) {
            const { data } = this.props.navigation.state.params;
            let navigateStore = {};

            this.props.stores.filter((storeArray) => {
                storeArray.data.filter((store) => {
                    const isTrue = store._id === data;
                    if (isTrue) {
                        navigateStore = store;
                    }
                    return isTrue;
                });
            });

            this.props.navigation.navigate("StoreDetail", {
                data: navigateStore,
                title: navigateStore.name
            });
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.searchTerm !== this.props.searchTerm) {
            this.filterStores(nextProps.searchTerm);
        }
    }

    filterStores = (searchTerm: string) => {
        const filteredStores = searchTerm !== "" ? JSON.parse(JSON.stringify(this.props.stores))
            .filter((object) => {
                const store = object;

                store.data = store.data.filter((st) => {
                    return getDefaultLanguageText(st.name)
                        .toLowerCase()
                        .indexOf(searchTerm.toLowerCase()) > -1;
                });
                return store.data.length > 0;
            }) : this.props.stores;

        this.setState({
            filteredStores,
            alphabet: filteredStores.map((object) => object.title)
        });
    };

    _onTouchLetter = (letter) => {
        if (this.flatlist)
            this.flatlist.scrollToIndex({ animated: true, index: this.state.alphabet.indexOf(letter) })
    };

    _wayfinderItem = (store) => {
        const item = {
            id: store["_id"],
            type: "store",
            title: store.name,
            image_url: store.logo,
            floor_names: store.floor_names,
            rawData: store
        };

        const locations = item.rawData.locations.map((location) => {
            const floor = location.s.substring(0, location.s.indexOf("_"));
            const extra = floor.length === 3 ? "-" : "";

            return {
                key: location.s,
                label: `${translateText("Floor")} ${extra}${floor[floor.length - 1]}`
            };
        })
        item.id = locations.length > 0 ? locations[0].key : ''
        this.props.navigation.state.params.returnData(item);
        this.props.navigation.goBack();

    }

    onScrollToIndexFailed = info => {
        const wait = new Promise(resolve => setTimeout(resolve, 500));
        wait.then(() => {
            this.flatlist.current?.scrollToIndex({ index: info.index, animated: true });
        });
    }

    onPressItem = (sectionIndex, itemIndex) => {
        const item = this.state.filteredStores[sectionIndex].data[itemIndex];
        if (this.props.wayfinderList) {
            return this._wayfinderItem(item);

        }
        this.props.navigation.navigate("StoreDetail", {
            data: item,
            title: item.name
        });
    }

    renderItem = ({ item, index }) => {
        const data = item.data ? item.data : [];
        return (
            <View key={item.title}>
                <ListHeader title={item.title} />
                {
                    data.map((dataItem, itemIndex) => {
                        return (
                            <ListItem
                                onPress={() => this.onPressItem(index, itemIndex)}
                                key={dataItem._id}
                                item={dataItem}
                                firstItemSection={itemIndex === 0}
                            />
                        )
                    })
                }
            </View>
        )
    }

    renderContent = () => {
        const { filteredStores } = this.state;
        if (this.state.filteredStores) {
            return (
                <View style={styles.listWrapper}
                >
                    <FlatList
                        data={filteredStores}
                        renderItem={this.renderItem}
                        ref={ref => this.flatlist = ref}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ marginRight: 25 }}
                        keyExtractor={(item, index) => index.toString()}
                        onScrollToIndexFailed={this.onScrollToIndexFailed}
                        ListEmptyComponent={<Text style={styles.emptyText}>Empty</Text>}
                    />
                    <SafeAreaView style={styles.rightColumn}>
                        <AlphabetPicker alphabet={this.state.alphabet} onTouchLetter={this._onTouchLetter} />
                    </SafeAreaView>
                    <ModalSelector
                        ref={(modal) => {
                            this.modal = modal;
                        }}
                        cancelText={translateText("Cancel")}
                        data={this.state.locations}
                        onChange={(option) => {
                            const data = this.state.item;
                            data["id"] = option.key;
                            this.props.navigation.state.params.returnData(data);
                            this.props.navigation.goBack();
                        }}
                    />
                </View>
            );
        }
        return (<ActivityIndicator size="small" color="#fff" />);
    };

    render() {
        if (!this.state.renderPlaceholderOnly) {
            return this.renderContent();
        }
        return (
            <Indicator />
        );
    }
}

const styles = StyleSheet.create({
    emptyText: {
        fontSize: 20,
        alignSelf: "center"
    },
    listWrapper: {
        height: "100%",
        width: "100%",
        flexDirection: "row"
    },
    rightColumn: {
        position: "absolute",
        backgroundColor: "transparent",
        top: 0,
        bottom: 0,
        right: 0,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'red'
    }
})

const mapStateToProps = (state) => {
    const { payload } = state.main;
    const { user } = state.auth;

    if (payload !== null) {
        const data = payload.stores;
        const newArray = [];
        data.forEach((store) => {
            const object = store;

            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_stores"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                object["fav"] = !!match;
                object["userid"] = user["_id"];
            }
            else {
                object["fav"] = false;
                object["userid"] = null;
            }

            if (newArray.filter((datas) => {
                if (datas.title === getDefaultLanguageText(object.name)
                    .charAt(0)
                    .toUpperCase()) {
                    datas.data.push(object);
                    return true;
                }
                return false;
            }).length === 0) {
                newArray.push({
                    data: [object],
                    title: getDefaultLanguageText(object.name)
                        .charAt(0)
                        .toUpperCase()
                });
            }

        });

        newArray.sort((a, b) => {
            return a.title.localeCompare(b.title);
        });

        return {
            stores: newArray,
            user
        };
    }

    return {
        stores: [],
        user: null
    };
};

AtoZLargeListScreen.propTypes = {
    stores: PropTypes.instanceOf(Array),
    navigation: PropTypes.instanceOf(Object),
    wayfinderList: PropTypes.bool,
    searchTerm: PropTypes.string
};

AtoZLargeListScreen.defaultProps = {
    stores: [],
    navigation: {},
    wayfinderList: false,
    searchTerm: ""
};

export default connect(mapStateToProps)(AtoZLargeListScreen);
