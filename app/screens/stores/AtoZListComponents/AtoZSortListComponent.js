import React from "react";
import { ActivityIndicator, InteractionManager, SafeAreaView, SectionList, View } from "react-native";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";
import ListHeader from "../../../components/stores/ListHeader";
import { getDefaultLanguageText, translateText, } from "../../../translation/translate";
import ListItem from "../../../components/stores/ListItem";
import ModalSelector from "../../../components/common/modal/ModalSelector";
import { Indicator } from "../../../components/common";
import AlphabetPicker from "../../../components/common/ATOZListView/AlphabetPicker";
import { favoriteAction } from "../../../actions/Favorite";

class AtoZSortListComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this._renderContent = this._renderContent.bind(this);
        this.state = {
            filteredStores: this.props.stores,
            renderPlaceholderOnly: true,
            alphabet: this.props.stores.map(object => {
                return object.title;
            })
        };
    }

    UNSAFE_componentWillMount() {

        InteractionManager.runAfterInteractions(() => {
            this.setState({ renderPlaceholderOnly: false });
        });

        if (this.props.navigation.state && this.props.navigation.state.params && this.props.navigation.state.params.notification) {
            const { data } = this.props.navigation.state.params;
            let navigateStore = {};

            this.props.stores.filter((storeArray) => {
                storeArray.data.filter((store) => {
                    const isTrue = store._id === data;
                    if (isTrue) {
                        navigateStore = store;
                    }
                    return isTrue;
                });
            });

            this.props.navigation.navigate("StoreDetail", {
                data: navigateStore,
                title: navigateStore.name
            });
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.searchTerm !== this.props.searchTerm) {
            this.filterStores(nextProps.searchTerm);
        }
    }

    filterStores = (searchTerm: string) => {
        this.setState({
            filteredStores: searchTerm !== "" ? JSON.parse(JSON.stringify(this.props.stores))
                .filter((object) => {
                    const store = object;

                    store.data = store.data.filter((st) => {
                        return getDefaultLanguageText(st.name)
                            .toLowerCase()
                            .indexOf(searchTerm.toLowerCase()) > -1;
                    });
                    return store.data.length > 0;
                }) : this.props.stores
        }, () => {
            const alphabet = this.state.filteredStores.map(object => {
                return object.title;
            });

            this.setState({
                alphabet: alphabet
            });

        });
    };

    _renderItem = ({ item, index, section }) => {
        if (item) {
            return (
                <ListItem
                    item={item}
                    firstItemSection={index === 0}
                    onFavPress={() => {
                        this.props.favoriteAction({
                            userid: item["userid"],
                            isFav: item["fav"],
                            item_id: item["_id"],
                            item_type: "favorite_stores"
                        });
                    }}
                    onPress={() => {
                        if (this.props.wayfinderList) {
                            this._wayfinderItem(item);
                            return;
                        }

                        this.props.navigation.navigate("StoreDetail", {
                            data: item,
                            title: item.name
                        });
                    }}
                />
            );
        }
        return null;
    };

    _renderHeader = ({ section }) => {

        if (section) {
            return (
                <ListHeader section={section} />
            );
        }

        return null;
    };

    _wayfinderItem = (store) => {
        const item = {
            id: store["_id"],
            type: "store",
            title: store.name,
            image_url: store.logo,
            floor_names: store.floor_names,
            rawData: store
        };

        const locations = item.rawData.locations.map((location) => {
            const floor = location.s.substring(0, location.s.indexOf("_"));
            const extra = floor.length === 3 ? "-" : "";

            return {
                key: location.s,
                label: `${translateText("Floor")} ${extra}${floor[floor.length - 1]}`
            };
        });

        if (locations.length > 1) {
            this.setState({
                locations,
                item
            }, () => {
                this.modal.open();
            });
        }
        else if (locations.length === 1) {
            item["id"] = item.rawData.locations[0].s;
            this.props.navigation.state.params.returnData(item);
            this.props.navigation.goBack();
        }
    };

    _onTouchLetter = (letter) => {
        setTimeout(() => {
            this._listView.scrollToLocation({
                animated: true,
                sectionIndex: this.state.alphabet.indexOf(letter),
                itemIndex: 0,
                viewPosition: 0
            }, 300);
        });
    };

    _renderContent = () => {

        if (this.state.filteredStores) {
            return (
                <View style={{
                    height: "100%",
                    width: "100%",
                    flexDirection: "row"
                }}
                >
                    <SectionList
                        renderItem={this._renderItem}
                        style={{ marginRight: 25 }}
                        renderSectionHeader={this._renderHeader}
                        sections={this.state.filteredStores}
                        keyExtractor={(item, index) => {
                            return index;
                        }}
                        ref={(view) => {
                            this._listView = view;
                        }}
                        maxToRenderPerBatch={1111}
                        initialNumToRender={1111}
                        stickySectionHeadersEnabled
                    />

                    <SafeAreaView style={{
                        position: "absolute",
                        backgroundColor: "transparent",
                        top: 0,
                        bottom: 0,
                        right: 0,
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                    >
                        <AlphabetPicker alphabet={this.state.alphabet} onTouchLetter={this._onTouchLetter} />
                    </SafeAreaView>

                    <ModalSelector
                        ref={(modal) => {
                            this.modal = modal;
                        }}
                        cancelText={translateText("Cancel")}
                        data={this.state.locations}
                        onChange={(option) => {
                            const data = this.state.item;
                            data["id"] = option.key;
                            this.props.navigation.state.params.returnData(data);
                            this.props.navigation.goBack();
                        }}
                    />
                </View>
            );
        }

        return (
            <ActivityIndicator size="small" color="#fff" />
        );

    };

    render() {
        if (!this.state.renderPlaceholderOnly) {
            return this._renderContent();
        }

        return (
            <Indicator />
        );
    }
}

const mapStateToProps = (state) => {
    const { payload } = state.main;
    const { user } = state.auth;

    if (payload !== null) {
        const data = payload.stores;
        const newArray = [];
        data.forEach((store) => {
            const object = store;

            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_stores"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                object["fav"] = !!match;
                object["userid"] = user["_id"];
            }
            else {
                object["fav"] = false;
                object["userid"] = null;
            }

            if (newArray.filter((datas) => {
                if (datas.title === getDefaultLanguageText(object.name)
                    .charAt(0)
                    .toUpperCase()) {
                    datas.data.push(object);
                    return true;
                }
                return false;
            }).length === 0) {
                newArray.push({
                    data: [object],
                    title: getDefaultLanguageText(object.name)
                        .charAt(0)
                        .toUpperCase()
                });
            }

        });

        newArray.sort((a, b) => {
            return a.title.localeCompare(b.title);
        });

        return {
            stores: newArray,
            user
        };
    }

    return {
        stores: [],
        user: null
    };
};

AtoZSortListComponent.propTypes = {
    stores: PropTypes.instanceOf(Array),
    navigation: PropTypes.instanceOf(Object),
    wayfinderList: PropTypes.bool,
    searchTerm: PropTypes.string
};

export default connect(mapStateToProps, { favoriteAction })(AtoZSortListComponent);
