import React from "react";
import { FlatList } from "react-native";
import { connect } from "react-redux";
import CategoryListItem from "../../components/stores/CategoryListItem";
import { getDefaultLanguageText } from "../../translation/translate";
import PropTypes from "prop-types";

class CategoryListScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { state } = navigation;

        return {
            title: `${state.params.title}`.toUpperCase(),
        };
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            filteredCategory: this.props.categories
        };
    }

    filterCategories = (searchTerm: string) => {
        this.setState({
            filteredCategory: searchTerm !== "" ? JSON.parse(JSON.stringify(this.props.categories))
                .filter((category) => {
                    return getDefaultLanguageText(category.name)
                        .toLowerCase()
                        .indexOf(searchTerm.toLowerCase()) > -1;
                }) : this.props.categories
        });
    };

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.searchTerm !== this.props.searchTerm) {
            this.filterCategories(nextProps.searchTerm);
        }
    }

    onPressedCard = (item) => {
        if (item.stores.length !== 0) {

            this.props.navigation.navigate("StoresList", {
                stores: item.stores,
                title: item.name
            });
        }
    };

    _renderItem({ item }) {
        return (
            <CategoryListItem
                item={item}
                onPress={() => {
                    return this.onPressedCard(item);
                }}
            />
        );
    }

    render() {
        return (
            <FlatList
                data={this.state.filteredCategory}
                keyExtractor={(item, key) => {
                    return key.toString();
                }}
                renderItem={this._renderItem.bind(this)}
                style={styles.container}
            />
        );
    }
}

const styles = {
    container: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 15
    }
};

const mapStateToProps = (state) => {
    const { payload } = state.main;
    const { user } = state.auth;

    if (payload !== null) {
        const categories = payload.categories;
        const st = payload.stores;

        categories.sort((a, b) => {
            return getDefaultLanguageText(a.name)
                .localeCompare(getDefaultLanguageText(b.name));
        });
        categories.forEach((c) => {
            c.stores = [...st.filter((store) => {
                if (user !== null && user.isLoggedIn) {
                    const match = user["favorite_stores"].find((obj) => {
                        return obj["s"] === store["_id"];
                    });
                    store["fav"] = !!match;
                    store["userid"] = user["_id"];
                }

                return store.categories.filter((category) => {
                    return category["s"] === c["_id"];
                }).length > 0;
            })];
        });
        return { categories };
    }

    return { categories: [] };
};

CategoryListScreen.propTypes = {
    categories: PropTypes.array,
    searchTerm: PropTypes.string,
    navigation: PropTypes.object
};

export default connect(mapStateToProps)(CategoryListScreen);
