import React from "react";
import {TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import {CachedImage} from "react-native-img-cache";
import {DrawerActions} from "react-navigation";

import config from "../../../config/config";
import {translateText} from "../../translation/translate";
import {Icon} from "../../components/common";

class BrandScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("BRANDS")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("BRANDS")
                .toUpperCase(),
        };
    };

    render() {
        const {Component} = config().BrandScreen;

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

const storesPagerRedux = connect(mapStateToProps)(BrandScreen);
export {storesPagerRedux as BrandScreen};
