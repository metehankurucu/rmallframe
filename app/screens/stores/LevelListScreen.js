import React from "react";
import { connect } from "react-redux";
import { Alert, FlatList } from "react-native";
import PropTypes from "prop-types";
import CategoryListItem from "../../components/stores/CategoryListItem";
import { translateText } from "../../translation/translate";

const styles = {
    container: {
        paddingLeft: 15,
        paddingRight: 15,
    }
};

class LevelListScreen extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.onPressedCard = this.onPressedCard.bind(this);
        this.state = {
            filteredLevel: this.props.floors
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.searchTerm !== this.props.searchTerm) {
            this.filterFloors(nextProps.searchTerm);
        }
    }

    onPressedCard = (item) => {
        if (item.stores.length !== 0) {
            return this.props.navigation.navigate("StoresList", {
                stores: item.stores,
                title: item.name
            });
        }

        Alert.alert(translateText("Stores"), translateText("stores_not_found"));
    };

    filterFloors = (searchTerm: string) => {
        this.setState({
            filteredLevel: searchTerm !== "" ? JSON.parse(JSON.stringify(this.props.floors))
                .filter((level) => {
                    return level.name["en"].toLowerCase()
                        .indexOf(searchTerm.toLowerCase()) > -1;
                }) : this.props.floors
        });
    };

    _renderItem = ({ item }) => {
        return (
            <CategoryListItem
                item={item}
                onPress={() => {
                    return this.onPressedCard(item);
                }}
            />
        );
    };

    render() {
        return (
            <FlatList
                data={this.state.filteredLevel}
                keyExtractor={(item) => {
                    return item["_id"];
                }}
                renderItem={this._renderItem}
                style={styles.container}
            />
        );
    }
}

const mapStateToProps = (state) => {
    const { payload } = state.main;
    if (payload !== null) {
        const floors = payload.floors;
        const st = payload.stores;

        floors.forEach((c) => {
            c.stores = [...st.filter((store) => {
                return store.locations.filter((floor) => {
                    return c.locations.includes(floor["s"]);
                }).length > 0;
            })];
        });
        return { floors: floors.reverse() };
    }
    return { floors: [] };
};

LevelListScreen.propTypes = {
    floors: PropTypes.array,
    searchTerm: PropTypes.string,
    navigation: PropTypes.object
};

export default connect(mapStateToProps)(LevelListScreen);
