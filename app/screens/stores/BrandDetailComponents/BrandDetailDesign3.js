// eslint-disable-next-line import/extensions
/* eslint-disable import/extensions,import/no-unresolved,import/no-unresolved */
import React, {Component} from "react";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import Toast, {DURATION} from "react-native-easy-toast";
import {CachedImage} from "react-native-img-cache";
import PropTypes from "prop-types";
import Communications from "react-native-communications";
import {getDefaultLanguageText, translateText} from "../../../translation/translate";
import config from "../../../../config/config";
import ModalSelector from "../../../components/common/modal/ModalSelector";
import AutoHeightWebView from "../../../components/common/AutoHeightWebView";
import {FavButton} from "../../../components/buttons";
import {Button} from "../../../components/common";

class BrandDetailDesign1 extends Component {
    constructor(props, context) {
        super(props, context);

    }

    selectLocationMallMap = (data) => {
        if (data.locations.length <= 1) {
            return (
                <View style={this.styles.FAVButtonContainerStyle}>
                    <Button
                        style={this.styles.rippleButtonStyle}
                        onPress={() => {
                            if (data.locations.length === 0) {
                                if (this.toast) {
                                    this.toast.show(translateText("location_not_found"), DURATION.LENGTH_LONG);
                                }
                            }
                            else {
                                this.props.navigation.navigate("MallMap", {
                                    locationOpen: data.locations[0].s,
                                    locationType: "store"
                                });
                            }
                        }}
                    >
                        <View style={{
                            flexDirection: "row",
                            alignSelf: "center"
                        }}
                        >
                            <CachedImage
                                source={require("../../../../assets/icons/ic_land_show_map.png")}
                                resizeMode="contain"
                                style={{
                                    width: 16,
                                    height: 18,
                                    resizeMode: "contain"
                                }}
                            />
                            <Text style={this.styles.locationMallMapText}>{translateText("ShowOnMap").toUpperCase()}</Text>
                        </View>
                    </Button>
                </View>
            );
        }

        const locations = data.locations.map((location) => {
            const floor = location.s.substring(0, location.s.indexOf("_store_"));
            const extra = floor.length === 3 ? "-" : "";
            return {
                key: location.s,
                label: `${translateText("Floor")} ${extra}${floor[floor.length - 1]}`
            };
        });
        return (
            <View style={this.styles.FAVButtonContainerStyle}>
                <ModalSelector
                    data={locations}
                    style={{flex: 1}}
                    cancelText={translateText("Cancel")}
                    buttonStyle={this.styles.rippleButtonStyle}
                    onChange={(option) => {
                        this.props.navigation.navigate("MallMap", {
                            locationOpen: option.key,
                            locationType: "store"
                        });
                    }}
                >
                    <View style={{
                        flexDirection: "row",
                        alignSelf: "center"
                    }}
                    >
                        <CachedImage
                            source={require("../../../../assets/icons/ic_land_show_map.png")}
                            resizeMode="contain"
                            style={{
                                width: 16,
                                height: 16,
                                resizeMode: "contain"
                            }}
                        />
                        <Text style={this.styles.locationMallMapText}>SHOW ON MAP</Text>
                    </View>
                </ModalSelector>
            </View>
        );
    };

    selectLocationWayfinder = (data) => {
        if (data.locations.length <= 1) {
            return (
                <Button
                    style={this.styles.buttonStyle}
                    onPress={() => {

                        if (data.locations.length === 0) {
                            if (this.toast) {
                                this.toast.show("Location not found.", DURATION.LENGTH_SHORT);
                            }
                        }
                        else {

                            this.props.navigation.navigate("Wayfinder", {
                                to: {
                                    id: data.locations[0].s,
                                    type: "store",
                                    title: data.name,
                                    image_url: data.logo,
                                    rawData: data
                                }
                            });
                        }
                    }}
                >
                    <Text style={this.styles.button}>{translateText("Wayfinder")
                        .toUpperCase()}
                    </Text>
                </Button>
            );
        }

        const locations = data.locations.map((location) => {
            const floor = location.s.substring(0, location.s.indexOf("_store_"));
            const extra = floor.length === 3 ? "-" : "";

            return {
                key: location.s,
                label: `${translateText("Floor")} ${extra}${floor[floor.length - 1]}`
            };
        });
        return (
            <ModalSelector
                data={locations}
                style={{flex: 1}}
                cancelText={translateText("Cancel")}
                buttonStyle={this.styles.buttonStyle}
                onChange={(option) => {
                    this.props.navigation.navigate("Wayfinder", {
                        to: {
                            id: option.key,
                            type: "store",
                            title: data.name,
                            image_url: data.logo,
                            rawData: data
                        }
                    });
                }}
            >
                <Text style={this.styles.button}>{translateText("Wayfinder")
                    .toUpperCase()}
                </Text>
            </ModalSelector>
        );
    };
    styles = {
        container: {
            padding: 15,
            flex: 1,
            backgroundColor: "#f7f7f7"
        },
        image: {
            resizeMode: "contain",
            width: "100%",
            height: 220,
        },
        imageLogo: {
            resizeMode: "contain",
            width: 64,
            height: 64,
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: "#999"
        },
        logoContainer: {
            flexDirection: "row",
            justifyContent: "space-between",
        },
        button: {
            color: "#000",
            width: "100%",
            textAlign: "center",
            fontSize: 14,
            fontWeight: "300",
            paddingTop: 10,
            paddingBottom: 10,

        },
        buttonStyle: {
            flex: 1,
            margin: 8,
            backgroundColor: "#fff",
            borderRadius: 25,
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: "#eee",
            elevation: 1,
            shadowOffset: {
                width: 0,
                height: 0
            },
            shadowColor: "#222",
            shadowOpacity: 0.2
        },
        buttonGroup: {
            justifyContent: "space-around",
            flexDirection: "row",
            marginTop: 10,
        },
        title: {
            fontWeight: "bold",
            fontSize: 20,
            marginBottom: 5,
            width: "100%",
            color: "#000"
        },
        floorText: {
            fontSize: 14,
            fontWeight: "600",
            color: config().DEFAULT_BUTTON_HIGHLIGHT_COLOR,
        },
        floorContainer: {
            flexDirection: "row"
        },
        rippleButtonStyle: {
            minWidth: 150,
            height: 40,
            backgroundColor: "#000",
            opacity: 0.8,
            alignItems: "center",
            justifyContent: "center"
        },
        locationMallMapText: {
            color: "#fff"
        },
        FAVButtonContainerStyle: {
            right: 0,
            top: 180,
            zIndex: 1,
            position: "absolute",
        }
    };

    render() {
        const {PlaceHolderImage} = config();

        const {data} = this.props.navigation.state.params;

        return (
            <View style={{flex: 1}}>
                <ScrollView style={{
                    flex: 1,
                    backgroundColor: "#f7f7f7"
                }}
                >
                    <View style={{
                        width: "100%",
                        height: 220
                    }}
                    >
                        {data.image ? <CachedImage
                            style={this.styles.image}
                            defaultSource={PlaceHolderImage}
                            source={{uri: data.image}}
                        /> : <CachedImage
                            style={this.styles.image}
                            source={PlaceHolderImage}
                        />}
                        {this.selectLocationMallMap(data)}
                    </View>
                    <View style={this.styles.container}>
                        <View style={{flexDirection: "row"}}>
                            <CachedImage
                                source={{uri: data.logo}}
                                defaultSource={PlaceHolderImage}
                                style={this.styles.imageLogo}
                            />
                            <View style={{
                                justifyContent: "center",
                                paddingLeft: 10,
                                flex: 1
                            }}
                            >
                                <View style={{
                                    flexDirection: "row",
                                    justifyContent: "space-between"
                                }}
                                >
                                    <View>
                                        <Text
                                            numberOfLines={1}
                                            style={this.styles.title}
                                        >{getDefaultLanguageText(data.name)}
                                        </Text>
                                        <View style={this.styles.floorContainer}>
                                            <CachedImage
                                                style={{
                                                    width: 16,
                                                    height: 16
                                                }}
                                                source={require("../../../../assets/icons/store_map_icon.png")}
                                            />
                                            <Text
                                                style={this.styles.floorText}
                                                numberOfLines={1}
                                            >{getDefaultLanguageText(data.floor_names)}
                                            </Text>
                                        </View>
                                    </View>
                                    <FavButton
                                        iconStyle={{padding: 12}}
                                        fav={data.fav}
                                        onPress={() => {
                                            this.props.favoriteAction({
                                                userid: data["userid"],
                                                isFav: data["fav"],
                                                item_id: data["_id"],
                                                item_type: "favorite_stores"
                                            });
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                        <Text>{data.subtitle}</Text>
                        <AutoHeightWebView
                            width={"100%"}
                            navigation={this.props.navigation}
                            defaultUrl={data.explanation}
                            autoHeight
                        />

                        <View style={this.styles.buttonGroup}>
                            <Button
                                style={this.styles.buttonStyle}
                                onPress={() => {
                                    if (data.phone !== "") {
                                        Communications.phonecall(data.phone, false);
                                    }
                                    else if (data.phone === "" && this.toast) {
                                        this.toast.show(translateText("location_not_found"), DURATION.LENGTH_LONG);
                                    }
                                }}
                            >
                                <Text
                                    style={[this.styles.button, {color: data.phone === "" ? "#999999" : "#000"}]}
                                >
                                    {translateText("Call")
                                        .toUpperCase()}
                                </Text>
                            </Button>
                            <Button
                                style={this.styles.buttonStyle}
                                onPress={() => {
                                    // send to email
                                    if (data.email !== "") {
                                        Communications.email([data.email], null, null, "", "");
                                    }
                                    else if (data.phone === "" && this.toast) {
                                        this.toast.show(translateText("email_not_found."), DURATION.LENGTH_LONG);
                                    }
                                }}
                            >
                                <Text
                                    style={[this.styles.button, {color: data.email === "" ? "#999999" : "#000"}]}
                                >
                                    {translateText("Email")
                                        .toUpperCase()}
                                </Text>
                            </Button>
                            {this.selectLocationWayfinder(data)}
                        </View>
                        <Toast ref={(toast) => {
                            this.toast = toast;
                        }}
                        />
                    </View>

                </ScrollView>
            </View>
        );
    }
}

BrandDetailDesign1.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    favoriteAction: PropTypes.func
};

export default BrandDetailDesign1;
