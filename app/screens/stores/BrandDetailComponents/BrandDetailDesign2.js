/* eslint-disable import/no-unresolved,import/extensions */
import React, {Component} from "react";
import {ScrollView, StyleSheet, Text, TouchableOpacity, View, WebView} from "react-native";
import Toast, {DURATION} from "react-native-easy-toast";
import {CachedImage} from "react-native-img-cache";
import PropTypes from "prop-types";

import Communications from "react-native-communications";
import {getDefaultLanguageText, translateText} from "../../../translation/translate";

import config from "../../../../config/config";
import ModalSelector from "../../../components/common/modal/ModalSelector";
import {FavButton} from "../../../components/buttons";
import AutoHeightWebView from "../../../components/common/AutoHeightWebView";
import {Button} from "../../../components/common";

class BrandDetailDesign2 extends Component {

    constructor(props, context) {
        super(props, context);

        const {MapHtmlFile} = config();

        this.state = {
            url: MapHtmlFile,
            selectedFloor: 0,
        };
    }

    onMessage = (event) => {
        const message = JSON.parse(event.nativeEvent.data);
        switch (message.action) {
        case "mapLoaded":
            this.mapLoadAfter();
            break;
        default:
            break;
        }
    };

    mapLoadAfter = () => {
        const {data} = this.props.navigation.state.params;

        this.postMessage({
            action: "highlightLocations",
            payload: data.locations.map((location) => {
                return location.s;
            })
        });
    };

    levelSelect = (no) => {
        this.setState({
            selectedFloor: no
        });
        this.postMessage({
            action: "selectFloor",
            floorNo: no
        });
    };

    postMessage = (data) => {
        if (this.webview) {
            this.webview.postMessage(JSON.stringify(data));
        }
        else {
            console.log("webview is not loaded");
        }
    };

    _initMap = () => {
        if (this.props.levels && this.props.levels.length !== 0) {
            this.props.levels.forEach((level) => {
                this.postMessage({
                    action: "loadLevel",
                    level,
                });
            });

            this.postMessage({
                action: "init",
                categories: this.props.categories
            });
        }
    };

    selectLocationMallMap = (data) => {
        if (data.locations.length <= 1) {
            return (
                <Button
                    style={this.styles.buttonStyle}
                    onPress={() => {
                        if (data.locations.length === 0) {
                            if (this.toast) {
                                this.toast.show(translateText("LocationNotFound"), DURATION.LENGTH_LONG);
                            }
                        }
                        else {
                            this.props.navigation.navigate("MallMap", {
                                locationOpen: data.locations[0].s,
                                locationType: "store",
                                navType: "push"
                            });
                        }
                    }}
                >
                    <Text style={this.styles.button}>{translateText("Floor Plan")}</Text>

                </Button>
            );
        }

        const locations = data.locations.map((location) => {
            const floor = location.s.substring(0, location.s.indexOf("_store_"));
            const extra = floor.length === 3 ? "-" : "";
            return {
                key: location.s,
                label: `${getDefaultLanguageText("Floor")} ${extra}${floor[floor.length - 1]}`
            };
        });

        return (
            <ModalSelector
                data={locations}
                style={{flex: 1}}
                buttonStyle={this.styles.buttonStyle}
                onChange={(option) => {
                    this.props.navigation.navigate("MallMap", {
                        locationOpen: option.key,
                        locationType: "store",
                        navType: "push"
                    });
                }}
            >
                <Text style={this.styles.button}>{translateText("Floor Plan")}</Text>
            </ModalSelector>

        );
    };

    _locationView = (data) => {
        if (data.locations.length > 0) {
            return (
                <WebView
                    ref={(a) => {
                        this.webview = a;
                    }}
                    source={this.state.url}
                    onMessage={this.onMessage}
                    style={this.styles.webViewStyle}
                    scrollEnabled={false}
                    onLoadEnd={this._initMap}
                />
            );
        }

        return null;
    };

    selectLocationWayfinder = (data) => {
        if (data.locations.length <= 1) {
            return (
                <Button
                    style={this.styles.buttonStyle}
                    onPress={() => {

                        if (data.locations.length === 0) {
                            if (this.toast) {
                                this.toast.show(translateText("LocationNotFound"), DURATION.LENGTH_SHORT);
                            }
                        }
                        else {
                            this.props.navigation.navigate("Wayfinder", {
                                to: {
                                    id: data.locations[0].s,
                                    type: "store",
                                    title: data.name,
                                    image_url: data.logo,
                                    rawData: data
                                },
                            });
                        }
                    }}
                >
                    <Text style={this.styles.button}>{translateText("Navigation")}</Text>
                </Button>
            );
        }

        const locations = data.locations.map((location) => {
            const floor = location.s.substring(0, location.s.indexOf("_store_"));
            const extra = floor.length === 3 ? "-" : "";

            return {
                key: location.s,
                label: `${getDefaultLanguageText("Floor")} ${extra}${floor[floor.length - 1]}`
            };
        });
        return (
            <ModalSelector
                data={locations}
                style={{flex: 1}}
                buttonStyle={this.styles.buttonStyle}
                onChange={(option) => {
                    this.props.navigation.navigate("Wayfinder", {
                        to: {
                            id: option.key,
                            type: "store",
                            title: data.name,
                            image_url: data.logo,
                            rawData: data
                        },
                    });
                }}
            >
                <Text style={this.styles.button}>{translateText("Navigation")}</Text>
            </ModalSelector>
        );
    };
    styles = {
        container: {
            flex: 1
        },
        image: {
            resizeMode: "cover",
            width: "100%",
            height: 220,
        },
        imageLogo: {
            resizeMode: "contain",
            width: 90,
            height: 90,
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: "#444444"
        },
        cardContainer: {
            backgroundColor: "#fff",
            marginRight: 15,
            marginLeft: 15,
            padding: 15,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2
            },
            shadowOpacity: 0.1,
            flex: 1
        },
        logoContainer: {
            flexDirection: "row",
            justifyContent: "space-between",
            margin: 10
        },
        button: {
            backgroundColor: config().BASE_COLOR,
            color: "#fff",
            padding: 10,
            textAlign: "center",
            fontSize: 16,
            fontWeight: "300"
        },
        buttonStyle: {
            flex: 1,
            backgroundColor: "#103768",
            marginLeft: 5,
            marginRight: 5
        },
        buttonGroup: {
            justifyContent: "space-around",
            flexDirection: "row",
            marginTop: 16,
        },
        p: {
            fontSize: 14,
            fontWeight: "300",
        },
        textContainer: {
            flexDirection: "column",
            flex: 1,
            marginTop: 10,
            marginLeft: 10
        },
        floorText: {
            fontSize: 13,
            fontWeight: "500",
            color: "#898989",
        },
        storeText: {
            fontSize: 18,
            fontWeight: "500",
            marginBottom: 10
        },
        webViewStyle: {
            height: 200,
            width: "100%",
        },
        webviewContainer: {
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderColor: "#444444"
        }
    };

    render() {
        const {data} = this.props.navigation.state.params;

        return (
            <ScrollView style={this.styles.container}>
                <View style={this.styles.webviewContainer}>
                    {this._locationView(data)}
                </View>
                <View style={this.styles.cardContainer}>
                    <View style={this.styles.logoContainer}>
                        <CachedImage
                            source={{uri: data.logo}}
                            style={this.styles.imageLogo}
                        />
                        <View style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContent: "space-between"
                        }}
                        >
                            <View style={this.styles.textContainer}>
                                <Text
                                    style={this.styles.storeText}
                                    numberOfLines={1}
                                >{getDefaultLanguageText(data.name)}
                                </Text>
                                <View style={{flexDirection: "row"}}>
                                    {data.locations.length > 0 && getDefaultLanguageText(data.floor_names)
                                        .split(",")
                                        .map((floor, index) => {
                                            return (
                                                <TouchableOpacity
                                                    key={index.toString()}
                                                    activeOpacity={this.state.selectedFloor === index ? 1 : 0.7}
                                                    style={{
                                                        marginRight: 8,
                                                        borderRadius: 4,
                                                        borderWidth: this.state.selectedFloor === index ? StyleSheet.hairlineWidth : 0,
                                                        borderColor: config().BASE_COLOR,
                                                        backgroundColor: this.state.selectedFloor === index ? "transparent" : "#c8c8c8",
                                                        padding: 6

                                                    }}
                                                    onPress={() => {
                                                        if (this.state.selectedFloor !== index) {
                                                            this.setState({
                                                                selectedFloor: index
                                                            }, () => {
                                                                this.postMessage({
                                                                    action: "reload",
                                                                });
                                                            });
                                                        }
                                                    }}
                                                >
                                                    <Text
                                                        style={[this.styles.floorText, {color: this.state.selectedFloor === index ? "#000" : "#898989"}]}
                                                    >{floor}
                                                    </Text>
                                                </TouchableOpacity>
                                            );
                                        })}
                                </View>

                            </View>
                            <FavButton
                                fav={data.fav}
                                iconStyle={{margin:12}}
                                onPress={() => {
                                    this.props.favoriteAction({
                                        userid: data["userid"],
                                        isFav: data["fav"],
                                        item_id: data["_id"],
                                        item_type: "favorite_stores"
                                    });
                                }}
                            />
                        </View>
                    </View>

                    <AutoHeightWebView
                        navigation={this.props.navigation}
                        defaultUrl={data.explanation}
                        autoHeight
                    />

                    <View style={{marginTop: 15}}>
                        <View style={this.styles.buttonGroup}>
                            <Button
                                style={this.styles.buttonStyle}
                                onPress={() => {
                                    return Communications.phonecall(data.phone, false);
                                }}
                            >
                                <Text style={this.styles.button}>{translateText("Call")}</Text>
                            </Button>
                            <Button
                                style={this.styles.buttonStyle}
                                onPress={() => {
                                    return Communications.email([data.email], null, null, "", "");
                                }}
                            >
                                <Text style={this.styles.button}>{translateText("E-Mail")}</Text>
                            </Button>
                        </View>
                        <View style={this.styles.buttonGroup}>
                            {this.selectLocationMallMap(data)}
                            {this.selectLocationWayfinder(data)}
                        </View>
                    </View>
                </View>

                <Toast
                    position="bottom"
                    ref={(toast) => {
                        this.toast = toast;
                    }}
                />
            </ScrollView>
        );
    }
}

BrandDetailDesign2.propTypes = {
    navigation: PropTypes.object,
    levels: PropTypes.array,
    favoriteAction: PropTypes.func,
    categories: PropTypes.array
};

export default BrandDetailDesign2;
