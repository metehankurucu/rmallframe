import React from "react";
import {View} from "react-native";
import config from "../../../config/config";

class BrandImageGalleryScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.title.toUpperCase().toUpperCase(),
    });

    render() {
        const BrandImageGallery = config().BrandImageGalleryScreen;

        if (!BrandImageGallery)
            return (
                <View>

                </View>
            );

        return (
            <BrandImageGallery {...this.props}/>
        )
    }
}

export {BrandImageGalleryScreen}