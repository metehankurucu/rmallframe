/* eslint-disable import/prefer-default-export */
import React from "react";
import {ScrollView, Text, TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import {DrawerActions} from "react-navigation";
import PropTypes from "prop-types";
import {translateText} from "../../translation/translate";
import {EventListItem} from "../../components/events";
import {GridView, Icon} from "../../components/common";
import config from "../../../config/config";

const styles = {
    subTitle: {
        marginTop: 15,
        padding: 10,
        paddingBottom: 0,
        fontSize: 15,
    }
};

class EventScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("EVENTS")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("EVENTS")
                .toUpperCase(),
        };
    };

    componentDidMount() {
        if (this.props.navigation.state && this.props.navigation.state.params && this.props.navigation.state.params.notification) {
            const {event} = this.props.navigation.state.params;
            let navigateEvent = {};

            this.props.events.map((eventArray) => {
                const isTrue = eventArray._id === event;
                if (isTrue) {
                    navigateEvent = eventArray;
                }

                return eventArray;
            });

            this.props.navigation.navigate("EventDetail", {
                data: navigateEvent,
                title: navigateEvent.title
            });
        }
    }

    renderItem = (data) => {
        return (
            <EventListItem
                data={data}
                onPress={() => {
                    this.props.navigation.navigate("EventDetail", {
                        data,
                        title: data.title
                    });

                }}
            />
        );
    };

    render() {
        const {Enabled, Component} = config().EventScreen;

        if (!Enabled) {
            return (
                <ScrollView>
                    <GridView
                        items={this.props.events}
                        renderItem={this.renderItem}
                        itemsPerRow={2}
                    />

                    {!!this.props.oldEvents && (
                        <Text style={styles.subTitle}> {translateText("OLD_EVENTS")} </Text>
                    )}
                    {!!this.props.oldEvents && (
                        <GridView
                            items={this.props.oldEvents}
                            renderItem={this.renderItem}
                            itemsPerRow={2}
                        />
                    )}
                </ScrollView>

            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    const {payload} = state.main;
    const {user} = state.auth;

    if (payload !== null) {
        payload.events.forEach((object) => {
            const event = object;

            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_events"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                event["fav"] = !!match;
                event["userid"] = user["_id"];
            }
            else {
                event["fav"] = false;
                event["userid"] = null;
            }

            return event;
        });

        if (payload["old-events"]) {
            payload["old-events"].forEach((object) => {
                const event = object;

                if (user !== null && user.isLoggedIn) {
                    const match = user["favorite_events"].find((obj) => {
                        return obj["s"] === object["_id"];
                    });
                    event["fav"] = !!match;
                    event["userid"] = user["_id"];
                }
                else {
                    event["fav"] = false;
                    event["userid"] = null;
                }

                return event;
            });

            return {
                events: payload["events"],
                oldEvents: payload["old-events"]
            };
        }

        return {
            events: payload.events
        };
    }
    return {
        events: null
    };
};

EventScreen.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    events: PropTypes.instanceOf(Array),
    oldEvents: PropTypes.instanceOf(Array)
};

EventScreen.propTypes = {
    navigation: {},
    events: [],
    oldEvents: []
};

const EventsRedux = connect(mapStateToProps)(EventScreen);

export {EventsRedux as EventScreen};
