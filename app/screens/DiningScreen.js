/* eslint-disable valid-typeof */
import React from "react";
import {connect} from "react-redux";
import {TouchableOpacity} from "react-native";
import {DrawerActions} from "react-navigation";
import _ from "lodash";
import config from "../../config/config";
import {translateText} from "../translation/translate";
import {BrandListScreen} from "./stores/BrandListScreen";
import {Icon} from "../components/common";

class DiningScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("DINING")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("Dining")
                .toUpperCase(),
        };
    };

    render() {
        const {Component, Enabled} = config().DiningScreen;
        if (!Enabled) {
            const data = {...this.props.navigation, ...{state: {params: {stores: this.props.category.stores}}}};

            return (
                <BrandListScreen navigation={data}/>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({main, auth}) => {
    const {payload} = main;
    const {user} = auth;

    if (payload !== null) {
        const {categories, mall} = payload;
        const st = payload.stores;

        const {DiningCategoryId} = config().DiningScreen;

        if (!DiningCategoryId && !mall.dining_category && !mall.dining_categories) {
            throw new Error("You must declare DiningCategoryId on config file...");
        }

        const category = _.find(categories, {"_id": DiningCategoryId || mall.dining_category});

        if (!category && mall.dining_categories) {
            const cat = {};

            cat.stores = [...st.filter((store) => {
                if (user !== null && user.isLoggedIn) {
                    const match = user["favorite_stores"].find((obj) => {
                        return obj["s"] === store["_id"];
                    });
                    store.fav = !!match;
                    store.userid = user["_id"];
                }
                return store.categories.filter((ct) => {
                    return !!_.find(mall.dining_categories, {"s": ct["s"]});
                }).length > 0;
            })];

            return {category: cat};
        }


        category.stores = [...st.filter((store) => {
            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_stores"].find((obj) => {
                    return obj["s"] === store["_id"];
                });
                store.fav = !!match;
                store.userid = user["_id"];
            }

            return store.categories.filter((cat) => {
                return cat["s"] === category["_id"];
            }).length > 0;
        })];

        return {category};
    }
    return {category: []};
};

const DiningRedux = connect(mapStateToProps)(DiningScreen);

export {DiningRedux as DiningScreen};
