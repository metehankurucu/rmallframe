/* eslint-disable global-require */
import React from "react";
import {TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import {DrawerActions} from "react-navigation";
import {CachedImage} from "react-native-img-cache";

import config from "../../../config/config";
import {translateText} from "../../translation/translate";
import {setLocale} from "../../actions/i18n";
import {checkServer} from "../../actions/Main";
import {Icon} from "../../components/common/";

class HomeScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {DrawerMenuIcon, headerTitleStyle, ProfileMenuIcon} = config();

        return {
            title: translateText("Home")
                .toUpperCase(),
            headerTitleStyle: {
                alignSelf: "center",
                ...headerTitleStyle
            },
            headerLeft: (
                <TouchableOpacity onPress={() => {
                    const {isDrawerOpen} = navigation.state;
                    if (!isDrawerOpen) {
                        navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                    }
                }}
                >
                    <Icon {...DrawerMenuIcon}/>
                </TouchableOpacity>
            ),
            headerRight: (
                <TouchableOpacity onPress={() => {
                    navigation.navigate("Profile");
                }}
                >
                    <Icon {...ProfileMenuIcon}/>
                </TouchableOpacity>
            )
        };
    };

    render() {
        const {Component} = config().HomeScreen;

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({main, i18n, auth}) => {
    const {payload} = main;
    const {user} = auth;
    /** @namespace payload.events */
    /** @namespace payload.promotions */
    /** @namespace payload.news */
    /** @namespace payload.offers */

    const data = {};

    if (config().offersKeymap.enabled) {
        data["offers"] = payload.offers.map((offer) => {
            const object = offer;
            if (user && user.isLoggedIn) {
                const match = user["favorite_offers"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                object["fav"] = !!match;
                object["userid"] = user["_id"];
            }
            else {
                object["fav"] = false;
                object["userid"] = null;
            }

            object.store = payload.stores.filter((store) => {
                return store._id === object.store_id;
            })[0];

            if (object.store) {
                if (user !== null && user.isLoggedIn) {
                    const match = user["favorite_stores"].find((obj) => {
                        return obj["s"] === object.store["_id"];
                    });

                    object.store["fav"] = !!match;
                    object.store["userid"] = user["_id"];
                }
                else {
                    object.store["fav"] = false;
                    object.store["userid"] = null;
                }
            }

            return object;
        });
    }

    if (config().eventsKeymap.enabled) {
        data["events"] = payload.events.map((event) => {

            const object = event;
            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_events"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                object["fav"] = !!match;
                object["userid"] = user["_id"];
            }
            else {
                object["fav"] = false;
                object["userid"] = null;
            }
            return object;
        });
    }

    if (config().promotionsKeymap.enabled) {
        data["promotions"] = payload.promotions.forEach((promotion) => {
            const object = promotion;
            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_promotions"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                object["fav"] = !!match;
                object["userid"] = user["_id"];
            }
            else {
                object["fav"] = false;
                object["userid"] = null;
            }
        });
    }

    if (config().newsKeymap.enabled) {
        data["whatsNews"] = payload.news.forEach((whatsnew) => {
            const object = whatsnew;
            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_news"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                object["fav"] = !!match;
                object["userid"] = user["_id"];
            }
            else {
                object["fav"] = false;
                object["userid"] = null;
            }
        });

    }

    data["stores"] = payload.stores;
    data["currentLocale"] = i18n.locale;
    data["user"] = user;
    data["mall"] = main.payload.mall;
    return data;
};

const HomeRedux = connect(mapStateToProps, {
    setLocale,
    checkServer
})(HomeScreen);

export {HomeRedux as HomeScreen};

