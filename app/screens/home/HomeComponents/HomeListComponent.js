import React, {Component} from "react";
import {Dimensions, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import PropTypes from "prop-types";
import LinearGradient from "react-native-linear-gradient";
import ParallaxScrollView from "react-native-parallax-scroll-view";

import config from "../../../../config/config";
import {getDefaultLanguageText, translateText} from "../../../translation/translate";
import {Card, Icon} from "../../../components/common";
import CardSection from "../../../components/common/CardSection";
import {BorderedButton} from "../../../components/buttons";
import {Normalize} from "../../../../config/helpers";

const {width} = Dimensions.get("window");

const styles = {
    searchInput: {
        padding: 10,
        borderColor: "#fff",
        borderBottomWidth: StyleSheet.hairlineWidth,
        fontSize: 16,
        fontWeight: "400",
        height: 40,
        color: "#fff"
    },
    searchTabText: {
        color: "#fff",
        fontSize: 12,
        marginTop: 4
    },
    headerStyle: {
        backgroundColor: "#fff"
    },
    textContent: {
        backgroundColor: "rgba(0, 0, 0, 0.3)",
        zIndex: 1,
        flex: 1,
        width: "100%",
        alignItems: "center",
        justifyContent: "center"
    },
    sliderItemText: {
        color: "#fff",
        fontWeight: 500,
        fontSize: 24,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: "cover",
    },
    itemContainer: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        marginTop: 10
    },
    footerImage: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: "cover",
        width: "100%",
    },
    footerText: {
        color: "#fff",
        fontWeight: 500,
        fontSize: 18,
        marginBottom: 16
    },
    headerContentStyle: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 10
    },
    headerTextStyle: {
        fontSize: 18,
        fontWeight: "300"
    },
    inlineContentStyle: {
        marginLeft: 10,
        marginRight: 10,
        flexDirection: "column"
    },
    imageStyle: {
        flex: 1,
        resizeMode: "cover",
        width: null
    },
    title: {
        fontSize: 15,
        fontWeight: "600",
        marginTop: 10,
        marginBottom: 10
    },
    subtitle: {
        fontSize: 15,
        marginTop: 10,
        marginBottom: 10
    },
    buttonStyle: {
        marginTop: 10,
        marginBottom: 15,
        alignSelf: "center",
    },
    linearGradient: {
        alignSelf: "center",
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 30
    },
    buttonText: {
        fontSize: 13,
        textAlign: "center",
        margin: 10,
        color: "#ffffff",
        fontWeight: "600"
    },
    campainContainer: {
        backgroundColor: "#959595",
        padding: 15,
        margin: 15,
        borderColor: "#fff",
        borderWidth: 1,
        borderRadius: 5
    },
    titleContainer: {
        color: "#fff",
        textAlign: 'center',
        fontSize: 15
    }
};

class HomeListComponent extends Component {
    static defaultProps = {
        stores: [],
        cinemas: [],
        events: [],
        offers: [],
        promotions: [],
        whatsNews: [],
        navigation: {},
        user: {}
    };
    getRandomStore = (BoxHeight) => {
        const {store} = this.state;

        return (
            <View style={[styles.itemContainer, {
                borderColor: config().BASE_COLOR
            }]}
            >
                <View style={styles.headerContentStyle}>
                    <Text
                        style={styles.headerTextStyle}
                    >
                        {translateText("Stores")
                            .toUpperCase()}
                    </Text>
                </View>
                <Card>
                    <CachedImage
                        style={[styles.imageStyle, {
                            width,
                            height: width * (1080 / 1920)
                        }]}
                        source={{uri: store.image || store.logo}}
                    />
                    <CardSection style={styles.inlineContentStyle}>
                        <Text style={styles.title}>{getDefaultLanguageText(store.name)}</Text>
                        {!!store.subtitle &&
                        <Text style={styles.subtitle}>{getDefaultLanguageText(store.subtitle)}</Text>}
                        <TouchableOpacity
                            style={styles.buttonStyle}
                            onPress={() => {
                                this.props.navigation.navigate("Brands");
                            }}
                        >
                            <LinearGradient
                                colors={[config().BASE_COLOR, config().BASE_THIRD_COLOR]}
                                style={styles.linearGradient}
                            >
                                <Text style={styles.buttonText}>
                                    {translateText("Brands")
                                        .toUpperCase()}
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </CardSection>
                </Card>
            </View>
        );
    };
    getRandomEvent = (BoxHeight) => {
        const {event} = this.state;

        return (
            <View style={[styles.itemContainer, {
                borderColor: config().BASE_COLOR
            }]}
            >
                <View style={styles.headerContentStyle}>
                    <Text
                        style={styles.headerTextStyle}
                    >
                        {
                            translateText("Events")
                                .toUpperCase()
                        }
                    </Text>
                </View>
                <Card>
                    <CachedImage
                        style={[styles.imageStyle, {
                            width,
                            height: width * (1080 / 1920)
                        }]}
                        source={{uri: event.image || event.icon}}
                    />
                    <CardSection style={styles.inlineContentStyle}>
                        <Text style={styles.title}>{getDefaultLanguageText(event.title)}</Text>
                        {!!event.subtitle &&
                        <Text style={styles.subtitle}>{getDefaultLanguageText(event.subtitle)}</Text>}

                        <TouchableOpacity
                            style={styles.buttonStyle}
                            onPress={() => {
                                this.props.navigation.navigate("Events");
                            }}
                        >
                            <LinearGradient
                                colors={[config().BASE_COLOR, config().BASE_THIRD_COLOR]}
                                style={styles.linearGradient}
                            >
                                <Text style={styles.buttonText}>
                                    {translateText("Events")
                                        .toUpperCase()}
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </CardSection>
                </Card>
            </View>
        );
    };
    getRandomOffer = (BoxHeight) => {
        const {offer} = this.state;

        return (
            <View style={[styles.itemContainer, {
                borderColor: config().BASE_COLOR
            }]}
            >
                <View style={styles.headerContentStyle}>
                    <Text
                        style={styles.headerTextStyle}
                    >
                        {
                            translateText("Offers")
                                .toUpperCase()
                        }
                    </Text>
                </View>
                <Card>
                    <CachedImage
                        style={[styles.imageStyle, {
                            width,
                            height: width * (1080 / 1920)
                        }]}
                        source={{uri: offer.image || offer.icon}}
                    />
                    <CardSection style={styles.inlineContentStyle}>
                        <Text style={styles.title}>{getDefaultLanguageText(offer.title)}</Text>
                        {!!offer.subtitle &&
                        <Text style={styles.subtitle}>{getDefaultLanguageText(offer.subtitle)}</Text>}

                        <TouchableOpacity
                            style={styles.buttonStyle}
                            onPress={() => {
                                this.props.navigation.navigate("Offers");
                            }}
                        >
                            <LinearGradient
                                colors={[config().BASE_COLOR, config().BASE_THIRD_COLOR]}
                                style={styles.linearGradient}
                            >
                                <Text style={styles.buttonText}>
                                    {translateText("Offers")
                                        .toUpperCase()}
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>

                    </CardSection>
                </Card>
            </View>
        );
    };
    getRandomPromotion = (BoxHeight) => {
        const {promotion} = this.state;
        return (
            <View style={[styles.itemContainer, {
                borderColor: config().BASE_COLOR
            }]}
            >
                <View style={styles.headerContentStyle}>
                    <Text
                        style={styles.headerTextStyle}
                    >
                        {
                            translateText("Promotions")
                                .toUpperCase()
                        }
                    </Text>
                </View>
                <Card>
                    <CachedImage
                        style={[styles.imageStyle, {
                            width,
                            height: width * (1080 / 1920)
                        }]}
                        source={{uri: promotion.image || promotion.icon}}
                    />
                    <CardSection style={styles.inlineContentStyle}>
                        <Text style={styles.title}>{getDefaultLanguageText(promotion.title)}</Text>
                        {!!promotion.subtitle &&
                        <Text style={styles.subtitle}>{getDefaultLanguageText(promotion.subtitle)}</Text>}

                        <TouchableOpacity
                            style={styles.buttonStyle}
                            onPress={() => {
                                this.props.navigation.navigate("Promotions");
                            }}
                        >
                            <LinearGradient
                                colors={[config().BASE_COLOR, config().BASE_THIRD_COLOR]}
                                style={styles.linearGradient}
                            >
                                <Text style={styles.buttonText}>
                                    {translateText("Promotions")
                                        .toUpperCase()}
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </CardSection>
                </Card>
            </View>
        );
    };
    getRandomNews = (BoxHeight) => {
        const {whatsNew} = this.state;
        return (
            <View style={[styles.itemContainer, {
                borderColor: config().BASE_COLOR
            }]}
            >
                <View style={styles.headerContentStyle}>
                    <Text
                        style={styles.headerTextStyle}
                    >
                        {
                            translateText("WhatsNews")
                                .toUpperCase()
                        }
                    </Text>
                </View>
                <Card>
                    <CachedImage
                        style={[styles.imageStyle, {
                            width,
                            height: width * (1080 / 1920)
                        }]}
                        source={{uri: whatsNew.image || whatsNew.icon}}
                    />
                    <CardSection style={styles.inlineContentStyle}>
                        <Text style={styles.title}>{getDefaultLanguageText(whatsNew.title)}</Text>
                        {!!whatsNew.subtitle &&
                        <Text style={styles.subtitle}>{getDefaultLanguageText(whatsNew.subtitle)}</Text>}
                        <BorderedButton
                            style={styles.buttonStyle}
                            onPress={() => {
                                this.props.navigation.navigate("WhatsNews");

                            }}
                            title={translateText("WhatsNews")
                                .toUpperCase()}
                        />
                    </CardSection>
                </Card>
            </View>
        );
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            store: this.props.stores[Math.floor(Math.random() * this.props.stores.length)],
            event: this.props.events[Math.floor(Math.random() * this.props.events.length)],
            offer: this.props.offers[Math.floor(Math.random() * this.props.offers.length)],
            promotion: this.props.promotions[Math.floor(Math.random() * this.props.promotions.length)],
            whatsNew: this.props.whatsNews[Math.floor(Math.random() * this.props.whatsNews.length)]
        };
    }

    render() {
        const {Refreshable, BoxHeight, SearchBarBackgroundImage} = config().HomeScreen;
        const {mall} = this.props;

        return (
            <View style={{
                backgroundColor: "#fff",
                flex: 1
            }}
            >
                <ParallaxScrollView
                    parallaxHeaderHeight={Normalize(250)}
                    stickyHeaderHeight={Normalize(65)}
                    backgroundColor="#333"
                    renderBackground={() => {
                        return (
                            <CachedImage
                                style={{
                                    width: "100%",
                                    height: "100%",
                                    resizeMode: "cover"
                                }}
                                source={SearchBarBackgroundImage}
                            />
                        );
                    }}
                    renderStickyHeader={() => {
                        return (
                            <View
                                key="sticky-header"
                                style={{
                                    flexDirection: "row",
                                    width: "100%",
                                    alignItems: "center",
                                    marginTop: 15,
                                    justifyContent: "space-around"
                                }}
                            >
                                <TouchableOpacity
                                    style={{
                                        alignItems: "center"
                                    }}
                                    onPress={() => {
                                        this.props.navigation.navigate("Brands");
                                    }}
                                >

                                    <Icon name="stores" size={25} color="#fff"/>
                                    <Text style={styles.searchTabText}>{translateText("Brands")}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{
                                        alignItems: "center"
                                    }}
                                    onPress={() => {
                                        this.props.navigation.navigate("Cinema");
                                    }}
                                >
                                    <Icon name="cinema" size={25} color="#fff"/>
                                    <Text style={styles.searchTabText}>{translateText("Cinema")}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{
                                        alignItems: "center"
                                    }}
                                    onPress={() => {
                                        this.props.navigation.navigate("MallMap");
                                    }}
                                >
                                    <Icon name="mall-map" size={25} color="#fff"/>
                                    <Text style={styles.searchTabText}>{translateText("MallMap")}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{
                                        alignItems: "center"
                                    }}
                                    onPress={() => {
                                        this.props.navigation.navigate("ContactUs");
                                    }}
                                >
                                    <Icon name="transportation" size={25} color="#fff"/>
                                    <Text style={styles.searchTabText}>{translateText("ContactUs")}</Text>
                                </TouchableOpacity>
                            </View>
                        );
                    }}
                    renderForeground={() => {
                        return (
                            <View style={{
                                width: "100%",
                                height: Normalize(250)
                            }}
                            >
                                <TouchableOpacity
                                    style={{
                                        zIndex: 99999,
                                        padding: 15,
                                    }}
                                    onPress={() => {
                                        this.props.navigation.navigate("Search");
                                    }}
                                >
                                    <View style={{
                                        flexDirection: "row",
                                        height: 40,
                                        alignSelf: "center",
                                        alignItems: "center",
                                        justifyContent: "flex-start",
                                        width: "90%",
                                        borderBottomWidth: StyleSheet.hairlineWidth,
                                        borderColor: "#fff",
                                        marginTop: 25,
                                        marginBottom: 15
                                    }}
                                    >
                                        <Icon
                                            style={{marginRight: 5}}
                                            name="home-search"
                                            color="#fff"
                                            size={15}
                                        />
                                        <Text style={{
                                            color: "#fff",
                                            fontSize: 13
                                        }}
                                        >
                                            {translateText("begin_exploring")}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                {this.props.user && this.props.user.name && this.props.user.surname &&
                                <View style={{
                                    flexDirection: "row",
                                    alignSelf: "center",
                                }}
                                >
                                    <Text style={{
                                        fontSize: 18,
                                        alignSelf: "center",
                                        color: "#fff",
                                        margin: 5
                                    }}
                                    >{
                                        translateText("Welcome")
                                            .toUpperCase()
                                    }
                                    </Text>

                                    <Text style={{
                                        fontSize: 18,
                                        alignSelf: "center",
                                        color: config().BASE_COLOR,
                                    }}
                                    >{`${this.props.user.name}`.toUpperCase()}
                                    </Text>
                                </View>}
                                <View style={{
                                    flexDirection: "row",
                                    position: "absolute",
                                    bottom: 25,
                                    width: "100%",
                                    justifyContent: "space-around"
                                }}
                                >
                                    <TouchableOpacity
                                        style={{
                                            alignItems: "center"
                                        }}
                                        onPress={() => {
                                            this.props.navigation.navigate("Brands");
                                        }}
                                    >

                                        <Icon name="stores" size={25} color="#fff"/>
                                        <Text style={styles.searchTabText}>{translateText("Brands")}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{
                                            alignItems: "center"
                                        }}
                                        onPress={() => {
                                            this.props.navigation.navigate("Cinema");
                                        }}
                                    >
                                        <Icon name="cinema" size={25} color="#fff"/>
                                        <Text style={styles.searchTabText}>{translateText("Cinema")}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{
                                            alignItems: "center"
                                        }}
                                        onPress={() => {
                                            this.props.navigation.navigate("MallMap");
                                        }}
                                    >
                                        <Icon name="mall-map" size={25} color="#fff"/>
                                        <Text style={styles.searchTabText}>{translateText("MallMap")}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{
                                            alignItems: "center"
                                        }}
                                        onPress={() => {
                                            this.props.navigation.navigate("ContactUs");
                                        }}
                                    >
                                        <Icon name="transportation" size={25} color="#fff"/>
                                        <Text style={styles.searchTabText}>{translateText("ContactUs")}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        );
                    }}
                >
                    <View style={{backgroundColor: "#fff"}}>
                        {mall.campaign && mall.campaign.enabled && (
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate("Browser", {
                                        uri: mall.campaign.url,
                                        title: getDefaultLanguageText(mall.campaign.title)
                                    });
                                }}
                                style={[styles.campainContainer, {backgroundColor: config().BASE_COLOR}]}>
                                <Text style={styles.titleContainer}>{getDefaultLanguageText(mall.campaign.title)}</Text>

                            </TouchableOpacity>
                        )}
                        {!!this.props.stores.length && this.getRandomStore(BoxHeight)}
                        {!!this.props.events.length && this.getRandomEvent(BoxHeight)}
                        {!!this.props.offers.length && this.getRandomOffer(BoxHeight)}
                        {!!this.props.promotions.length && this.getRandomPromotion(BoxHeight)}
                        {!!this.props.whatsNews.length && this.getRandomNews(BoxHeight)}
                    </View>
                </ParallaxScrollView>
            </View>
        );
    }
}

HomeListComponent.propTypes = {
    stores: PropTypes.instanceOf(Array),
    cinemas: PropTypes.instanceOf(Array),
    navigation: PropTypes.instanceOf(Object),
    user: PropTypes.instanceOf(Object),
    whatsNews: PropTypes.instanceOf(Array),
    promotions: PropTypes.instanceOf(Array),
    events: PropTypes.instanceOf(Array),
    offers: PropTypes.instanceOf(Array)
};

export default HomeListComponent;
