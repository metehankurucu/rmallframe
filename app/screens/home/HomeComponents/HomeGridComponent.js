/* eslint-disable no-nested-ternary */
/* eslint-disable global-require */
import React from "react";
import {Dimensions, RefreshControl, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {CachedImage} from "react-native-img-cache";
import {NavigationActions} from "react-navigation";
import Swiper from "react-native-swiper";
import PropTypes from "prop-types";

import config from "../../../../config/config";
import {translateText} from "../../../translation/translate";
import {swiperRouteNames} from "../../../../config/utils";
import {Normalize} from "../../../../config/helpers";
import {Button} from "../../../components/common";

const {width} = Dimensions.get("window");

const styles = {
    defaultImageStyle: {
        width,
        height: "100%",
        resizeMode: "cover"
    },
    arrowIcon: {
        width: 30,
        height: 30,
        marginBottom: 15,
        marginTop: 15,
        marginLeft: 5,
        marginRight: 5,
        resizeMode: "contain"
    },
    cardStyle: {
        flexDirection: "row",
        width: "100%",
        flex: 1
    },
    backgroundImage: {
        backgroundColor: "#ccc",
        resizeMode: "cover",
        position: "absolute",
        width: "100%",
        height: "100%",
        justifyContent: "center",
    },
    cardSectionStyle: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
    },
    cardTitleStyle: {
        backgroundColor: "transparent",
        color: "#fff",
        fontSize: Normalize(15),
        marginTop: 5,
        textAlign: "center",
    },
    slide3: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1
    },
    buttonStyle: {
        flex: 1,
        borderWidth: 1,
        borderColor: "#fff"
    },
    translateText: {
        color: "#fff",
        margin: 15
    },
    textStyle: {fontWeight: "800"},
    tab: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row"
    },
    tabCountText: {
        fontSize: 11,
        color: "#fff"
    },
    tabCountContainer: {
        backgroundColor: "#292E31",
        borderRadius: 16,
        height: 16,
        width: 16,
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 6,
        marginLeft: 6
    }
};

class HomeGridComponent extends React.Component {
    defaultProps = {
        checkServer: () => {
        }
    };

    constructor(props, context) {
        super(props, context);

    }

    state = {
        refreshing: false
    };

    _onRefresh = () => {
        this.setState({refreshing: true});
        this.props.checkServer()
            .then(() => {
                this.setState({refreshing: false});
            });
    };

    _getSwiperContent(routeName, data) {
        if (swiperRouteNames.filter((rn) => {
            return rn === routeName;
        }).length === 0) {
            throw new Error(`${routeName} is not swappable.`);
        }

        const {PlaceHolderImage} = config();

        if (data && !!data.length) {
            return data.map((dt) => {
                return (
                    <View key={dt["_id"]} style={styles.slide3}>
                        <TouchableOpacity
                            activeOpacity={0.9}
                            onPress={() => {
                                const navigateAction = NavigationActions.navigate({
                                    routeName
                                });


                                const navigateDetailAction = NavigationActions.navigate({
                                    routeName: routeName === "Events" ? "EventDetail" : routeName === "Offers" ? "OfferDetail" : routeName === "WhatsNews" ? "WhatsNewDetail" : "PromotionDetail",
                                    params: {
                                        data: dt,
                                        title: dt.title
                                    }
                                });

                                if (!this.state.beginDrag) {
                                    this.props.navigation.dispatch(navigateAction);
                                    this.props.navigation.dispatch(navigateDetailAction);
                                }
                                else {
                                    this.setState({
                                        beginDrag: false
                                    });
                                }
                            }}
                        >

                            <CachedImage
                                style={styles.defaultImageStyle}
                                defaultSource={PlaceHolderImage}
                                source={{uri: dt.image}}
                            />
                        </TouchableOpacity>
                    </View>
                );
            });
        }
        return (
            <View style={styles.slide3}>
                <CachedImage style={styles.defaultImageStyle} source={PlaceHolderImage}/>
            </View>
        );
    }

    render() {
        const {
            DefaultGridType, Refreshable, BoxHeight, SwiperHeight
        } = config().HomeScreen;
        const swiperHeight = SwiperHeight || width - BoxHeight / 2 * 3;

        return (
            <ScrollView
                refreshControl={Refreshable ?
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        title={translateText("loading")}
                        titleColor="#868686"
                        onRefresh={this._onRefresh}
                    /> : null
                }
            >
                {
                    DefaultGridType.map((row, index) => {
                        return (
                            <View key={index.toString()} style={styles.cardStyle}>
                                {
                                    row.map((col) => {
                                        if (col.swiper) {
                                            const data = col.routeName === "Events" ? this.props.events : col.routeName === "Offers" ? this.props.offers : col.routeName === "WhatsNews" ? this.props.whatsNews : this.props.promotions;

                                            return (
                                                <View
                                                    key={col.routeName}
                                                    style={{
                                                        width,
                                                        height: swiperHeight * 3 / 2,
                                                    }}
                                                >
                                                    <Swiper
                                                        showsButtons={data.length > 1}
                                                        showsPagination={false}
                                                        loadMinimal
                                                        autoplay={false}
                                                        loop={false}
                                                        onScrollBeginDrag={() => {
                                                            this.setState({
                                                                beginDrag: true
                                                            });
                                                        }}
                                                        nextButton={<CachedImage
                                                            style={styles.arrowIcon}
                                                            source={require("../../../../assets/icons/home-right-arrow.png")}
                                                        />}
                                                        prevButton={<CachedImage
                                                            style={styles.arrowIcon}
                                                            source={require("../../../../assets/icons/home-left-arrow.png")}
                                                        />}
                                                    >
                                                        {
                                                            this._getSwiperContent(col.routeName, data, swiperHeight * 3 / 2)
                                                        }
                                                    </Swiper>
                                                </View>
                                            );
                                        }

                                        return (
                                            <Button
                                                key={col.routeName}
                                                style={styles.buttonStyle}
                                                onPress={() => {
                                                    this.props.navigation.navigate(col.routeName);
                                                }}
                                            >
                                                <View style={[styles.cardSectionStyle, {height: BoxHeight}]}>
                                                    <CachedImage
                                                        style={styles.backgroundImage}
                                                        source={col.backgroundImage}
                                                    />
                                                    <CachedImage source={col.icon}/>
                                                    <Text
                                                        style={styles.cardTitleStyle}
                                                    >
                                                        {
                                                            translateText(col.name)
                                                                .toUpperCase()
                                                        }
                                                    </Text>
                                                </View>
                                            </Button>
                                        );

                                    })
                                }
                            </View>
                        );
                    })
                }
            </ScrollView>
        );
    }
}

HomeGridComponent.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    checkServer: PropTypes.func,
    whatsNews: PropTypes.instanceOf(Array),
    promotions: PropTypes.instanceOf(Array),
    events: PropTypes.instanceOf(Array),
    offers: PropTypes.instanceOf(Array)
};

export default HomeGridComponent;
