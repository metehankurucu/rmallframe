/* eslint-disable import/prefer-default-export */
import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import {DrawerActions} from "react-navigation";
import {connect} from "react-redux";
import {CachedImage} from "react-native-img-cache";
import PropTypes from "prop-types";
import {getDefaultLanguageText, translateText} from "../../translation/translate";
import config from "../../../config/config";
import {OfferListItem} from "../../components/offers/";
import {GridView, Icon} from "../../components/common";

class OfferScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("Offers")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("Offers")
                .toUpperCase(),
        };
    };

    componentDidMount() {
        if (this.props.navigation.state && this.props.navigation.state.params && this.props.navigation.state.params.notification) {
            const {offer} = this.props.navigation.state.params;
            let navigateOffer = false;

            this.props.offers.filter((offerArray) => {
                const isTrue = offerArray._id === offer;
                if (isTrue) {
                    navigateOffer = offerArray;
                }
                return isTrue;
            });
            if (navigateOffer) {
                this.props.navigation.navigate("OfferDetail", {
                    data: navigateOffer,
                    title: navigateOffer.title
                });
            }
        }
    }

    renderItem = (item) => {
        return (
            <OfferListItem
                offer={item}
                onPress={() => {
                    this.props.navigation.navigate("OfferDetail", {
                        data: item,
                        title: getDefaultLanguageText(item.title)
                    });
                }}
            />
        );
    };

    render() {
        const {Enabled, Component} = config().OfferScreen;

        if (!Enabled) {
            return (
                <GridView
                    items={this.props.offers}
                    itemsPerRow={2}
                    renderItem={this.renderItem}
                />
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = ({main, auth}) => {
    const {payload} = main;
    const {user} = auth;

    if (payload !== null) {
        payload.offers.forEach((object) => {
            const offer = object;

            if (user && user.isLoggedIn) {
                const match = user["favorite_offers"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                offer["fav"] = !!match;
                offer["userid"] = user["_id"];
            }
            else {
                offer["fav"] = false;
                offer["userid"] = null;
            }

            offer.store = payload.stores.filter((store) => {
                return store._id === object.store_id;
            })[0];

            if (offer.store) {
                if (user !== null && user.isLoggedIn) {
                    const match = user["favorite_stores"].find((obj) => {
                        return obj["s"] === object.store["_id"];
                    });

                    offer.store["fav"] = !!match;
                    offer.store["userid"] = user["_id"];
                }
                else {
                    offer.store["fav"] = false;
                    offer.store["userid"] = null;
                }
            }

            return offer;
        });
        return {
            offers: payload.offers
        };
    }

    return {offers: null};
};

OfferScreen.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    offers: PropTypes.instanceOf(Array)
};

const OffersRedux = connect(mapStateToProps)(OfferScreen);

export {OffersRedux as OfferScreen};
