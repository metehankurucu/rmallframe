/* eslint-disable import/no-unresolved,import/prefer-default-export,import/extensions */
/* eslint-disable global-require */

import React from "react";
import { Dimensions, Platform, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import PropTypes from "prop-types";
import { CachedImage } from "react-native-img-cache";
import { connect } from "react-redux";
import { favoriteAction } from "../../actions/Favorite";
import { getDefaultLanguageText, translateText } from "../../translation/translate";
import { shareData } from "../../actions/Main";
import { FavButton } from "../../components/buttons";
import AutoHeightWebView from "../../components/common/AutoHeightWebView";
import config from "../../../config/config";

const { width } = Dimensions.get("window");

class OfferDetailScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { state } = navigation;
        return {
            title: `${getDefaultLanguageText(state.params.title)}`.toUpperCase(),
        };
    };

    styles = {
        dateContainer: {
            paddingRight: 10,
            height: 48,
            alignItems: "center",
            justifyContent: "space-between",
            flexDirection: "row",
            borderBottomWidth: 1,
            borderTopWidth: StyleSheet.hairlineWidth,
            borderColor: "#ccc",
            backgroundColor: "#fff",
        },
        imageStyle: {
            width: "100%",
            height: 130,
            resizeMode: "cover"
        },
        dateText: {
            fontSize: 15,
            color: "#787878"
        },
        container: {
            flex: 1,
        },
        likeButtonContainer: {
            position: "absolute",
            right: 0,
            zIndex: 1,
            margin: 15
        },
        image: {
            width,
            height: width * (1080 / 1920),
        },
        button: {
            fontSize: 16,
            color: config().BASE_SECOND_COLOR
        },
        buttonGroup: {
            flexDirection: "row",
        },
        title: {
            fontSize: 18,
            fontWeight: "600",
            alignSelf: "center",
            marginLeft: Platform.OS === "android" ? 20 : 0,
            marginTop: 15
        },
        subtitle: {
            fontSize: 16,
            fontWeight: "600",
            marginLeft: 30,
            marginTop: 30
        },
        explanation: {
            fontSize: 13,
            marginTop: 10,
        },
        favButton: {
            position: "absolute",
            width: 50,
            height: 50,
            borderRadius: 25,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: config().BASE_COLOR,
            right: 15,
            bottom: 15,
        },
        favText: {
            color: "#fff",
            fontSize: 15
        }
    };

    render() {
        const { Enabled, BackgroundColor, Component } = config().OfferDetailScreen;

        if (!Enabled) {
            const { data } = this.props.navigation.state.params;

            return (
                <View style={{
                    flex: 1,
                    backgroundColor: BackgroundColor
                }}
                >
                    <View style={this.styles.container}>
                        <CachedImage source={{ uri: data.image }} style={this.styles.image} />
                        <View style={this.styles.dateContainer}>
                            <FavButton
                                fav={data.fav}
                                iconStyle={{ margin: 10 }}
                                onPress={() => {
                                    this.props.favoriteAction({
                                        userid: data["userid"],
                                        isFav: data["fav"],
                                        item_id: data["_id"],
                                        item_type: "favorite_offers"
                                    });
                                }}
                            />
                            <Text
                                style={this.styles.button}
                                onPress={() => {
                                    const shareOptions = {
                                        title: getDefaultLanguageText(data.title),
                                        message: getDefaultLanguageText(data.title),
                                        url: data.image,
                                        subject: getDefaultLanguageText(data.title),
                                        type: "image/jpeg"
                                    };

                                    this.props.shareData(shareOptions);
                                }}
                            >
                                {translateText("Share")
                                    .toUpperCase()}
                            </Text>
                        </View>
                        <Text style={this.styles.title}>{getDefaultLanguageText(data.title)}</Text>
                        {!!data.subtitle &&
                            <Text style={this.styles.subtitle}>{getDefaultLanguageText(data.subtitle)}</Text>}
                        <AutoHeightWebView
                            navigation={this.props.navigation}
                            defaultUrl={data.explanation}
                            autoHeight
                        />
                    </View>
                    {data.store && (
                        <TouchableOpacity
                            style={this.styles.favButton}
                            activeOpacity={1}
                            onPress={() => {
                                this.props.navigation.navigate("StoreDetail", {
                                    data: data.store,
                                    title: data.store.name
                                });
                            }}
                        >
                            <CachedImage
                                style={{
                                    width: 18,
                                    height: 18
                                }}
                                source={require("../../../assets/icons/directory.png")}
                            />

                        </TouchableOpacity>
                    )}
                </View>
            );
        }

        return (
            <Component {...this.props} />
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

OfferDetailScreen.propTypes = {
    navigation: PropTypes.instanceOf(Object),
    shareData: PropTypes.func,
    favoriteAction: PropTypes.func
};

OfferDetailScreen.defaultProps = {
    navigation: {},
    shareData: () => {
    },
    favoriteAction: () => {
    }
};

const OfferDetailRedux = connect(mapStateToProps, {
    favoriteAction,
    shareData
})(OfferDetailScreen);

export { OfferDetailRedux as OfferDetailScreen };
