import React from "react";
import {connect} from "react-redux";
import {TouchableOpacity} from "react-native";
import _ from "lodash";
import PropTypes from "prop-types";
import {DrawerActions} from "react-navigation";

import config from "../../config/config";
import {translateText} from "../translation/translate";
import {BrandListScreen} from "./";
import {Icon} from "../components/common/";

class CafeRestaurantScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("CafeRestaurants")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("CafeRestaurants")
                .toUpperCase(),
        };
    };

    render() {
        const {Enabled, Component} = config().CafeRestaurantScreen;
        if (!Enabled) {
            const data = {...this.props.navigation, ...{state: {params: {stores: this.props.category.stores}}}};

            return (
                <BrandListScreen navigation={data}/>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    const {payload} = state.main;
    const {user} = state.auth;
    if (payload !== null) {
        const {categories, mall} = payload;
        const st = payload.stores;
        const {CafeRestaurantCategoryId} = config().CafeRestaurantScreen;

        if (!CafeRestaurantCategoryId && !mall.dining_category) {
            throw new Error("You must declare CafeRestaurantCategoryId on config file...");
        }

        const category = _.find(categories, {"_id": CafeRestaurantCategoryId || mall.dining_category});

        category.stores = [...st.filter((store) => {
            if (user !== null && user.isLoggedIn) {
                const match = user["favorite_stores"].find((obj) => {
                    return obj["s"] === store["_id"];
                });
                store["fav"] = !!match;
                store["userid"] = user["_id"];
            }

            return store.categories.filter((cat) => {
                return cat["s"] === category["_id"];
            }).length > 0;
        })];

        return {category};
    }
    return {category: {}};
};

CafeRestaurantScreen.propTypes = {
    navigation: PropTypes.object,
    category: PropTypes.object
};

const CafeRestaurantsRedux = connect(mapStateToProps)(CafeRestaurantScreen);

export {CafeRestaurantsRedux as CafeRestaurantScreen};
