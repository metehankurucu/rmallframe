import React from "react";
import {TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import {DrawerActions} from "react-navigation";

import {translateText} from "../../translation/translate";
import config from "../../../config/config";
import {Icon} from "../../components/common/";

class WhatsNewScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        const {DrawerMenuIcon} = config();

        if (state.params && state.params.type === "REPLACE") {
            return {
                title: translateText("WhatsNews")
                    .toUpperCase(),
                headerLeft: (
                    <TouchableOpacity onPress={() => {
                        const {isDrawerOpen} = navigation.state;

                        if (!isDrawerOpen) {
                            navigation.dispatch({type: DrawerActions.OPEN_DRAWER});
                        }
                    }}
                    >
                        <Icon {...DrawerMenuIcon}/>
                    </TouchableOpacity>
                )
            };
        }
        return {
            title: translateText("WhatsNews")
                .toUpperCase(),
        };
    };

    render() {
        const WhatsNew = config().WhatsNewScreen;

        if (!WhatsNew) {
            return (
                <View>
                    <Text>Override WhatsNew screen.</Text>
                </View>
            );
        }

        return (
            <WhatsNew {...this.props}/>
        );
    }
}

const mapStateToProps = ({main, auth}) => {
    const {payload} = main;
    const {user} = auth;

    if (payload !== null) {
        payload.news.forEach((object) => {
            if (user && user.isLoggedIn) {
                const match = user["favorite_news"].find((obj) => {
                    return obj["s"] === object["_id"];
                });
                object["fav"] = !!match;
                object["userid"] = user["_id"];
            }
            else {
                object["fav"] = false;
                object["userid"] = null;
            }

            object.store = payload.stores.filter((store) => {
                return store._id === object.store_id;
            })[0];

            if (object.store) {
                if (user && user.isLoggedIn) {
                    const match = user["favorite_stores"].find((obj) => {
                        return obj["s"] === object.store["_id"];
                    });

                    object.store["fav"] = !!match;
                    object.store["userid"] = user["_id"];
                }
                else {
                    object.store["fav"] = false;
                    object.store["userid"] = null;
                }
            }
        });
        return {
            new: payload.news
        };
    }

    return {new: null};
};

const WhatsNewsRedux = connect(mapStateToProps)(WhatsNewScreen);

export {WhatsNewsRedux as WhatsNewScreen};
