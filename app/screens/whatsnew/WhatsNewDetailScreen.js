import React from 'react';
import {View} from 'react-native';
import {connect} from "react-redux";
import {getDefaultLanguageText} from "../../translation/translate";
import config from "../../../config/config";

class WhatsNewDetailScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        return {
            title: `${getDefaultLanguageText(state.params.title)}`.toUpperCase(),
        };
    };


    render() {
        const WhatsNewDetail = config().WhatsNewDetailScreen;

        if (WhatsNewDetail)
            return (
                <View style={{flex: 1}}>
                    <Text>Override WhatsNewDetail screen.</Text>
                </View>
            );

        return (
            <WhatsNewDetailScreen {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return state
};


const WhatsNewDetailRedux = connect(mapStateToProps)(WhatsNewDetailScreen);

export {WhatsNewDetailRedux as WhatsNewDetailScreen}
