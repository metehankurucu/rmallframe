import React from "react";
import {WebView} from "react-native";
import {HeaderBackButton} from "react-navigation";


class BrowserScreen extends React.Component {
    static navigationOptions = ({navigation}) => {

        return {
            title: navigation.state.params.title.toUpperCase(),
            headerLeft: < HeaderBackButton
                tintColor={'#ffffff'}
                pressColorAndroid={'#ffffff'}
                onPress={() => {
                    if (navigation.state.params && navigation.state.params.browserClosed) {
                        navigation.state.params.browserClosed();
                    }
                    navigation.goBack();
                }}
            />,
        }
    };

    render() {
        const {uri} = this.props.navigation.state.params;

        return (
            <WebView
                startInLoadingState
                source={{uri: uri}}
            />
        );
    }
}


export {BrowserScreen};