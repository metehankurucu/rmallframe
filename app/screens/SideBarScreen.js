import React from "react";
import {Dimensions, Image, ScrollView, Text, TouchableHighlight, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import ModalDropdown from "react-native-modal-dropdown";
import config from "../../config/config";
import {setLocale} from "../actions/i18n";
import {Normalize} from "../../config/helpers";

const {width, height} = Dimensions.get("window");

class SideBarScreen extends React.Component {

    _dropdown_renderButtonText = (rowData) => {
        const {sort} = rowData;
        return `${sort}`;
    };

    _dropdown_renderRow = (rowData, rowID, highlighted) => {
        return (
            <TouchableHighlight underlayColor="cornflowerblue">
                <Text style={[styles.languageTextStyle, highlighted && {color: "mediumaquamarine"}]}>
                    {`${rowData.lang}`}
                </Text>
            </TouchableHighlight>
        );
    };

    _dropdown_renderSeparator = (sectionID, rowID, adjacentRowHighlighted) => {
        if (rowID == config().DEFAULT_LANGUAGES.length - 1) return;
        const key = `spr_${rowID}`;
        return (<View
            style={styles.dropdown_separator}
            key={key}
        />);
    }

    render() {
        const {
            Enabled, Component, BackgroundImage, ChangeLanguage
        } = config().SideBarScreen;

        return (
            <View style={styles.container}>
                <Image
                    style={styles.backgroundImage}
                    source={BackgroundImage}
                />

                <View style={styles.header}>
                    {ChangeLanguage && <View style={styles.menu_lang}>
                        <ModalDropdown
                            textStyle={styles.menu_lang_text}
                            options={config().DEFAULT_LANGUAGES}
                            dropdownStyle={styles.dropdown_dropdown}
                            renderSeparator={(sectionID, rowID, adjacentRowHighlighted) => {
                                return this._dropdown_renderSeparator(sectionID, rowID, adjacentRowHighlighted);
                            }}
                            defaultValue={this.props.currentLocale.toUpperCase()}
                            renderButtonText={(rowData) => {
                                return this._dropdown_renderButtonText(rowData); 
                            }}
                            renderRow={this._dropdown_renderRow.bind(this)}
                            onSelect={(idx, value) => {
                                this.props.setLocale(value.sort.toLowerCase());
                                this.props.navigation.dispatch(NavigationActions.reset({
                                    index: 0,
                                    actions: [NavigationActions.navigate({routeName: "Home"})]
                                }));
                            }}
                        />
                    </View>}
                </View>

                <View style={styles.routeContainer}>
                    <ScrollView>
                        <Component {...this.props}/>
                    </ScrollView>
                </View>
            </View>
        );
    }

}

const styles = {
    languageTextStyle: {
        fontSize: 15,
        margin: 5,
        color: "#fff"
    },
    dropdown_separator: {
        height: 1,
        backgroundColor: "#fff",
    },
    backgroundImage: {
        backgroundColor: "#ccc",
        flex: 1,
        resizeMode: "cover",
        position: "absolute",
        width: "100%",
        height: "100%",
        justifyContent: "center",
    },
    container: {
        flex: 1,
        height,
        width,
    },
    routeContainer: {
        flex: 1,
        marginLeft: 15,
        marginBottom: 15,
    },
    iconContainer: {
        marginRight: 10,
        justifyContent: "center",
        alignItems: "center",
    },
    routeItemStyle: {
        fontSize: 16,
        marginLeft: 8,
        color: "#fff",
    },
    routeItemContainer: {
        flexDirection: "row",
        alignItems: "center",
    },
    routeItemButton: {
        marginTop: 8,
    },
    menu_lang: {
        justifyContent: "center"
    },
    dropdown_dropdown: {
        height: 126,
        borderColor: "#fff",
        borderRadius: 3,
        marginTop: 5,
        backgroundColor: "transparent"
    },
    menu_lang_text: {
        fontSize: 21,
        color: "#fff",
        borderColor: "#fff",
        borderWidth: 1,
        padding: 5
    },
    header: {
        flexDirection: "row",
        height: Normalize(100),
        alignItems: "flex-end",
        justifyContent: "flex-end",
        width: "65%"
    },
};

const mapStateToProps = ({main, i18n}) => {
    return {currentLocale: i18n.locale, };
};

export default connect(mapStateToProps, {setLocale})(SideBarScreen);
