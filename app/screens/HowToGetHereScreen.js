/* eslint-disable import/prefer-default-export */
import React, {PureComponent} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {ActivityIndicator, Dimensions, InteractionManager, Platform, StyleSheet, View} from "react-native";
import Communications from "react-native-communications";
import MapView, {PROVIDER_GOOGLE} from "react-native-maps";

import config from "../../config/config";
import {BorderlessButton} from "../components/buttons";
import {getDefaultLanguageText, translateText} from "../translation/translate";

const {width, height} = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class HowToGetHereScreen extends PureComponent {
    static defaultProps = {
        liteMode: false
    };

    constructor(props) {
        super(props);

        this.state = {
            region: {
                latitude: parseFloat(this.props.mall.latitude) - 0.015,
                longitude: parseFloat(this.props.mall.longitude),
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            coordinate: {
                latitude: parseFloat(this.props.mall.latitude),
                longitude: parseFloat(this.props.mall.longitude),
            },
            toolbarHackHeight: 0,
        };
    }

    _renderMarkers() {
        return (
            <MapView.Marker
                coordinate={this.state.coordinate}
                onPress={this._showToolbarHack}
                title={getDefaultLanguageText(this.props.mall.name)}
            />
        );
    }

    _showToolbarHack = () => {
        const heightDiff = 0.5;
        // we don't want to change the height too much so keep it small. I've noticed 0.5 works best,
        // as opposed to 0.1 doesn't work at all, and 0.5 is barely noticeable to the user.
        // switch the height between 0 and 0.5 and back.
        this.setState({
            toolbarHackHeight: this.state.toolbarHackHeight === heightDiff ? this.state.toolbarHackHeight - heightDiff : this.state.toolbarHackHeight + heightDiff
        });
    };

    styles = StyleSheet.create({
        map: {
            flex: 1,
        },
        button: {
            width: 80,
            paddingHorizontal: 12,
            alignItems: "center",
            marginHorizontal: 10,
        },
        buttonContainer: {
            flexDirection: "row",
            marginVertical: 20,
            backgroundColor: "transparent",
        },
        redirectLocation: {
            position: "absolute",
            margin: 15,
            marginTop: 50,
            height: 50,
            backgroundColor: "#fff",
            shadowOpacity: 0.2,
            shadowRadius: 10,
            elevation: 2,
            zIndex: 1,
            alignSelf: "center",
        }
    });

    render() {
        const {Enabled, Component} = config().HowToGetHereScreen;

        if (!Enabled) {
            return (
                <View style={{flex: 1}}>

                    <MapView
                        liteMode={this.props.liteMode || false}
                        region={this.state.region}
                        style={[this.styles.map, {marginBottom: this.state.toolbarHackHeight}]}
                        toolbarEnabled
                    >
                        {this._renderMarkers()}
                    </MapView>
                </View>
            );
        }

        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    const {payload} = state.main;
    if (payload !== null) {
        return {
            mall: payload.mall
        };
    }
    return {mall: null};
};

HowToGetHereScreen.propTypes = {
    mall: PropTypes.instanceOf(Object),
    liteMode: PropTypes.bool
};

const HowToGetHereScreenRedux = connect(mapStateToProps)(HowToGetHereScreen);

export {HowToGetHereScreenRedux as HowToGetHereScreen};
