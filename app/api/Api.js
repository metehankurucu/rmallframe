import config from "../../config/config";

class Api {
    init = (data) => {
        return {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                info: {
                    ...config().INFO_DATA(),
                    userid: config()
                        .getUserID(),
                    push_token: config()
                        .getPushToken()
                },
                data
            })
        };
    };
    LoginUser = (data = {}) => {
        return fetch(`${config().API_URL}user/login/email`, this.init(data))
            .then((response) => {
                return response.json();
            });
    };
    getAll = (version = {}) => {
        const init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                info: config().INFO_DATA(),
                version
            })
        };

        return fetch(`${config().API_URL}all`, init)
            .then((response) => {
                return response.json();
            })
            .catch((err) => {
                // console.log("err", err);
                return false;
            });
    };

    NewSP(data = {}) {
        return fetch(`${config().API_URL}sp`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    NewGeofence(data = {}) {
        return fetch(`${config().API_URL}geofence`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    BeaconVisited(data = {}) {
        return fetch(`${config().API_URL}beacons/visited`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    BeaconParking(data = {}) {
        return fetch(`${config().API_URL}beacons/parking`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    BeaconLive(data = {}) {
        return fetch(`${config().API_URL}beacons/live`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    AllBeacon(data = {}) {
        return fetch(`${config().API_URL}beacons/all`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    ForgetPassword(email = "") {
        return fetch(`${config().API_URL}user/password/forget`, this.init({ email }))
            .then((response) => {
                return response.json();
            });
    }

    RegisterUser(data = {}) {
        return fetch(`${config().API_URL}user/register`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    LoginWithFacebook(resp, infoResponse) {
        const data = {
            "email": infoResponse.email,
            "type": "facebook",
            "name": infoResponse.first_name,
            "surname": infoResponse.last_name,
            "social_token": resp.authResponse.accessToken,
            "raw": {
                "first_name": infoResponse.first_name,
                "last_name": infoResponse.last_name,
                "gender": infoResponse.gender,
                "id": infoResponse.id,
                "image": infoResponse.cover && infoResponse.cover.source ? infoResponse.cover.source : "",
                "link": infoResponse.link
            }
        };

        return fetch(`${config().API_URL}user/login/social`, this.init(data))
            .then((response) => {
                return response.json();
            })
            .catch((err) => {
                console.log("err", err);

            });
    }

    LoginWithGoogle(user) {
        const data = {
            "email": user.email,
            "type": "google",
            "social_token": user.accessToken,
            "name": user.givenName,
            "surname": user.familyName,
            "raw": {
                "first_name": user.givenName,
                "last_name": user.familyName,
                "id": user.id,
                "image": user.photo,
            }
        };

        return fetch(`${config().API_URL}user/login/social`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    AddFavorite(userid, data = {}) {
        return fetch(`${config().API_URL}user/${userid}/favorite/add`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    RemoveFavorite(userid, data = {}) {
        return fetch(`${config().API_URL}user/${userid}/favorite/remove`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    UpdateUser(data = {}, userProfile) {
        return fetch(`${config().API_URL}user/${userProfile["_id"]}/update`, this.init(data))
            .then((response) => {
                return response.json();
            });
    }

    saveImage(userid, data) {

        if (!data.hasOwnProperty("fileName")) {
            data["fileName"] = `${new Date().getTime()
                .toString()}.JPG`;
        }

        const form = new FormData();
        form.append("language", "en");
        form.append("profile-picture", {
            uri: data.uri,
            name: data.fileName,
            type: data.type,
        });

        const request = {
            method: "POST",
            header: {
                "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            },
            body: form
        };
        return fetch(`${config().API_URL}user/${userid}/upload/profile-picture`, request)
            .then((response) => {
                return response.json();
            });
    }
}

const api = new Api();
export default api;
