import I18n from "react-native-i18n";
import config from "../../config/config";
import _ from "lodash";

export function translateText(text, params) {
    I18n.fallbacks = true;

    if (params) {
        return I18n.t(text, params);
    }
    return I18n.t(text)
}

export function getDefaultLanguageText(data) {
    if (typeof data === "string") {
        return data;
    }

    const { DEFAULT_LANGUAGE, DEFAULT_LANGUAGES } = config();

    const language = _.find(DEFAULT_LANGUAGES, (o) => {
        return I18n.currentLocale().includes(o.sort.toLowerCase())
    });

    const sort = language && language.sort ? language.sort.toLowerCase() : DEFAULT_LANGUAGE;

    return data[sort] ? data[sort] : data[DEFAULT_LANGUAGE];
}
