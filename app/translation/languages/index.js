import en from "./en";
import ar from "./ar";
import tr from "./tr";

export default {en, ar, tr};
