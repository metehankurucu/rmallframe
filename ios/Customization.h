//
//  Customization.h
//  iOSMallApp
//
//  Created by Deniz Tav on 25/07/16.
//  Copyright © 2016 Deniz Tav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Customization : NSObject

@property (nonatomic, strong) NSString *localizationLanguage;
@property (nonatomic, strong) NSString *targetName;
@property (nonatomic, strong) NSString *selectedMallId;
@property (nonatomic, strong) NSString *mallframeItApiKey;
@property (nonatomic, strong) NSString *mallframeItURL;
@property (nonatomic, strong) NSString *mallframeItDevURL;
@property (strong, nonatomic) NSArray *sideMenuItems;
@property (strong, nonatomic) NSArray *menuItems;
@property (assign, nonatomic) BOOL subcategory;


+ (id)sharedController;
- (id)init;

@end
