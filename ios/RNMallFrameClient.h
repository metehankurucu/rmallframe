#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <CoreLocation/CoreLocation.h>


@interface RNMallFrameClient :RCTEventEmitter <RCTBridgeModule, CLLocationManagerDelegate>

@property CLLocationManager* locationManager;

@end
