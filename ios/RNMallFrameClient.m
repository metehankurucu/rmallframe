
#import "RNMallFrameClient.h"
#import "CUK_Client.h"
#import "React/RCTLog.h"

@implementation RNMallFrameClient;

RCT_EXPORT_MODULE()

-(instancetype)init
{
    self = [super init];
    if (self) {
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return self;
}

- (NSArray<NSString *> *)supportedEvents
{
    return @[@"didEnterRegion"];
}

RCT_EXPORT_METHOD(configure:(NSDictionary *)configDictionary)
{
    RCTLogInfo(@"RNMallFrameClient #configure");
    
    NSString *url = [configDictionary objectForKey:@"url"];
    NSString *userid = [configDictionary objectForKey:@"userid"];
    NSString *push_token = [configDictionary objectForKey:@"push_token"];
    
    RCTLogInfo(@"userid %@" , userid);
    RCTLogInfo(@"url %@" , url);
    RCTLogInfo(@"push_token %@" , push_token);

    [[NSUserDefaults standardUserDefaults] setObject:push_token forKey:@"pushToken"];
    [[NSUserDefaults standardUserDefaults] setObject:userid forKey:@"userid"];
    [[NSUserDefaults standardUserDefaults] setObject:url forKey:@"url"];
}

RCT_EXPORT_METHOD(startGeofence:(NSDictionary *)locations)
{
    // [self.locationManager requestAlwaysAuthorization];
    
        NSArray *params = [locations objectForKey:@"locations"];
        RCTLogInfo(@"userid %@" , [params[0] objectForKey:@"lat"]);
        RCTLogInfo(@"lang %@" , [params[0] objectForKey:@"lang"]);
        RCTLogInfo(@"key %@" , [params[0] objectForKey:@"key"]);
        
        NSString *keyValue = [NSString stringWithFormat:@"%@",[params[0] objectForKey:@"key"]];
        
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                    [self setUpGeofences:[[params[0] objectForKey:@"lat"] doubleValue] lang:[[params[0] objectForKey:@"lang"] doubleValue] key:keyValue];
                break;
            case kCLAuthorizationStatusAuthorizedAlways:
                    [self setUpGeofences:[[params[0] objectForKey:@"lat"] doubleValue] lang:[[params[0] objectForKey:@"lang"] doubleValue] key:keyValue];
                break;
            case kCLAuthorizationStatusNotDetermined:
            case kCLAuthorizationStatusDenied:
            case kCLAuthorizationStatusRestricted:
                [self.locationManager requestAlwaysAuthorization];
                break;
        }

}

RCT_EXPORT_METHOD(enableGPS)
{
    RCTLogInfo(@"enableGPS called");
    [self.locationManager requestAlwaysAuthorization];
}

RCT_EXPORT_METHOD(getCurrentState:(RCTPromiseResolveBlock)resolve
                  reject:(RCTPromiseRejectBlock)reject)
{
    if (!self.locationManager) {
        NSError * error;
        reject(@"no_bluetooth_init", @"Location manager cannot be initialized", error);
    } else {
        resolve(@[[self nameForAuthorizationStatus:[CLLocationManager authorizationStatus]]]);
    }
}

-(NSString *)nameForAuthorizationStatus:(CLAuthorizationStatus)authorizationStatus
{
    switch (authorizationStatus) {
        case kCLAuthorizationStatusAuthorizedAlways:
            NSLog(@"Authorization Status: authorizedAlways");
            return @"authorizedAlways";
            
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"Authorization Status: authorizedWhenInUse");
            return @"authorizedWhenInUse";
            
        case kCLAuthorizationStatusDenied:
            NSLog(@"Authorization Status: denied");
            return @"denied";
            
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"Authorization Status: notDetermined");
            return @"notDetermined";
            
        case kCLAuthorizationStatusRestricted:
            NSLog(@"Authorization Status: restricted");
            return @"restricted";
    }
}

- (void)setUpGeofences:(double)latValue lang:(double)langValue key:(NSString*)keyValue{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *geoFance = [NSString stringWithFormat:@"geofence_%@",keyValue];
        
        
        RCTLogInfo(@"setUpGeofences");
        
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(latValue, langValue);
        CLRegion *bridge = [[CLCircularRegion alloc]initWithCenter:center
                                                            radius:250.0
                                                        identifier:geoFance];
        bridge.notifyOnEntry =YES;
        bridge.notifyOnExit = YES;
        [self.locationManager startMonitoringForRegion:bridge];
        
    });
}

- (void)locationManager:(CLLocationManager *)manager startMonitoringForRegion:(CLCircularRegion *)region
{
    RCTLogInfo(@"startMonitoringForRegion");
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLCircularRegion *)regions {
    RCTLogInfo(@"didStartMonitoringForRegion");
}

-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLCircularRegion *)regions withError:(nonnull NSError *)error{
    RCTLogInfo(@"monitoringDidFailForRegion");
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLCircularRegion *)region{
    if (! [region respondsToSelector:@selector(proximityUUID)]) {
        return;
    }
}

- (void)locationManager:(CLLocationManager *)manager stopMonitoringForRegion:(CLCircularRegion *)region
{
    RCTLogInfo(@"stopMonitoringForRegion");
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        // Burda bi kez daha cagirmak lazim
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"didEnterRegion");
    if ([region.identifier hasPrefix:@"geofence"]) {
        [[CUK_Client sharedClient] sendGeofence:region.identifier completion:^(NSString *error, id responseObject) {}];
    }
}

@end
  
