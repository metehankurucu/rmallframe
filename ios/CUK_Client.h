//
//  CUK_Client.h
//  Pods
//
//  Created by Kivanc on 17/06/15.
//
//

#import <UIKit/UIKit.h>
#import "AFHTTPSessionManager.h"
#import "Constants.h"

@interface CUK_Client : AFHTTPSessionManager

+ (CUK_Client *) sharedClient;
+ (CUK_Client *) googleMapsClient;

- (instancetype)initWithBaseURL:(NSURL *)url;

- (void)makeRequestPost:(NSString *)route WithParameters:(NSDictionary *) paramaters completion:(DefaultIdResultBlock) completion;

-(void)sendGeofence:(NSString *) identifer completion:(DefaultIdResultBlock) completion;

@end
