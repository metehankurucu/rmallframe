//
//  Customization.m
//  iOSMallApp
//
//  Created by Deniz Tav on 25/07/16.
//  Copyright © 2016 Deniz Tav. All rights reserved.
//

#import "Customization.h"

@implementation Customization

+ (id)sharedController {
    static NSObject* sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedController = [[self alloc] init];

    });
    return sharedController;
}

- (id)init
{
    if (self = [super init])
    {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"customise" ofType:@"plist"];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        NSArray *generalInfoArray = [dict objectForKey:@"General"];
        _selectedMallId = [generalInfoArray objectAtIndex:1];
        _localizationLanguage = [generalInfoArray objectAtIndex:0];
        _targetName = [generalInfoArray objectAtIndex:2];
        NSDictionary *dic = [generalInfoArray objectAtIndex:3];
        _mallframeItURL = [dic objectForKey:@"MallframeItURL"];
        _mallframeItDevURL = [dic objectForKey:@"MallframeItDevURL"];
        _mallframeItApiKey = [dic objectForKey:@"MallframeItApiKey"];
        _subcategory = [[dic objectForKey:@"subcategory"] boolValue];

        _sideMenuItems = [dict objectForKey:@"SideMenu"];
        _menuItems = [dict objectForKey:@"MainMenu"];

    }
    return self;
}

@end
