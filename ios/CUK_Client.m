//
//  CUK_Client.m
//  Pods
//
//  Created by Kivanc on 17/06/15.
//
//

// Set this to your World Weather Online API Key

//add this to your constan.h file

#import "CUK_Client.h"
#import "Customization.h"
#import <sys/utsname.h>
#import "React/RCTLog.h"

//Get DeviceName without using any thirdParty lib
NSString* deviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}


@implementation CUK_Client


+ (CUK_Client *) sharedClient {
    static CUK_Client *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken , ^ {
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:[[Customization sharedController] mallframeItURL]]];
        
    });
    
    return  _sharedClient;
}

+ (CUK_Client *) googleMapsClient {
    static CUK_Client *googleMapsClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken , ^ {
        googleMapsClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:@"http://maps.googleapis.com/"]];
        
    });
    
    return  googleMapsClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        self.securityPolicy = [AFSecurityPolicy defaultPolicy];
        self.securityPolicy.allowInvalidCertificates = YES;
    }
    
    return self;
    
}

- (void) makeRequestPost:(NSString *)route WithParameters:(NSDictionary *) paramaters completion:(DefaultIdResultBlock) completion {
    
    NSLog(@"Doing Something");
    
    [self POST:route
    parameters:paramaters
      progress:nil
       success:^(NSURLSessionDataTask *task, id responseObject) {
           
           if (!responseObject) {
               return completion(NSLocalizedString(@"Your internet connection may be too slow and/or server may not be responding, please try again later.", nil),  nil);
           }
           
           if ([responseObject isKindOfClass:[NSDictionary class]]) {
               NSDictionary *error = responseObject[@"error"];
               if (error) {
                   
                   NSNumber *code = [error valueForKey:@"code"];
                   NSString *msg = [error objectForKey:@"msg"];
                   NSLog(@"%@ - %@" ,code,error);
                   if ([code isEqualToNumber:@9000]) {
                       return completion ( [NSString stringWithFormat:@"%@",msg] ,nil );
                   }
                   else {
                       return completion (NSLocalizedString(@"Your internet connection may be too slow and/or server may not be responding, please try again later.", nil), nil);
                   }
               }
           }
           
           return completion (nil, responseObject);
       }
       failure:^(NSURLSessionDataTask *task, NSError *error) {
           NSLog(@"%@", [error description]);
           NSLog(@"The request is failed due to other problems. Check you internet.");
           return completion ([error localizedDescription], nil);
       }];
}


#pragma mark Geofence

- (void)sendGeofence:(NSString *) identifer completion:(DefaultIdResultBlock) completion {
    

    RCTLogInfo(@"geofence bulundu.. :%@", [self getInfoObject]);

    NSDictionary *parameters = @{
                                 @"info":[self getInfoObject],
                                 @"data": @{
                                         @"identifier": identifer
                                         }
                                 };
    
    NSString *route = [[NSUserDefaults standardUserDefaults] objectForKey:@"url"];
    
    route = [NSString stringWithFormat:@"%@geofence", route];
    
    NSLog(@"%@",route);
    
    [self makeRequestPost:route WithParameters:parameters completion:completion];
}

- (NSDictionary *)getInfoObject {
    
    NSString *push_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"push_token"];
    NSString *userid = [[NSUserDefaults standardUserDefaults] objectForKey:@"userid"];
    push_token = push_token ? push_token : @"";

    NSString *env = @"";
    
    #if defined(DEBUG)
    env = @"development";
    #else
    env = @"production";
    #endif
    
    NSLog(@"%@", @{
        @"language" : [self getLanguage],
        @"device" : @"ios",
        @"env" :env,
        @"push_token":push_token,
        @"userid": userid
    });
    
    return @{
             @"language" : [self getLanguage],
             @"device" : @"ios",
             @"env" :env,
             @"push_token":push_token,
             @"userid": userid
             };
}

- (NSString *) getLanguage {
    return @"en";
}

@end
