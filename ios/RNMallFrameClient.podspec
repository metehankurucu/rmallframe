
Pod::Spec.new do |s|
  s.name         = "RNMallFrameClient"
  s.version      = "1.0.0"
  s.summary      = "RNMallFrameClient Updated"
  s.description  = <<-DESC
                  RNMallFrameClient
                   DESC
  s.homepage     = "https://metehankurucu@bitbucket.org/metehankurucu/rmallframe"
  s.license      = "MIT"
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://metehankurucu@bitbucket.org/metehankurucu/rmallframe.git", :tag => "master" }
  s.source_files  = "RNMallFrameClient/**/*.{h,m}"
  s.requires_arc = true

  s.dependency "React"
  #s.dependency "others"

end

  